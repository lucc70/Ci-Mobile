//
//  AccessTempViewController.swift
//  ConnectAsso
// // F7F7F0
//  Created by Luc Coujonde on 24/05/2021.
// Page temporaire qui fait office de point d'entrée pour accéder rapidement a toutes les autres view a supprimer une fois l'arborescence du projet fini

import UIKit

class AccessTempViewController: UIViewController {

    @IBOutlet weak var ConnexionButton: UIButton!
    @IBOutlet weak var InscriptionButton: UIButton!
    @IBOutlet weak var SignalerButton: UIButton!
    @IBOutlet weak var listeEvenementButton: UIButton!
    @IBOutlet weak var CreerEvenementButton: UIButton!
    @IBOutlet weak var UnEvenement: UIButton!
    @IBOutlet var profilAssoBtn: UIButton!

    @IBOutlet var navBarRightBtnText: UIBarButtonItem!

    var eventService: EvenementService = EvenementService()
    var evenement : Evenement!

    var benevoleService : BenevoleService = BenevoleService()

    var associationService: AssociationService = AssociationService()
    var association : Association!

    var gestionnaireService: GestionnaireAssoService = GestionnaireAssoService()
    var gestionnaire : Association!

    var gestionnaireAssociation : GestionnaireAssociation!

    static func newInstance(gestionnaireAssociation : GestionnaireAssociation, association : Association) ->AccessTempViewController {
        let resumeController = AccessTempViewController()
        resumeController.gestionnaireAssociation = gestionnaireAssociation
        resumeController.association = association
        return resumeController
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        if ((gestionnaireAssociation) != nil) {
            self.navBarRightBtnText.title = gestionnaireAssociation.mail
        }


        // Do any additional setup after loading the view.
    }

    @IBAction func AllerConnexionView(_ sender: Any) {
        let connexionView = ConnexionViewController(nibName: "ConnexionViewController", bundle: nil)
        self.navigationController?.pushViewController(connexionView, animated: true)
    }

    @IBAction func AllerInscriptionView(_ sender: Any) {
        let inscriptionView = InscriptionViewController(nibName: "InscriptionViewController", bundle: nil)
        self.navigationController?.pushViewController(inscriptionView, animated: true)
    }

    @IBAction func AllerProfilGAView(_ sender: Any) {
        let signalerView = ProfilGAViewController(nibName: "ProfilGAViewController", bundle: nil)
        self.navigationController?.pushViewController(signalerView, animated: true)
    }
    @IBAction func AllerProfilAssoView(_ sender: Any) {
        let profilAssoView = ProfilAssoViewController(nibName: "ProfilAssoViewController", bundle: nil)
        self.navigationController?.pushViewController(profilAssoView, animated: true)
    }

    @IBAction func AllerListeEvenementView(_ sender: Any) {
        let tousEvenementView = TousEvenementsViewController()
        self.navigationController?.pushViewController(tousEvenementView, animated: true)
    }


//    @IBAction func AllerDetailEvenementView(_ sender: Any) {
//        let detailEvenement = DetailsEvenementViewController()
//        self.navigationController?.pushViewController(detailEvenement, animated: true)
//    }


    @IBAction func AllerCreerEvenementView(_ sender: Any) {
        let creerEvenement = CreerEvenementViewController()
        self.navigationController?.pushViewController(creerEvenement, animated: true)
    }

    @IBAction func AllerListParticipantsView(_ sender: Any) {
        let listParticipantsView = ParticipantsListViewController()
        self.navigationController?.pushViewController(listParticipantsView, animated: true)
    }


    @IBAction func AllerDescriptionPersonnelle(_ sender: Any) {
        let descriptionPersonnelle = DescriptionPersonnelle2ViewController()
        self.navigationController?.pushViewController(descriptionPersonnelle, animated: true)
    }

    @IBAction func AllerDescriptionAssociationView(_ sender: Any) {
        let descriptionAssociation = DescriptionAssociationViewController()
        self.navigationController?.pushViewController(descriptionAssociation, animated: true)
    }


    @IBAction func testReq(_ sender: Any) {

        let eventId = 2


        benevoleService.participantsEnAttente(evenementId: eventId) { (BenevoleListFromApi) in
            print(BenevoleListFromApi)
        }
    }

}



