//
//  ParticipantsListViewController.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 05/09/2021.
//

import UIKit

class ParticipantsListViewController: UIViewController{

    var gestionnaireAssociation : GestionnaireAssociation!
    var association : Association!
    var evenement : Evenement!
    var OneParticipant : Benevole!
    var participantType :String! //= "ParticipantsEnAttente"
    var benevolService : BenevoleService = BenevoleService()
    var evenementService : EvenementService = EvenementService()
    var particpantsIncr = 0
    var participantsMock : ParticipantsMocks?
    var participantsInscriptMock : ParticipantsMocks?

    //var participantT = "ParticipantsEnAttente"
    var participants : [Benevole] = []
    var participantsInscritMockView : [Benevole] = []
    var participantsEnAttenteMockView : [Benevole] = []
    var modification : Bool?
    @IBOutlet var participantsTableView: UITableView!
    @IBOutlet weak var refreshBtn: UIButton!


    static func newInstance(gestionnaireAssociation : GestionnaireAssociation, association : Association, evenement : Evenement, participantType : String) ->ParticipantsListViewController {
        let participantListController = ParticipantsListViewController()
        participantListController.gestionnaireAssociation = gestionnaireAssociation
        participantListController.association = association
        participantListController.evenement = evenement
        participantListController.participantType = participantType
        return participantListController
    }

    static func newInstanceEnAttenteTEST(gestionnaireAssociation : GestionnaireAssociation, association : Association, evenement : Evenement, participantType : String, participantsEnAttenteMock : ParticipantsMocks?) ->ParticipantsListViewController {
        let participantListController = ParticipantsListViewController()
        participantListController.gestionnaireAssociation = gestionnaireAssociation
        participantListController.association = association
        participantListController.evenement = evenement
        participantListController.participantType = participantType
        participantListController.participantsMock = participantsEnAttenteMock
        //participantListController.particpantsIncr = particpantsIncr
        return participantListController
    }

    static func newInstanceInscritTEST(gestionnaireAssociation : GestionnaireAssociation, association : Association, evenement : Evenement, participantType : String, participantsInscript : ParticipantsMocks?) ->ParticipantsListViewController {
        let participantListController = ParticipantsListViewController()
        participantListController.gestionnaireAssociation = gestionnaireAssociation
        participantListController.association = association
        participantListController.evenement = evenement
        participantListController.participantType = participantType
        participantListController.participantsInscriptMock = participantsInscript
        return participantListController
    }

    override func viewDidLoad() {
        if ProcessInfo.processInfo.arguments.contains("creerEvent") {
            print("test creerEvent ")

        }
        super.viewDidLoad()

        self.participantsTableView.delegate = self

        if(self.participantType == "participantsEnAttentes") {
            self.title = "Participants En Attente"
        } else {
            self.title = "Participants Inscrits"
        }

        self.participantsTableView.dataSource = self
        //self.participantsTabView.delegate = self
        self.participantsTableView.register(UINib(nibName: "ParticipantsTableViewCell", bundle: nil), forCellReuseIdentifier: "participant-cell")


    }

    @IBAction func refreshTab(_ sender: Any) {
        let listParticipantsView = ParticipantsListViewController()
        self.navigationController?.pushViewController(listParticipantsView, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {

        self.fillTabParticipants(animated)
        self.participantsTableView.reloadData()
    }

    func fillTabParticipants(_ animated: Bool) {

        if (self.participantType == "participantsEnAttentes"  && ProcessInfo.processInfo.arguments.contains("creerEvent")) {
            self.navigationController?.setNavigationBarHidden(false, animated: animated)
            print("laaaaaa")
            //print(self.participantsMock?.getListParticpantsEnAttenteEvenements())
            self.participants = (self.participantsMock?.getListParticpantsEnAttenteEvenements())!
            self.participantsTableView.reloadData()
            // Mock participants
        }

        if (self.participantType == "participantsInscrits"  && ProcessInfo.processInfo.arguments.contains("creerEvent")) {
            self.navigationController?.setNavigationBarHidden(false, animated: animated)
            print("laaaaaa2")
            //(self.participantsInscriptMock?.getListParticpantsInscritEvenements())
            self.participants = (self.participantsInscriptMock?.getListParticpantsInscritEvenements())!
            self.participantsTableView.reloadData()
        }

        if (self.participantType == "participantsEnAttentes" && !ProcessInfo.processInfo.arguments.contains("creerEvent")) {
            self.navigationController?.setNavigationBarHidden(false, animated: animated)
            self.benevolService.participantsEnAttente(evenementId: (self.evenement?.id)!){ (participantsFromAPI) in
                self.participants = participantsFromAPI
                DispatchQueue.main.sync {
                    self.participantsTableView.reloadData()
                }
            }
        }
        if(self.participantType == "participantsInscrits") && !ProcessInfo.processInfo.arguments.contains("creerEvent") {
            self.navigationController?.setNavigationBarHidden(false, animated: animated)
            self.benevolService.participantsInscrit(evenementId: (self.evenement?.id)!){ (participantsFromAPI) in
                self.participants = participantsFromAPI
                DispatchQueue.main.sync {
                    self.participantsTableView.reloadData()
                }
            }
        }
    }
}

extension ParticipantsListViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.participants.count//self.evenements.count A remplacer
    }

    func tableViewTEST(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //var participants : [Benevole]?
        if (self.participantType == "participantsEnAttentes") {
            print("participantsEnAttentes 52")
            self.participants = (self.participantsMock?.getListParticpantsEnAttenteEvenements())!
        }
        if (self.participantType == "participantsInscrits") {
            print("participantsInscrits")
            self.participants = (self.participantsInscriptMock?.getListParticpantsInscritEvenements())!
        }
        //print(participants)
        let participant = self.participants[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "participant-cell", for: indexPath) as! ParticipantsTableViewCell
        cell.nomParticipantLabel.text = participant.nom
        cell.prenomParticipantLabel.text = participant.prenom
        cell.mailParticpantLabel.text = participant.mail

        if (self.participantType == "participantsInscrits"){
            cell.acceptBtn.isHidden = true

        }

        if(self.evenement.statut == "termine" || self.evenement.statut == "annule") {
            cell.refuseBtn.isHidden = true
            cell.acceptBtn.isHidden = true
        }

        cell.buttonPressed = {
            let participant = self.participants[indexPath.row]
            print("indexPath.row \(indexPath.row)")
            self.participantsMock?.getOneParticpant(participantSelected: participant)
            self.participantsMock?.moveParticipantsEnAttenteToParticipantsInscrit(participantAccepted: participant)
            cell.acceptBtn.isEnabled = false
            print("ok")
                }


        cell.buttonRefusePressed = {
            let participant = self.participants[indexPath.row]
            print(indexPath.row)
            self.participantsMock?.getOneParticpant(participantSelected: participant)
            self.participantsMock?.moveParticipantsInscritToParticipantsEnAttente(idParticipantAccepted:participant.id!)
            cell.acceptBtn.isEnabled = false
            print("ok")
            }

        return cell
    }

    func tableViewAPI(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell  {
        let participant = self.participants[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "participant-cell",for: indexPath) as! ParticipantsTableViewCell
        cell.nomParticipantLabel.text = participant.nom
        cell.prenomParticipantLabel.text = participant.prenom
        cell.mailParticpantLabel.text = participant.mail

        if (self.participantType == "participantsInscrits"){
            cell.acceptBtn.isHidden = true

        }

        if(self.evenement.statut == "termine" || self.evenement.statut == "annule") {
            cell.refuseBtn.isHidden = true
            cell.acceptBtn.isHidden = true
        }


        cell.buttonPressed = {
            let participant = self.participants[indexPath.row]
            self.evenementService.acceptAParticipants(particpantsId: participant.id!, evenementId: self.evenement.id!) { (evenementFromApi) in
                DispatchQueue.main.async {
                    self.participantsTableView.reloadData()
                }
            }
            cell.acceptBtn.isEnabled = false
            print("ok")
        }


        cell.buttonRefusePressed = {
            if(self.participantType == "participantsInscrits"){
                    let participant = self.participants[indexPath.row]
                    // to do remove le benevole de la liste des inscrits a la liste attentes
                    self.evenementService.resetParticipantAccepted(particpantsId: participant.id!, evenementId: self.evenement.id!) { (evenementFromApi) in
                        DispatchQueue.main.async {
                            self.participantsTableView.reloadData()
                        }
                    }
                    cell.refuseBtn.isEnabled = false
                    print("participant reset ")
            }
            if (self.participantType == "participantsEnAttentes"){
                let participant = self.participants[indexPath.row]
                // to do remove le benevole de la liste des inscrits a la liste attentes
                self.evenementService.refusedParticipant(particpantsId: participant.id!, evenementId: self.evenement.id!) { (evenementFromApi) in
                        DispatchQueue.main.async {
                            self.participantsTableView.reloadData()
                        }
                    }
                cell.refuseBtn.isEnabled = false
                print("participant refusé ")
            }

        }
        return cell
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if ProcessInfo.processInfo.arguments.contains("creerEvent"){
            print("testé")
            return self.tableViewTEST(tableView, cellForRowAt: indexPath)
        }
            return self.tableViewAPI(tableView, cellForRowAt: indexPath)

    }
}

extension ParticipantsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let participant = self.participants[indexPath.row]
        print("participant id : \(participant.id!)")
        print("evenement id : \(self.evenement.id!)")

        // recuperer le participants à la bonne ligne
//        let controller = DetailsEvenementViewController.newInstance(evenement: evenement, gestionnaireAsso: self.gestionnaireAssociation, association: self.association)
//        self.navigationController?.pushViewController(controller, animated: true)
    }
}



