//
//  TousEvenementsViewController.swift
//  ConnectAsso
// F7F7F0
//  Created by Luc Coujonde on 06/06/2021.
//

import UIKit

class TousEvenementsViewController: UIViewController {

    var gestionnaireAssociation : GestionnaireAssociation!
    var association : Association!
    var associationMock : AssociationMock?// = AssociationMock()
    var gestionnaireService : GestionnaireAssoService = GestionnaireAssoService()
    var associationService : AssociationService = AssociationService()
    let evenementService : EvenementService = EvenementService()
    var evenements : [Evenement] = []
    var eventType : String = "Aucun"
    var evenementsMock: EvenementsMock?
    var gaMock: GestionnaireAssociationsMock?

    @IBOutlet var evenementSortSegmentControl: UISegmentedControl!
    @IBOutlet var evenementTableView: UITableView!
    @IBOutlet var leftBtnNavBar: UIBarButtonItem!
    @IBOutlet var rightBtnNavBar: UIBarButtonItem!
    @IBOutlet var navBar: UINavigationBar!



    @IBAction func sortListEvenementWithSegmentControl(_ sender: Any) {
        switch evenementSortSegmentControl.selectedSegmentIndex {
        case 0:
            print("tous")
            self.eventType = "tous"
            self.evenementService.getAllEventOfAsso(assoid: self.association.id!) { (evenementsFromAPI) in
                self.evenements = evenementsFromAPI
                DispatchQueue.main.sync {
                    print(self.evenements)
                    self.evenementTableView.reloadData()
                }
            }

        case 1:
            print("a venir")
            self.eventType = "avenir"
            self.evenementService.getAvenirEventOfAsso(assoid: self.association.id!) { (evenementsFromAPI) in
                self.evenements = evenementsFromAPI
                DispatchQueue.main.sync {
                    print(self.evenements)
                    self.evenementTableView.reloadData()
                }
            }
        case 2:
            print("terminés")
            self.eventType = "termine"
            self.evenementService.getTermineEventOfAsso(assoid: self.association.id!) { (evenementsFromAPI) in
                self.evenements = evenementsFromAPI
                DispatchQueue.main.sync {
                    print(self.evenements)
                    self.evenementTableView.reloadData()
                }
            }
        default:
            print("nada")
        }
    }



    let gestionnaireAssociationTest: GestionnaireAssociation = GestionnaireAssociation(id: nil, nom: "Dupont ", prenom: "David", motDePasse: "azerty", mail: "luckiminus@hotmail.fr", descriptionPersonnelle: "Description du gestionnaire. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including  versions of Lorem Ipsumversions of Lorem Ipsumversions of Lorem Ipsumversions of Lorem Ipsum")

    let associationTest: Association = Association(id: nil, nom: "Croix rouge", info: "Description de l'association. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including  versions of Lorem Ipsumversions of Lorem Ipsumversions of Lorem Ipsumversions of Lorem Ipsum", dateCreation: "12/12/1985")

    static func newInstance(gestionnaireAssociation: GestionnaireAssociation, association: Association, evenementsMock: EvenementsMock?, gaMock: GestionnaireAssociationsMock?) ->TousEvenementsViewController {
        print("in newInstance")
        let allEventController = TousEvenementsViewController()
        allEventController.gestionnaireAssociation = gestionnaireAssociation
        allEventController.association = association
        allEventController.evenementsMock = evenementsMock ?? nil
        allEventController.gaMock = gaMock ?? nil
        return allEventController
    }

    static func newInstanceProfilAssoEditTEST(gestionnaireAssociation : GestionnaireAssociation, association : Association, evenementsMock: EvenementsMock?, associationMock : AssociationMock?) ->TousEvenementsViewController {
        let allEventController = TousEvenementsViewController()
        allEventController.gestionnaireAssociation = gestionnaireAssociation
        allEventController.association = association
        allEventController.associationMock = associationMock
        allEventController.evenementsMock = evenementsMock ?? nil
        return allEventController
    }

    static func newInstanceTest(gestionnaireAssociation : GestionnaireAssociation, association : Association, evenementsMock: EvenementsMock?) ->TousEvenementsViewController {
        let allEventController = TousEvenementsViewController()
        allEventController.gestionnaireAssociation = gestionnaireAssociation
        allEventController.association = association
        allEventController.evenementsMock = evenementsMock ?? nil
        return allEventController
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let uiTestMode =  ProcessInfo.processInfo.arguments.contains("testMode")
        if uiTestMode {
            if uiTestMode && ProcessInfo.processInfo.arguments.contains("creerEvent"){
                print("test créé")
                self.gestionnaireAssociation = GestionnaireAssociationsMock.getOneGestionnaireAssociation()
                self.leftBtnNavBar.title = self.gestionnaireAssociation.mail

                self.association = AssociationMock.getOneAssociationMock()
                self.rightBtnNavBar.title = self.association.nom

            }
            if ProcessInfo.processInfo.arguments.contains("deleteEvent"){
                self.gestionnaireAssociation = GestionnaireAssociationsMock.getOneGestionnaireAssociation()
                self.leftBtnNavBar.title = self.gestionnaireAssociation.mail

                self.association = AssociationMock.getOneAssociationMock()
                self.rightBtnNavBar.title = self.association.nom

            }
        }else{
            if(self.gestionnaireAssociation != nil) {
                print("fail1")
                self.gestionnaireService.GetOneGA(gestionnaireAssociationId: self.gestionnaireAssociation.id!) { gestionnaireFromAPI in
                    DispatchQueue.main.async {
                        self.gestionnaireAssociation = gestionnaireFromAPI
                        print(self.gestionnaireAssociation.description)
                        self.leftBtnNavBar.title = self.gestionnaireAssociation.mail
                        self.associationService.getAssofromGA(gestionnaireAssociationId: self.gestionnaireAssociation.id!) { associationFromApi in
                            DispatchQueue.main.async {
                                self.association = associationFromApi
                                print(self.association.description)
                                self.rightBtnNavBar.title = self.association.nom
                            }
                        }
                    }
                }
            }
        }



        self.title = "Vos événements"
        if ((gestionnaireAssociation) != nil) {
            self.leftBtnNavBar.title = gestionnaireAssociation.mail
        }

        if ((gestionnaireAssociation) != nil) {
            self.rightBtnNavBar.title = association.nom
        }


        self.evenementTableView.dataSource = self
        self.evenementTableView.delegate = self
        self.evenementTableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "evenement-cell")



        print(self.evenements.count)

        self.navigationItem.rightBarButtonItems = [
            UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(handleAddEvent))
        ]

    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)

        let uiTestMode =  ProcessInfo.processInfo.arguments.contains("testMode")
        if uiTestMode {
            if ProcessInfo.processInfo.arguments.contains("deleteEvent"){
                print(self.evenements)
                self.evenements = (self.evenementsMock?.getListEvenements())!
                print(self.evenements)
                self.evenementTableView.reloadData()
            }
            if ProcessInfo.processInfo.arguments.contains("creerEvent"){
                print("blzbla")
                self.evenements = (self.evenementsMock?.getListEvenements())!
                print("evenements : \(self.evenements)")
                //self.evenementTableView.reloadData()
            }
        }else{
            self.evenementService.getAllEventOfAsso(assoid: self.association.id!) { (evenementsFromAPI) in
                self.evenements = evenementsFromAPI
                DispatchQueue.main.sync {
                    print(self.evenements)
                    self.evenementTableView.reloadData()
                }
            }
        }

    }


    @IBAction func goToProfileGA(_ sender: Any) {
        let controller = ProfilGAViewController.newInstance(gestionnaireAssociation: self.gestionnaireAssociation, association: self.association, gaMock: self.gaMock)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func goToProfileAsso(_ sender: Any) {
        print("ok")
        let profilAssoController = ProfilAssoViewController.newInstance(gestionnaireAssociation: self.gestionnaireAssociation,association: self.association, associationMock: self.associationMock)
        self.navigationController?.pushViewController(profilAssoController, animated: true)
    }

    @objc func handleAddEvent() {
        let controller = CreerEvenementViewController.newInstanceCreateTest(gestionnaireAssociation:self.gestionnaireAssociation, association: self.association, evenementsMock: evenementsMock)
        self.navigationController?.pushViewController(controller, animated: true)
    }

    @objc func handleEditList() {
        self.evenementTableView.isEditing = !self.evenementTableView.isEditing
    }

}

extension TousEvenementsViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.evenements.count//self.evenements.count A remplacer
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let evenement = self.evenements[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "evenement-cell",for: indexPath) as! EventsTableViewCell
        //cell.textLabel?.text = evenement.titre//evenement.titre A remplacer

        print(evenement.description)
        cell.titleLabel.text = evenement.titre
        cell.dateLabel.text = evenement.dateDebut
        cell.adresseLabel.text = evenement.adresse
        cell.besoinLabel.text = String(evenement.besoin ?? 0)
        cell.nombreParticipant.text = String(evenement.nombreParticipants ?? 0)
        cell.miniDescrip.text = evenement.info?.maxLength(length: 150)



        let boldSize = CGFloat(17.0)
        cell.titleLabel.font = UIFont.boldSystemFont(ofSize: boldSize)

        return cell
    }
}


@IBDesignable
    class DesignableSegmentControl: UISegmentedControl{
    }
    extension UISegmentedControl{
        @IBInspectable
        var textColor: UIColor{
            get {
                return self.textColor
            }
            set {
                let unselectedAttributes = [NSAttributedString.Key.foregroundColor: newValue,
                                            NSAttributedString.Key.font:  UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.regular)]
                self.setTitleTextAttributes(unselectedAttributes, for: .normal)
                self.setTitleTextAttributes(unselectedAttributes, for: .selected)
            }
        }
    }

extension String {
   func maxLength(length: Int) -> String {
       var str = self
       let nsString = str as NSString
       if nsString.length >= length {
           str = nsString.substring(with:
               NSRange(
                location: 0,
                length: nsString.length > length ? length : nsString.length)
           )
       }
       return  str
   }
}

extension TousEvenementsViewController: UITableViewDelegate {


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let evenement = self.evenements[indexPath.row] // recuperer
        let controller = CreerEvenementViewController.newInstanceEdit(evenement: evenement, gestionnaireAsso: self.gestionnaireAssociation, association: self.association , evenementsMock: self.evenementsMock)
            self.navigationController?.pushViewController(controller, animated: true)
        }
}


