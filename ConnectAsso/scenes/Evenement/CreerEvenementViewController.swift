//
//  CreerEvenementViewController.swift
//  ConnectAsso
// F7F7F0
//  Created by Luc Coujonde on 06/06/2021.
//

import UIKit

class CreerEvenementViewController: UIViewController, UITextViewDelegate {

    var evenementService : EvenementService = EvenementService()
    var gestionnaireAssociation : GestionnaireAssociation!
    var association : Association!
    var evenement : Evenement!
    var evenementToEdit : Evenement!
    var dateDeDebutPicked = false
    var participantsInscrits = "participantsInscrits"
    var participantsEnAttentes = "participantsEnAttentes"
    var evenementsMock: EvenementsMock?
    var isMockMod : Bool?
    //

    @IBOutlet var nomEvenementField: SDCTextField!
    @IBOutlet var adresseField: SDCTextField!
    @IBOutlet var infoTextView: UITextView!
    @IBOutlet var dateDebutPicker: UIDatePicker!
    @IBOutlet var dateFinPicker: UIDatePicker!
    @IBOutlet var dateDebutField: UITextField!
    @IBOutlet var dateFinField: UITextField!
    @IBOutlet var adresseLenghtMaxLabel: UILabel!
    @IBOutlet var nomEvenementLenghtMaxLabel: UILabel!
    @IBOutlet var infoLenghtMaxLabel: UILabel!
    @IBOutlet var confirmerButton: UIButton!
    @IBOutlet var organisateurField: SDCTextField!
    @IBOutlet var besoinNumbField: UITextField!
    // a cacher si evenment == nil
    @IBOutlet var nombreDeParticipantsInscritsField: SDCTextField!
    @IBOutlet var nombreDeParticipantsInscritsLabel: UILabel!
    @IBOutlet var participantsEnAttenteBtn: UIButton!
    @IBOutlet var participantsInscritsBtn: UIButton!



    var format : StylesAndFormat = StylesAndFormat()
    var lineUnderMyTextField : StylesAndFormat = StylesAndFormat()

    let currentDate = Date()
    var dateDebutOrigin = Date()
    var dateFinOrigin = Date()
    let limitLenghtBeginSentence = "limité à "
    let limitLenghtEndSentence = " caractères"
    var eventType = ""

    let limitLenghtOfNomEvenementField = 30
    let limitLenghtOfadresseField = 60
    let limitLenghtOfInfoField = 2000

    var isDateDebutPicker = false
    var particpantsIncr = 0

    static func newInstanceEdit(evenement  : Evenement, gestionnaireAsso : GestionnaireAssociation, association : Association, evenementsMock: EvenementsMock?) -> CreerEvenementViewController {
        let controller = CreerEvenementViewController()
        controller.evenementToEdit = evenement
        controller.association = association
        controller.gestionnaireAssociation = gestionnaireAsso
        controller.evenementsMock = evenementsMock
        return controller
    }

    static func newInstance(gestionnaireAssociation : GestionnaireAssociation, association : Association) ->CreerEvenementViewController {
        let createViewController = CreerEvenementViewController()
        createViewController.gestionnaireAssociation = gestionnaireAssociation
        createViewController.association = association
        return createViewController
    }

    static func newInstanceCreateTest(gestionnaireAssociation : GestionnaireAssociation, association : Association, evenementsMock: EvenementsMock?) ->CreerEvenementViewController {
        let createViewController = CreerEvenementViewController()
        createViewController.gestionnaireAssociation = gestionnaireAssociation
        createViewController.association = association
        createViewController.evenementsMock = evenementsMock
        return createViewController
    }



    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Créer un Evenement"
        print("ici")
        print(ProcessInfo.processInfo.arguments.contains("creerEvent"))

        self.infoTextView.delegate = self
        self.nomEvenementField.delegate = self
        self.adresseField.delegate = self
        self.dateDebutField.delegate = self
        self.setOrganisateurMail()


        //hide every label by default
        self.nomEvenementLenghtMaxLabel.isHidden = true
        self.adresseLenghtMaxLabel.isHidden = true
        self.infoLenghtMaxLabel.isHidden = true
        self.besoinNumbField.delegate = self


        //content of all limitLenghtLabel
        self.nomEvenementLenghtMaxLabel.text = "\(limitLenghtBeginSentence)\(limitLenghtOfNomEvenementField)\(limitLenghtEndSentence)"
        self.adresseLenghtMaxLabel.text = "\(limitLenghtBeginSentence)\(limitLenghtOfadresseField)\(limitLenghtEndSentence)"
        self.infoLenghtMaxLabel.text = "\(limitLenghtBeginSentence)\(limitLenghtOfInfoField)\(limitLenghtEndSentence)"


        self.styleOnTextField()

        var contentInset = self.infoTextView.contentInset
        contentInset.left = 2
        contentInset.top = 2
        contentInset.right = 2
        self.infoTextView.contentInset = contentInset

        //maxLenght to textField
        self.nomEvenementField.maxLength = limitLenghtOfNomEvenementField
        self.adresseField.maxLength = limitLenghtOfadresseField
        self.hiddenItems()
        if (self.evenementToEdit != nil) {
            self.editExistingEvent()
            self.title = "Editer un Evenement"
            if(self.evenementToEdit.statut == "annule" || self.evenementToEdit.statut == "termine"){
                self.navigationItem.rightBarButtonItems = [
                    UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(nada))
                ]
            }
            self.navigationItem.rightBarButtonItems = [
                UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteEvent))
            ]
        }

        if(self.evenementToEdit != nil) {
            if (self.evenementToEdit.statut == "annule" ) {
                self.title = "Annule"
                self.participantsEnAttenteBtn.isHidden = true
                self.participantsInscritsBtn.isHidden = true
            } else if (self.evenementToEdit.statut == "termine") {

                self.title = "Terminé"
                self.participantsEnAttenteBtn.isHidden = true
            }
        }

    }

    @objc func deleteEvent() {
        let uiTestMode =  ProcessInfo.processInfo.arguments.contains("testMode")
                if uiTestMode {
                    if ProcessInfo.processInfo.arguments.contains("deleteEvent"){
                        self.evenementsMock?.deleteEvenementById(idEvenementToDelete: self.evenementToEdit.id!)
                        let controller = TousEvenementsViewController.newInstance(gestionnaireAssociation: self.gestionnaireAssociation, association: self.association, evenementsMock: self.evenementsMock, gaMock: nil)
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                }else{
                    self.evenementService.delete(id: self.evenementToEdit.id!) { Bool in
                        DispatchQueue.main.sync {
                            let controller = TousEvenementsViewController.newInstance(gestionnaireAssociation: self.gestionnaireAssociation, association: self.association, evenementsMock: nil, gaMock: nil)
                            self.navigationController?.pushViewController(controller, animated: true)
                        }
                    }
                }

    }

    @objc func nada() {
     //to delete a event
    }


    func hiddenItems() {
        if (self.evenementToEdit != nil) {
            self.nombreDeParticipantsInscritsLabel.isHidden = false
            self.participantsEnAttenteBtn.isHidden = false
            self.participantsInscritsBtn.isHidden = false
            self.nombreDeParticipantsInscritsField.isHidden = false
        } else {
            self.nombreDeParticipantsInscritsLabel.isHidden = true
            self.participantsEnAttenteBtn.isHidden = true
            self.participantsInscritsBtn.isHidden = true
            self.nombreDeParticipantsInscritsField.isHidden = true
        }
    }

    func editExistingEvent() {

        evenementService.getEvenementInfo(id: self.evenementToEdit.id!) { evenementToEditFromAPI in
            DispatchQueue.main.async {
                guard let eventData = evenementToEditFromAPI else{
                    return
                }
                //print("eventData : \(eventData)")
                //print("evenementToEditFromAPI!.dateDebut " + eventData.dateDebut!);
                //print("evenementToEditFromAPI!.dateFin " + eventData.dateFin!);
                self.nomEvenementField.text = eventData.titre
                self.adresseField.text = eventData.adresse
                self.infoTextView.text = eventData.info
                //self.convertStringInData(dateDebut: eventData.dateDebut!, dateFin: eventData.dateFin!)
                self.manageDates(dateDebut: eventData.dateDebut!, dateFin: eventData.dateFin!)

//                cell.dateLabel.text = evenement.dateDebut
//                cell.dateLabel.text = evenement.dateDebut
                self.besoinNumbField.text = String(eventData.besoin!)
                self.organisateurField.text = self.gestionnaireAssociation.mail
                self.nombreDeParticipantsInscritsField.text = String(eventData.nombreParticipants!)
                print( "evenementid : \(eventData.nombreParticipants!)")

            }
        }
    }


    override func viewWillAppear(_ animated: Bool) {
        if(self.evenementToEdit != nil) {
            self.editExistingEvent()
        }

        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }


    func manageDates(dateDebut: String, dateFin: String){
        let datesData = self.getConvertedStringDateInData(dateDebut: dateDebut, dateFin: dateFin)
        setDatesInFieldsAndPickers(datesData: datesData)
    }

    func getConvertedStringDateInData(dateDebut: String, dateFin: String) -> [String : Date]{

        let stringDateDebut = dateDebut
        let stringDateFin = dateFin

        let formatterStringToDate = DateFormatter()
        formatterStringToDate.dateFormat = "dd-MM-yyyy HH:mm"

        let dateDebut = formatterStringToDate.date(from: stringDateDebut)
        let dateFin = formatterStringToDate.date(from: stringDateFin)

        let datesData: [String : Date] = ["dateDebut": dateDebut!, "dateFin": dateFin!]
        return datesData
    }

    func setDatesInFieldsAndPickers(datesData: [String : Date]){
        self.dateDebutPicker.date = datesData["dateDebut"]!
        self.dateFinPicker.date = datesData["dateFin"]!

        if (self.dateDeDebutPicked == false) {
            let dateFormatterShort = DateFormatter()

            dateFormatterShort.dateStyle = DateFormatter.Style.short
            dateFormatterShort.timeStyle = DateFormatter.Style.short
            print("date de debut du picker \(dateFormatterShort)")
            let dateDebutString = dateFormatterShort.string(from: self.dateDebutPicker.date)
            let dateFinString = dateFormatterShort.string(from: self.dateFinPicker.date)
            dateDebutField.text = dateDebutString
            dateFinField.text = dateFinString
        }
    }

    //290
    func compareNewEventDateWithAllEventDateExisting(newEventDate : Date , allEventDateExisting : [Date]) ->Bool {
        print("newEventDate \(newEventDate)")
        for elements in allEventDateExisting {
            print("dates  \(elements)")
            if (elements <= newEventDate && elements >= newEventDate) {
                print("evenement avec une même date trouvé êtes vous sur de vouloir créér 2 évenements à la même date ?")
                return true
            } else {
                print("pas d'autre événement à cette date")
            }
        }
        return false
    }


    func setOrganisateurMail(){
        if(gestionnaireAssociation != nil) {
            self.organisateurField.text = self.gestionnaireAssociation.mail
        }
    }


    func styleOnTextField() {
        lineUnderMyTextField.lineUnderTextField(textField: nomEvenementField)
        lineUnderMyTextField.lineUnderTextField(textField: adresseField)
        lineUnderMyTextField.lineUnderTextField(textField: organisateurField)
        lineUnderMyTextField.lineUnderTextField(textField: besoinNumbField)
        lineUnderMyTextField.lineUnderTextField(textField: nombreDeParticipantsInscritsField)
    }

    //hidden keyboard and every limit lenght label
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.nomEvenementLenghtMaxLabel.isHidden = true
        self.adresseLenghtMaxLabel.isHidden = true
        self.infoLenghtMaxLabel.isHidden = true
    }



    func compareDateDebutAndDateDeFin(dateDebut : Date, dateFin : Date) -> Bool {
        return dateDebut <= dateFin
    }

    func compareDateDebutAndDateDeDebutDefaut(dateDebut : Date, dateDefaut : Date) -> Bool {
        return dateDebut <= dateDefaut
    }

    func compareDateDebutAndDateDeDebutDefaut(datefin : Date, dateDefaut : Date) -> Bool {
        return datefin <= dateDefaut
    }



//    func compareDateDebutAndDefautDate()

    @IBAction func goToAttente(_ sender: Any) {
        if ProcessInfo.processInfo.arguments.contains("creerEvent") {
            print("test")
            let controller = ParticipantsListViewController.newInstanceEnAttenteTEST(gestionnaireAssociation: self.gestionnaireAssociation, association: self.association, evenement: self.evenementToEdit, participantType: self.participantsEnAttentes, participantsEnAttenteMock: ParticipantsMocks())
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
        let controller = ParticipantsListViewController.newInstance(gestionnaireAssociation: self.gestionnaireAssociation, association: self.association, evenement: self.evenementToEdit, participantType: self.participantsEnAttentes)
        self.navigationController?.pushViewController(controller, animated: true)
        }
    }

    @IBAction func goToinscrits(_ sender: Any) {
        if ProcessInfo.processInfo.arguments.contains("creerEvent") {
            print("test")
            let controller = ParticipantsListViewController.newInstanceInscritTEST(gestionnaireAssociation: self.gestionnaireAssociation, association: self.association, evenement: self.evenementToEdit, participantType: self.participantsInscrits, participantsInscript: ParticipantsMocks())
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
        let controller = ParticipantsListViewController.newInstance(gestionnaireAssociation: self.gestionnaireAssociation, association: self.association, evenement: self.evenementToEdit, participantType: self.participantsEnAttentes)
        self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    @IBAction func handleSubmit(_ sender: Any) {
        print(" ici   \(self.evenement)? ")

        if (evenementToEdit != nil) {
            let titre = self.nomEvenementField.text ?? ""
            let adresse = self.adresseField.text ?? ""
            let info = self.infoTextView.text ?? ""
            let dateDebut = self.dateDebutField.text ?? ""
            let dateFin = self.dateFinField.text ?? ""
            let besoin = Int(self.besoinNumbField.text ?? "0")
            let organisateur = self.organisateurField.text ?? ""
            let nombreDeParticipants = self.nombreDeParticipantsInscritsField.text ?? ""
            print("date de debut du handle : " + dateDebut)
            print("date de debut du handle : " + dateFin)


            self.evenementToEdit = Evenement(id: self.evenementToEdit.id!, titre: titre, dateDebut: dateDebut, dateFin: dateFin, info: info, adresse: adresse, besoin: besoin, organisateur: organisateur, complet: false, nombreParticipants: Int(nombreDeParticipants), statut: self.evenementToEdit.statut)
            if (self.evenementToEdit != nil) {
                print("evenement to edit : \(evenementToEdit.description)")
                self.evenementService.edit(evenementId: self.evenementToEdit.id!, evenementToEdit: evenementToEdit) { evenementFromApi in
                    DispatchQueue.main.sync {
                        if (evenementFromApi != nil) {
                            let controller = TousEvenementsViewController.newInstance(gestionnaireAssociation: self.gestionnaireAssociation, association: self.association, evenementsMock: nil, gaMock: nil)
                            self.navigationController?.pushViewController(controller, animated: true)
                        } else {
                            let alert = UIAlertController(title: "Erreur", message: "Erreur Evenement non créé", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
                            self.present(alert, animated: true)
                        }
                    }
                }
            }
        }
        if (self.evenement == nil && ProcessInfo.processInfo.arguments.contains("creerEvent")){

            let titre = self.nomEvenementField.text ?? ""
            let adresse = self.adresseField.text ?? ""
            let info = self.infoTextView.text ?? ""
            let dateDebut = self.dateDebutField.text ?? ""
            let dateFin = self.dateFinField.text ?? ""
            let besoin = Int(self.besoinNumbField.text ?? "0")
            let organisateur = self.organisateurField.text ?? ""
            let nombreDeParticipants = Int(self.nombreDeParticipantsInscritsField.text ?? "")
            print("date de debut du handle : " + dateDebut)
            print("date de debut du handle : " + dateFin)

            let newEvenement = Evenement(id: nil, titre: titre, dateDebut: dateDebut, dateFin: dateFin, info: info, adresse: adresse, besoin: besoin, organisateur: organisateur, complet: false, nombreParticipants: nombreDeParticipants, statut: nil)

            self.evenementsMock?.createEvenement(newEvent: newEvenement) // tab + event
            let controller = TousEvenementsViewController.newInstance(gestionnaireAssociation: self.gestionnaireAssociation, association: self.association, evenementsMock: self.evenementsMock, gaMock: nil)
            self.navigationController?.pushViewController(controller, animated: true)


            //self.association.id!
        }
        
        if (self.evenement == nil && !ProcessInfo.processInfo.arguments.contains("creerEvent")) {
            
            let titre = self.nomEvenementField.text ?? ""
            let adresse = self.adresseField.text ?? ""
            let info = self.infoTextView.text ?? ""
            let dateDebut = self.dateDebutField.text ?? ""
            let dateFin = self.dateFinField.text ?? ""
            let besoin = Int(self.besoinNumbField.text ?? "0")
            let organisateur = self.organisateurField.text ?? ""
            let nombreDeParticipants = Int(self.nombreDeParticipantsInscritsField.text ?? "")
            print("date de debut du handle : " + dateDebut)
            print("date de debut du handle : " + dateFin)

            let evenement = Evenement(id: nil, titre: titre, dateDebut: dateDebut, dateFin: dateFin, info: info, adresse: adresse, besoin: besoin, organisateur: organisateur, complet: false, nombreParticipants: nombreDeParticipants, statut: nil)

            
            self.evenementService.create(associationId: self.association.id!, evenement: evenement) { evenementFromAPI in
                DispatchQueue.main.sync {
                    if (evenementFromAPI != nil) {
                        let controller = TousEvenementsViewController.newInstance(gestionnaireAssociation: self.gestionnaireAssociation, association: self.association, evenementsMock: nil, gaMock: nil)
                        self.navigationController?.pushViewController(controller, animated: true)
                    } else {
                        let alert = UIAlertController(title: "Erreur", message: "Erreur Evenement non créé", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel))
                        self.present(alert, animated: true)
                    }
                }
            }
        }
//
//        if (evenementToEdit != nil && !ProcessInfo.processInfo.arguments.contains("creerEvent")) {
//            let titre = self.nomEvenementField.text ?? ""
//            let adresse = self.adresseField.text ?? ""
//            let info = self.infoTextView.text ?? ""
//            let dateDebut = self.dateDebutField.text ?? ""
//            let dateFin = self.dateFinField.text ?? ""
//            let besoin = Int(self.besoinNumbField.text ?? "0")
//            let organisateur = self.organisateurField.text ?? ""
//            let nombreDeParticipants = self.nombreDeParticipantsInscritsField.text ?? ""
//            print("date de debut du handle : " + dateDebut)
//            print("date de debut du handle : " + dateFin)
//
//
//            self.evenementToEdit = Evenement(id: self.evenementToEdit.id!, titre: titre, dateDebut: dateDebut, dateFin: dateFin, info: info, adresse: adresse, besoin: besoin, organisateur: organisateur, complet: false, nombreParticipants: Int(nombreDeParticipants), statut: self.evenementToEdit.statut)
//            if (self.evenementToEdit != nil) {
//                print("evenement to edit : \(evenementToEdit.description)")
//                self.evenementService.edit(evenementId: self.evenementToEdit.id!, evenementToEdit: evenementToEdit) { evenementFromApi in
//                    DispatchQueue.main.sync {
//                        if (evenementFromApi != nil) {
//                            let controller = TousEvenementsViewController.newInstance(gestionnaireAssociation: self.gestionnaireAssociation, association: self.association, evenementsMock: nil)
//                            self.navigationController?.pushViewController(controller, animated: true)
//                        } else {
//                            let alert = UIAlertController(title: "Erreur", message: "Erreur Evenement non créé", preferredStyle: .alert)
//                            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
//                            self.present(alert, animated: true)
//                        }
//                    }
//                }
//            }
//
//        } else {
//            let titre = self.nomEvenementField.text ?? ""
//            let adresse = self.adresseField.text ?? ""
//            let info = self.infoTextView.text ?? ""
//            let dateDebut = self.dateDebutField.text ?? ""
//            let dateFin = self.dateFinField.text ?? ""
//            let besoin = Int(self.besoinNumbField.text ?? "0")
//            let organisateur = self.organisateurField.text ?? ""
//            print("date de debut du handle : " + dateDebut)
//            print("date de debut du handle : " + dateFin)
//
//            guard titre.count > 0,
//                  adresse.count > 0,
//                  info.count > 0,
//                  dateDebut.count > 0,
//                  dateFin.count > 0
//            else {
//                let alert = UIAlertController(title: "Erreur", message: "Veuillez remplir tous les champs", preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "OK", style: .cancel))
//                self.present(alert, animated: true)
//                return
//            }
//            print(!compareDateDebutAndDateDeFin(dateDebut: self.dateDebutOrigin, dateFin: self.dateFinOrigin))
//
//            if(compareDateDebutAndDateDeDebutDefaut(dateDebut: self.dateDebutOrigin, dateDefaut: self.currentDate)){
//                let alert = UIAlertController(title: "Erreur", message: "La date de début doit être ultérieur à la date d'aujourd'hui", preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "OK", style: .cancel))
//                self.present(alert, animated: true)
//                return
//            }
//
//            if(!compareDateDebutAndDateDeFin(dateDebut: self.dateDebutOrigin, dateFin: self.dateFinOrigin)) {
//                let alert = UIAlertController(title: "Erreur", message: "La date de Fin doit être ultérieur à la date du début", preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "OK", style: .cancel))
//                self.present(alert, animated: true)
//                return
//            }
//
//            let evenement = Evenement(id: nil, titre: titre, dateDebut: dateDebut, dateFin: dateFin, info: info, adresse: adresse, besoin: besoin, organisateur: organisateur, complet: false, nombreParticipants: 0, statut: nil)
//
//    //        let detailsEvenement = DetailsEvenementViewController.newInstance(evenement: evenement)
//    //        self.navigationController?.pushViewController(detailsEvenement, animated: true)
//
//
//            //self.association.id!
//
//            if (self.evenement == nil) {
//                self.evenementService.create(associationId: self.association.id!, evenement: evenement) { evenementFromAPI in
//                    DispatchQueue.main.sync {
//                        if (evenementFromAPI != nil) {
//                            let controller = TousEvenementsViewController.newInstance(gestionnaireAssociation: self.gestionnaireAssociation, association: self.association, evenementsMock: nil)
//                            self.navigationController?.pushViewController(controller, animated: true)
//                        } else {
//                            let alert = UIAlertController(title: "Erreur", message: "Erreur Evenement non créé", preferredStyle: .alert)
//                            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
//                            self.present(alert, animated: true)
//                        }
//                    }
//                }
//            }
//        }
    }
    //nomEvenementDisplayLimitLenght fin de l'edition
//    @IBAction func nomEvenementDisplayLimitLenght(_ sender: Any) {
//        self.nomEvenementLenghtMaxLabel.isHidden = false
//    }
    //debut de l'edition
//    @IBAction func nomEvenementHiddenLimitLenght(_ sender: Any) {
//        self.nomEvenementLenghtMaxLabel.isHidden = true
//    }


    @IBAction func nomEvenementDisplayLimitLenght(_ sender: Any) {
        self.nomEvenementLenghtMaxLabel.isHidden = false
        self.adresseLenghtMaxLabel.isHidden = true
        self.infoLenghtMaxLabel.isHidden = true
    }

    @IBAction func adresseEvenementDisplayLimitLenght(_ sender: Any) {
        self.adresseLenghtMaxLabel.isHidden = false
        self.nomEvenementLenghtMaxLabel.isHidden = true
        self.infoLenghtMaxLabel.isHidden = true
    }


    @IBAction func dateDebutPickerChanged(_ sender: Any){

        self.dateDebutOrigin = self.dateDebutPicker.date
        print("date debut \(dateDebutOrigin)")
        let dateDebutFormatter = DateFormatter()

        dateDebutFormatter.dateStyle = DateFormatter.Style.short
        dateDebutFormatter.timeStyle = DateFormatter.Style.short
        self.isDateDebutPicker = true
        print("date de debut du picker \(dateDebutFormatter)")
        let dateDebutString = dateDebutFormatter.string(from: self.dateDebutPicker.date)
        dateDebutField.text = dateDebutString
        self.dateDeDebutPicked = true
    }

//    func dateToString(dateFormatter : DateFormatter, date : Date) -> String {
//        return dateFormatter.string(from: date)
//    }


    @IBAction func dateFinPickerChanged(_ sender: Any) {
        self.dateFinOrigin = self.dateDebutPicker.date
        let dateFinFormatter = DateFormatter()
        dateFinFormatter.dateStyle = DateFormatter.Style.short
        dateFinFormatter.timeStyle = DateFormatter.Style.short

        let dateFinString = dateFinFormatter.string(from: dateFinPicker.date)
        dateFinField.text = dateFinString
        self.dateDeDebutPicked = true
    }



}


var dateDebutFormatter = DateFormatter()
var dateFinFormatter = DateFormatter()

extension CreerEvenementViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField : UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == self.nomEvenementField {
            self.adresseField.becomeFirstResponder()
        } else if (textField == adresseField) {
            self.infoTextView.becomeFirstResponder()
        }
        return true
    }

    func textFieldNumb(_ besoinNumbField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let allowedCharacters = "1234567890"
        let allowedCharcterSet = CharacterSet(charactersIn: allowedCharacters)
        let typedCharcterSet = CharacterSet(charactersIn: string)
        return allowedCharcterSet.isSuperset(of: typedCharcterSet)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let sdcTextField = textField as? SDCTextField {
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }
        return true
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        self.infoLenghtMaxLabel.isHidden = false
        self.nomEvenementLenghtMaxLabel.isHidden = true
        self.adresseLenghtMaxLabel.isHidden = true
        return updatedText.count <= limitLenghtOfInfoField //limit of infoTextView
    }

}



