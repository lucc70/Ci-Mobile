//
//  SignalerViewController.swift
//  ConnectAsso
// F7F7F0
//  Created by Luc Coujonde on 13/05/2021.
//

import UIKit

class ProfilGAViewController: UIViewController {
    var gestionnaireAssociation : GestionnaireAssociation!
    var association : Association!
    //var gestionnaireAssociationEdited : GestionnaireAssociation = GestionnaireAssociation()
    var lineUnderMyTextField : StylesAndFormat = StylesAndFormat()
    var gestionnaireAssoService : GestionnaireAssoService = GestionnaireAssoService()
    var gaMock: GestionnaireAssociationsMock?

    @IBOutlet var nomTextField: UITextField!
    @IBOutlet var prenomTextField: UITextField!
    @IBOutlet var mailTextField: UITextField!
    @IBOutlet var mdpTextField: UITextField!
    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var sauvegarderBtn: UIButton!
    @IBOutlet var annulerBtn: UIButton!

    //
    static func newInstance(gestionnaireAssociation : GestionnaireAssociation, association : Association, gaMock: GestionnaireAssociationsMock?) ->ProfilGAViewController {
        let profileController = ProfilGAViewController()
        profileController.gestionnaireAssociation = gestionnaireAssociation
        profileController.association = association
        profileController.gaMock = gaMock
        return profileController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.styleOnTextField()

        self.title = "Profil"

        if (gestionnaireAssociation != nil) {
            self.nomTextField.text = self.gestionnaireAssociation.nom
            self.prenomTextField.text = self.gestionnaireAssociation.prenom
            self.mailTextField.text = self.gestionnaireAssociation.mail
            self.mdpTextField.text = self.gestionnaireAssociation.motDePasse
            self.descriptionTextView.text = self.gestionnaireAssociation.descriptionPersonnelle
        }


        // Do any additional setup after loading the view.
    }

    func styleOnTextField() {
        lineUnderMyTextField.lineUnderTextField(textField: self.nomTextField)
        lineUnderMyTextField.lineUnderTextField(textField: self.prenomTextField)
        lineUnderMyTextField.lineUnderTextField(textField: self.mailTextField)
        lineUnderMyTextField.lineUnderTextField(textField: self.mdpTextField)
    }

    func checkAllField() -> Bool {
        if (self.nomTextField.text == "" ||
            self.prenomTextField.text == "" ||
            self.mailTextField.text == "" ||
            self.mdpTextField.text == "" ||
            self.nomTextField.text == "" ||
            self.descriptionTextView.text == "") {
            let alert = UIAlertController(title: "Erreur", message: "Veuillez remplir tous les champs", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
            self.present(alert, animated: true)
            return false
        } else if (self.nomTextField.text == self.gestionnaireAssociation.nom &&
                   self.prenomTextField.text == self.gestionnaireAssociation.prenom &&
                   self.mailTextField.text == self.gestionnaireAssociation.mail &&
                   self.mdpTextField.text == self.gestionnaireAssociation.motDePasse &&
                   self.descriptionTextView.text == self.gestionnaireAssociation.descriptionPersonnelle){
            let alert = UIAlertController(title: "Infos", message: "Aucune modification détecté", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
            self.present(alert, animated: true)
            return false
        } else {
            return true
        }
    }

    func editAGestionnaireAsso() -> GestionnaireAssociation {
        let gestionnaireAssociationEdited : GestionnaireAssociation = GestionnaireAssociation(id: self.gestionnaireAssociation.id, nom: self.nomTextField.text!, prenom: self.prenomTextField.text!, motDePasse: self.mdpTextField.text!, mail: self.mailTextField.text!, descriptionPersonnelle: self.descriptionTextView.text!)
        self.gestionnaireAssociation = gestionnaireAssociationEdited
        return gestionnaireAssociation
    }

    @IBAction func editProfileGA(_ sender: Any) {
        if (self.checkAllField()) {
            let uiTestMode =  ProcessInfo.processInfo.arguments.contains("testMode")
            if uiTestMode {
                if ProcessInfo.processInfo.arguments.contains("editGA"){
                    self.gestionnaireAssociation = self.gaMock?.getGestionnaireAssociation()
                    self.gaMock?.editGestionnaireAssociation(newGA: self.editAGestionnaireAsso())
                    let controller = TousEvenementsViewController.newInstance(gestionnaireAssociation: self.gestionnaireAssociation, association: self.association, evenementsMock: nil, gaMock: self.gaMock)

                    self.navigationController?.pushViewController(controller, animated: true)
                    let alert = UIAlertController(title: "Update", message: "Vos données ont été mise à jour", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel))
                    self.present(alert, animated: true)
                }
            }else{
                self.gestionnaireAssoService.edit(gestionnaireAssociationId: self.gestionnaireAssociation.id!, gestionnaireAssociation: self.editAGestionnaireAsso()) { gestionnaireAssociationFromAPI in
                    DispatchQueue.main.async {
                        self.gestionnaireAssociation = gestionnaireAssociationFromAPI
                        print("GA from api after edit : \(gestionnaireAssociationFromAPI!.description)")

                        let controller = TousEvenementsViewController.newInstance(gestionnaireAssociation: self.gestionnaireAssociation, association: self.association, evenementsMock: nil, gaMock: self.gaMock)

                        self.navigationController?.pushViewController(controller, animated: true)
                        let alert = UIAlertController(title: "Update", message: "Vos données ont été mise à jour", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel))
                        self.present(alert, animated: true)
                    }
                }
            }

        }
    }

//    @IBAction func backToEventsList(_ sender: Any) {
//        self.navigationController?.pushViewController(TousEvenementsViewController(), animated: true)
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProfilGAViewController: UITextFieldDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        //self.textCountLabel.text = "\(updatedText.count) / 700"
        return updatedText.count <= 2000 //limit of infoTextView
    }

}


