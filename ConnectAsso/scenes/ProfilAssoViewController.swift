//
//  HomeViewController.swift
//  ConnectAsso
// F7F7F0
//  Created by Luc Coujonde on 06/06/2021.
//

import UIKit

class ProfilAssoViewController: UIViewController {

    var gestionnaireAssociation: GestionnaireAssociation!
    var association: Association!
    var associationMock: AssociationMock?
    var associationService: AssociationService = AssociationService()
    var lineUnderMyTextField: StylesAndFormat = StylesAndFormat()

    @IBOutlet var nomAssoTextField: UITextField!
    @IBOutlet var descripAssoTextView: UITextView!
    @IBOutlet var dateAssoPicker: UIDatePicker!
    @IBOutlet var saveBtn: UIButton!

    static func newInstance(gestionnaireAssociation : GestionnaireAssociation, association : Association?, associationMock : AssociationMock?) ->ProfilAssoViewController {
        let allEventController = ProfilAssoViewController()
        allEventController.gestionnaireAssociation = gestionnaireAssociation
        allEventController.association = association
        allEventController.associationMock = associationMock
        return allEventController
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Association"
        self.styleOnTextField()

        if (association != nil) {
            if ProcessInfo.processInfo.arguments.contains("creerEvent") {
                self.association = self.associationMock?.getAssociationMock()
            }
            print("avant")

            self.nomAssoTextField.text = self.association.nom
            self.descripAssoTextView.text = self.association.info
            print("après")

            self.associationMock?.printTestAssoMock()
        }


        }

    func styleOnTextField() {
        lineUnderMyTextField.lineUnderTextField(textField: self.nomAssoTextField)
    }

    func checkAllField()->Bool {
        if (self.nomAssoTextField.text == "" ||
            self.descripAssoTextView.text == "") {
            let alert = UIAlertController(title: "Erreur", message: "Veuillez remplir tous les champs", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
            self.present(alert, animated: true)
            return false
        } else if (self.nomAssoTextField.text == self.association.nom &&
                   self.descripAssoTextView.text == self.association.info){
            let alert = UIAlertController(title: "Infos", message: "Aucune modification détecté", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
            self.present(alert, animated: true)
            return false
        } else {
            return true
        }
    }

    func editAssociation() -> Association {
        let associationEdited = Association(id: self.association.id, nom: self.nomAssoTextField.text, info: self.descripAssoTextView.text, dateCreation: self.association.dateCreation)
        self.association = associationEdited
        return association
    }


    @IBAction func editProfileAsso(_ sender: Any) {
        if ProcessInfo.processInfo.arguments.contains("creerEvent"){
            print("creerEvent edit profil")
            if (self.checkAllField()){
                //print("editProfileAsso profilView \(self.association)")
                self.associationMock?.editAssociationMock(newAssociation: self.editAssociation())
                let controller = TousEvenementsViewController.newInstanceProfilAssoEditTEST(gestionnaireAssociation: self.gestionnaireAssociation, association: self.association, evenementsMock:  EvenementsMock(), associationMock : self.associationMock)
                //(gestionnaireAssociation: self.gestionnaireAssociation, association: self.association, evenementsMock: nil, gaMock: self.gaMock)

                self.navigationController?.pushViewController(controller, animated: true)
                let alert = UIAlertController(title: "Update", message: "Vos données ont été mise à jour", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel))
                self.present(alert, animated: true)

        } else {
            if (self.checkAllField())  {
                self.associationService.edit(associationId: self.association.id!, association: self.editAssociation()) { associationFromAPI in
                    DispatchQueue.main.async {
                        self.association = associationFromAPI
                        print("Asso from api after edit : \(associationFromAPI!.description)")

                        let controller = TousEvenementsViewController.newInstance(gestionnaireAssociation: self.gestionnaireAssociation, association: self.association, evenementsMock: nil, gaMock: nil)

                        self.navigationController?.pushViewController(controller, animated: true)
                        let alert = UIAlertController(title: "Update", message: "Vos données ont été mise à jour", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel))
                        self.present(alert, animated: true)
                    }
                }
            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
}

