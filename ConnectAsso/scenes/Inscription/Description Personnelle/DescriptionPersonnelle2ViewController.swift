//
//  DescriptionPersonnelle2ViewController.swift
//  ConnectAsso
//  F7F7F0
//  F9F9F4
//  Created by Luc Coujonde on 06/07/2021.
//

import UIKit

class DescriptionPersonnelle2ViewController: UIViewController, UITextViewDelegate {

    @IBOutlet var descriptionPersonnelleTextView: UITextView!
    @IBOutlet weak var continuerButton: UIButton!

    var descriptionPersonnelle : String!

    var gestionnaireAssociationAvecDescription : GestionnaireAssociation!
    var gestionnaireAssoFactory : GEstionnaireAssoFactory = GEstionnaireAssoFactory()
    var gestionnaireAssoService : GestionnaireAssoService = GestionnaireAssoService()

    var nomMembre: String?
    var prenomMembre : String?
    var email : String?
    var motDePasse : String?
    var descriptionUtilisateur : String?

    static func newInstance(gestionnaireAssociation : GestionnaireAssociation) -> DescriptionPersonnelle2ViewController {
        let descriptionController = DescriptionPersonnelle2ViewController()
        descriptionController.gestionnaireAssociationAvecDescription = gestionnaireAssociation
        return descriptionController
    }

    static func buildGAWithDescription(utilisateurInscrit : Association)->Association{
        return utilisateurInscrit
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Etape 2 Description Personnelle"

        self.continuerButton.layer.cornerRadius = 30

        var contentInset = self.descriptionPersonnelleTextView.contentInset
        contentInset.left = 10
        contentInset.top = 10
        contentInset.right = 10

    }

    func buildGAWithDescription() -> Void {
        self.prenomMembre = self.gestionnaireAssociationAvecDescription.prenom
        self.nomMembre = self.gestionnaireAssociationAvecDescription.nom
        self.email = self.gestionnaireAssociationAvecDescription.mail
        self.motDePasse = self.gestionnaireAssociationAvecDescription.motDePasse
        self.descriptionUtilisateur = self.descriptionPersonnelleTextView.text

        var newGAWithDescription: GestionnaireAssociation

        let uiTestMode =  ProcessInfo.processInfo.arguments.contains("testMode")
        if uiTestMode {
            newGAWithDescription = GestionnaireAssociation(id: 1, nom: self.nomMembre!, prenom: self.prenomMembre!, motDePasse: self.motDePasse!, mail: self.email!, descriptionPersonnelle: self.descriptionUtilisateur!)
            print(newGAWithDescription.description)
            let newUtilisateurAvecDescription = DescriptionAssociationViewController.newInstance(gestionnaireAssociation: newGAWithDescription)
            self.navigationController?.pushViewController(newUtilisateurAvecDescription, animated: true)
            return
        }else{
            newGAWithDescription = GestionnaireAssociation(id: nil, nom: self.nomMembre!, prenom: self.prenomMembre!, motDePasse: self.motDePasse!, mail: self.email!, descriptionPersonnelle: self.descriptionUtilisateur!)
            self.gestionnaireAssoService.create(gestionnaireAssociation: newGAWithDescription) { (success) in
                DispatchQueue.main.sync {
                    if(success) {
                        // alert OK
                        let newUtilisateurAvecDescription = DescriptionAssociationViewController.newInstance(gestionnaireAssociation: newGAWithDescription)
                        self.navigationController?.pushViewController(newUtilisateurAvecDescription, animated: true)
                    } else {
                        print("gestionnaire Asso non créé")
                        // alert PAS OK
                    }
                }
            }
        }
    }

    private func reloadUI() {
        self.prenomMembre = self.gestionnaireAssociationAvecDescription.prenom
        self.nomMembre = self.gestionnaireAssociationAvecDescription.nom
        self.email = self.gestionnaireAssociationAvecDescription.mail
        self.motDePasse = self.gestionnaireAssociationAvecDescription.motDePasse
        print(self.nomMembre!)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }



    @IBAction func handleContinuer(_ sender: Any) {
        if(descriptionPersonnelleTextView.text == "") {
            print("Veuillez entrer une description personnelle !")
            let alert = UIAlertController(title: "Erreur", message: "Veuillez entrer une description personnelle !", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
            self.present(alert, animated: true)
            return
        } else if(descriptionPersonnelleTextView.text.count < 1) {
            print("Decription trop courte, veuillez entrer au minimum 50 caractères")
        } else {
            self.buildGAWithDescription()
        }
    }


}

extension DescriptionPersonnelle2ViewController: UITextFieldDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        //self.textCountLabel.text = "\(updatedText.count) / 700"
        return updatedText.count <= 1 //limit of infoTextView
    }

}

