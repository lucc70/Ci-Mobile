//
//  DescriptionAssociationViewController.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 08/07/2021.
//

import UIKit

class DescriptionAssociationViewController: UIViewController, UITextViewDelegate {


    var lineUnderTextField : StylesAndFormat = StylesAndFormat()
    var gestionnaireAssociationG : GestionnaireAssociation!
    var associationG : Association!
    var utilisateurInscritFactory : GEstionnaireAssoFactory = GEstionnaireAssoFactory()
    var utilisateurService : GestionnaireAssoService = GestionnaireAssoService()

    var GestionnaireAvecAssociation : GEstionnaireAssoFactory = GEstionnaireAssoFactory()

    //var associationAvecDescription : Association?
    var associationFactory : AssociationFactory = AssociationFactory()
    var associationService : AssociationService = AssociationService()


    @IBOutlet var nomAssociationTextField: UITextField!
    @IBOutlet var descriptionAssociationTextView: UITextView!
    @IBOutlet var dateDeCreationDatePicker: UIDatePicker!
    @IBOutlet weak var continuerButton: UIButton!
    @IBOutlet var recoverDateLabel: UILabel!

    var dateDeCreationD = Date()
    var isDateDeCreationPicker = false

    var nomAssociationU : String?
    var nomMembre: String?
    var prenomMembre : String?
    var email : String?
    var motDePasse : String?
    var descriptionUtilisateur : String?
    var descriptionPersonnelle : String?
    var isCalledByGestionnaire = false


    var nomAssociationA : String?
    var descriptionAssociation : String?
    var dateDeCreationS : String?

    static func newInstance(gestionnaireAssociation : GestionnaireAssociation) -> DescriptionAssociationViewController {
        let controller = DescriptionAssociationViewController()
        controller.gestionnaireAssociationG = gestionnaireAssociation
        return controller
    }

    func buildAssociation() -> Void {

        self.nomAssociationA = self.nomAssociationTextField.text
        self.descriptionAssociation = self.descriptionAssociationTextView.text
        self.dateDeCreationS = self.recoverDateLabel.text

        var association: Association

        let gestionnaireAssociationId = gestionnaireAssociationG.id!

        let uiTestMode =  ProcessInfo.processInfo.arguments.contains("testMode")
        if uiTestMode {
            association = Association(id: 1, nom: nomAssociationA, info: descriptionAssociation, dateCreation: dateDeCreationS)
            self.associationG = association
        }else{
            association = Association(id: nil, nom: nomAssociationA, info: descriptionAssociation, dateCreation: dateDeCreationS)
            self.associationService.create(gestionnaireAssociationId: gestionnaireAssociationId, association: association) { (success) in
                DispatchQueue.main.sync {
                    if(success) {
                        // alert OK
                        print("ici + \(association)")
                        self.associationG = association

                    } else {
                        print("gestionnaire Asso non créé")
                        // alert PAS OK
                    }
                }
            }
        }

    }

    func nomAssociationFieldIsEmpty() ->Bool {
        if(nomAssociationTextField.text!.count <= 0) {
            let alert = UIAlertController(title: "Erreur", message: "Veuillez entrer le nom de votre association", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
            self.present(alert, animated: true)
            return true
        }
        return false
    }

    func descriptionAssociationFieldIsEmpty() ->Bool{
        if (descriptionAssociationTextView.text.count <= 0){
            let alert = UIAlertController(title: "Erreur", message: "Veuillez entrer une description", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
            self.present(alert, animated: true)
            return true

        } else if(descriptionAssociationTextView.text.count <= 0){
            let alert = UIAlertController(title: "Erreur", message: "Decription trop courte, veuillez entrer au minimum 50 caractères", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
            self.present(alert, animated: true)
            return true
        } else {
            //self.buildAssociationWithDescription()
            print("Description de l'association : \(descriptionAssociationTextView.text!)")
            return false
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Etape 3 Description Association"
        self.descriptionAssociationTextView.delegate = self
        self.descriptionAssociationTextView.layer.cornerRadius = 30
        self.continuerButton.layer.cornerRadius = 30

        print("ga recupéré de inscrition : \(gestionnaireAssociationG.description)")

        var contentInset = self.descriptionAssociationTextView.contentInset
        contentInset.left = 10
        contentInset.top = 10
        contentInset.right = 10
        self.descriptionAssociationTextView.contentInset = contentInset

        lineUnderTextField.lineUnderTextField(textField: nomAssociationTextField)

        // Do any additional setup after loading the view.
    }

    func connexionSubmit() {
        if !gestionnaireAssociationG.mail!.isEmpty && !gestionnaireAssociationG.motDePasse!.isEmpty {
            let email = gestionnaireAssociationG.mail!
            let motDePasse = gestionnaireAssociationG.motDePasse!

            let uiTestMode =  ProcessInfo.processInfo.arguments.contains("testMode")
            if uiTestMode {
                let controller = TousEvenementsViewController.newInstance(gestionnaireAssociation: self.gestionnaireAssociationG, association: self.associationG, evenementsMock: nil, gaMock: nil)
                self.navigationController?.pushViewController(controller, animated: true)
            }else{
                RequestConnexion.AuthenticationService.requestConnexion(mail: email, password: motDePasse){ gestionnaireAssociationApi in
                    DispatchQueue.main.async {
                        if(gestionnaireAssociationApi != nil) {
                            self.gestionnaireAssociationG = gestionnaireAssociationApi
                            print("GA recup de lapi : \(self.gestionnaireAssociationG.description)")
                            self.associationService.getAssofromGA(gestionnaireAssociationId: self.gestionnaireAssociationG.id!) { associationApi in
                                DispatchQueue.main.async {
                                    self.associationG = associationApi
                                    print("association du ga : \(self.associationG.description)")
                                    //                    deco
                                    let controller = TousEvenementsViewController.newInstance(gestionnaireAssociation: self.gestionnaireAssociationG, association: self.associationG, evenementsMock: nil, gaMock: nil)
                                    self.navigationController?.pushViewController(controller, animated: true)
                                }
                            }
                        } else {
                            let alert = UIAlertController(title: "Erreur", message: "mail ou mot de passe incorrect", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
                            self.present(alert, animated: true)
                            print("mail ou mot de passe incorrect")
                        }
                    }
                }
            }

        } else {
            let alert = UIAlertController(title: "Erreur", message: "mail et mot de passe requis", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
            self.present(alert, animated: true)
        print("mail et mot de passe requis")
        }
    }

    @IBAction func handleContinuer(_ sender: Any) {
        self.nomAssociationFieldIsEmpty()
        self.descriptionAssociationFieldIsEmpty()
        if !self.descriptionAssociationFieldIsEmpty() && !self.nomAssociationFieldIsEmpty() {
            self.buildAssociation()
            self.connexionSubmit()
        }
    }



    @IBAction func dateDeCreationAssociation(_ sender: Any) {

        self.dateDeCreationD = self.dateDeCreationDatePicker.date
        print("date de ceation \(dateDeCreationD)")
        let dateDeCreationFormatter = DateFormatter()

        dateDeCreationFormatter.dateStyle = DateFormatter.Style.short
        self.isDateDeCreationPicker = true

        print("date de debut du picker \(dateDeCreationFormatter)")
        let dateDeCreationTemp = dateDeCreationFormatter.string(from: dateDeCreationDatePicker.date)
        recoverDateLabel.text = dateDeCreationTemp
        print("date de debut format String du label \(recoverDateLabel.text!)")
    }



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

var dateDeCreationFormatter = DateFormatter()


extension DescriptionAssociationViewController: UITextFieldDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        //self.textCountLabel.text = "\(updatedText.count) / 700"
        return updatedText.count <= 51//limit of infoTextView
    }

}

