//
//  InscriptionViewController.swift
//  ConnectAsso
// F7F7F0
//  Created by Luc Coujonde on 09/05/2021.
//

import UIKit

class InscriptionViewController: UIViewController, UITextFieldDelegate{


    @IBOutlet var ContentView: UIView!
    @IBOutlet var ScrollView: UIScrollView!

    @IBOutlet var allerConnexionViewButton: UIButton!
    @IBOutlet var inscrireButton: UIButton!

    @IBOutlet weak var nomMembreTextField: UITextField!
    @IBOutlet weak var prenomMembreTextField: UITextField!
    @IBOutlet weak var emailUtilisateurTextField: UITextField!
    @IBOutlet weak var confirmerEmailUtilisateurTextField: UITextField!
    @IBOutlet weak var motDePasseTextField: UITextField!
    //@IBOutlet weak var confirmerMotDePasseTextField: UITextField!

    var format : StylesAndFormat = StylesAndFormat()
    var lineUnderMyTextField : StylesAndFormat = StylesAndFormat()
    var gestionnaireAssociation : Association!
    var gestionnaireAssociationFactory : GEstionnaireAssoFactory = GEstionnaireAssoFactory()
    var gestionnaireAssociationService : GestionnaireAssoService = GestionnaireAssoService()
    //var evenementService : EvenementService = EvenementService()
    var textFieldRealYPosition: CGFloat = 0.0

    var nomMembre: String?
    var prenomMembre : String?
    var email : String?
    var motDePasse : String?
    var pasRempli : Bool?
    var isKeyboardAppear = false
    var isSameMail = false
    var isSamePassWord = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Inscription"
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)

        lineUnderMyTextField.lineUnderTextField(textField: nomMembreTextField)
        lineUnderMyTextField.lineUnderTextField(textField: prenomMembreTextField)
        lineUnderMyTextField.lineUnderTextField(textField: emailUtilisateurTextField)
        lineUnderMyTextField.lineUnderTextField(textField: confirmerEmailUtilisateurTextField)
        lineUnderMyTextField.lineUnderTextField(textField: motDePasseTextField)
        //lineUnderMyTextField.lineUnderTextField(textField: confirmerMotDePasseTextField)

        self.nomMembreTextField.layer.masksToBounds = true
        self.prenomMembreTextField.layer.masksToBounds = true
        self.emailUtilisateurTextField.layer.masksToBounds = true
        self.confirmerEmailUtilisateurTextField.layer.masksToBounds = true
        self.motDePasseTextField.layer.masksToBounds = true
        //self.confirmerMotDePasseTextField.layer.masksToBounds = true

        self.inscrireButton.layer.cornerRadius = 30

    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.ScrollView.endEditing(true)
    }

    func isAllTextFieldEmpty() -> Bool {
        if (self.nomMembreTextField.text == "" || self.prenomMembreTextField.text == "" || self.emailUtilisateurTextField.text == "" || self.confirmerEmailUtilisateurTextField.text == "" || self.motDePasseTextField.text == "" ) { // || self.confirmerMotDePasseTextField.text == ""
            print("istexfieldEmpty est a true ")
            let alert = UIAlertController(title: "Erreur", message: "Veuillez remplir tous les champs", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
            self.present(alert, animated: true)
            return true
        } else {
            print("istexfieldEmpty est a false ")
            return false
        }
    }

    // bloc de vérification des formats nom prenom et mail
    func isNameCorrectFormat()->Bool{
        if format.checkFormatNamesIsCorrect(strToCheck: nomMembreTextField.text!){
            print("le format de votre nom est incorrect ici ")
            let alert = UIAlertController(title: "Erreur", message: "Les caractères spéciaux sont interdits pour le nom", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
            self.present(alert, animated: true)
            return false
        }
        print("le format de votre nom est bon ici")
        return true
    }

    func isPrenomCorrectFormat()->Bool{
        if format.checkFormatNamesIsCorrect(strToCheck: prenomMembreTextField.text!) {
            print("le format de votre prenom est incorrect ici")
            let alert = UIAlertController(title: "Erreur", message: "Les caractères spéciaux sont interdits pour le prenom", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
            self.present(alert, animated: true)
            return false
        }
        print("le format de votre prenom est bon ici")
        return true
    }

    func isEmailFormatCorrect() -> Bool {
        if format.checkFormatMailIsCorrect(strToCheck: self.emailUtilisateurTextField.text!){
            print("le format de votre email est bon")
            return true
        } else {
            print("le format de votre email est incorrect")
            let alert = UIAlertController(title: "Erreur", message: "Le format de votre mail semble incorrect", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
            self.present(alert, animated: true)
            return false
        }
    }
    //fin du bloc de verification

    func isEmailCorrect() -> Bool {
        if (self.emailUtilisateurTextField.text == self.confirmerEmailUtilisateurTextField.text) {
            print("isEmailCorrect est a true")
            return true
        } else {
            print("isEmailCorrect est a false")
            let alert = UIAlertController(title: "Erreur", message: "Le mail de confirmation saisie est incorrect", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
            self.present(alert, animated: true)
            return false
        }
    }

//    func isPasswordCorrect() -> Bool {
//        if (self.motDePasseTextField.text == self.confirmerMotDePasseTextField.text) {
//            print("isPasswordCorrect est a true")
//            return true
//        } else {
//            print("isPasswordCorrect est a false")
//            let alert = UIAlertController(title: "Erreur", message: "Le mot de passe de confirmation saisie est incorrect", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
//            self.present(alert, animated: true)
//            return false
//        }
//    }


        func buildGA() -> GestionnaireAssociation {
            self.nomMembre = self.nomMembreTextField.text
            self.prenomMembre = self.prenomMembreTextField.text
            self.email = self.emailUtilisateurTextField.text
            self.motDePasse = self.motDePasseTextField.text
            //self.association = nil

            let newGestionnaireAsso = GestionnaireAssociation(id: nil, nom: self.nomMembre!, prenom: self.prenomMembre!, motDePasse: self.motDePasse!, mail: self.email!, descriptionPersonnelle: "")

            return newGestionnaireAsso
        }

//    let evenement = Evenement(id: nil, titre: titre, dateDebut: dateDebut, dateFin: dateFin, participants: nil, participantsEnAttente: nil, nomAssociation: "SPA", nomOrganisateur: "Dan", info: info, adresse: adresse, conversation: nil)
//    let detailsEvenement = DetailsEvenementViewController.newInstance(evenement: evenement)
//    self.navigationController?.pushViewController(detailsEvenement, animated: true)



    // fonction qui permet de ramener l'utilisateur vers la page de connexion
    @IBAction func allerConnexionView(_ sender: Any) {
        let connexionViewController = ConnexionViewController(nibName: "ConnexionViewController", bundle: nil)
        self.navigationController?.pushViewController(connexionViewController, animated: true)
    }


    // fonction qui permet de passer à l'étape suivante lors de l'inscription
    @IBAction func inscrireButton(_ sender: Any) {

        //TODO Rajouter le check du format de l'email && self.isEmailFormatCorrect()
        if (!self.isAllTextFieldEmpty() && self.isEmailCorrect() && self.isNameCorrectFormat() && self.isPrenomCorrectFormat() && self.isEmailFormatCorrect()){
            print("les champs sont bien remplis, le format du nom, du prenom et l'email est bon, l'email de confirmation est bon et le mdp de confirmation est bon")
            let instanceGAForDescrPeso = DescriptionPersonnelle2ViewController.newInstance(gestionnaireAssociation: self.buildGA())
            self.navigationController?.pushViewController(instanceGAForDescrPeso, animated: true)
        }
    }



    override func viewWillAppear(_ animated: Bool) {
            self.addKeyboardObserver()
        }

        override func viewWillDisappear(_ animated: Bool) {
            self.removeKeyboardObserver()
        }


}

extension InscriptionViewController : UITextViewDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


extension UIView {
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}


// extension pour que le clavier se mette sous les text field

extension UIResponder {

    static weak var responder: UIResponder?

    static func currentFirst() -> UIResponder? {
        responder = nil
        UIApplication.shared.sendAction(#selector(trap), to: nil, from: nil, for: nil)
        return responder
    }

    @objc private func trap() {
        UIResponder.responder = self
    }
}

extension UIViewController {
    func addKeyboardObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardNotifications(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
    }

    func removeKeyboardObserver(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }

    // This method will notify when keyboard appears/ dissapears
    @objc func keyboardNotifications(notification: NSNotification) {

        var txtFieldY : CGFloat = 0.0  //Using this we will calculate the selected textFields Y Position
        let spaceBetweenTxtFieldAndKeyboard : CGFloat = 5.0 //Specify the space between textfield and keyboard


        var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        if let activeTextField = UIResponder.currentFirst() as? UITextField ?? UIResponder.currentFirst() as? UITextView {
            // Here we will get accurate frame of textField which is selected if there are multiple textfields
            frame = self.view.convert(activeTextField.frame, from:activeTextField.superview)
            txtFieldY = frame.origin.y + frame.size.height
        }

        if let userInfo = notification.userInfo {
            // here we will get frame of keyBoard (i.e. x, y, width, height)
            let keyBoardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let keyBoardFrameY = keyBoardFrame!.origin.y
            let keyBoardFrameHeight = keyBoardFrame!.size.height

            var viewOriginY: CGFloat = 0.0
            //Check keyboards Y position and according to that move view up and down
            if keyBoardFrameY >= UIScreen.main.bounds.size.height {
                viewOriginY = 0.0
            } else {
                // if textfields y is greater than keyboards y then only move View to up
                if txtFieldY >= keyBoardFrameY {

                    viewOriginY = (txtFieldY - keyBoardFrameY) + spaceBetweenTxtFieldAndKeyboard

                    //This condition is just to check viewOriginY should not be greator than keyboard height
                    // if its more than keyboard height then there will be black space on the top of keyboard.
                    if viewOriginY > keyBoardFrameHeight { viewOriginY = keyBoardFrameHeight }
                }
            }

            //set the Y position of view
            self.view.frame.origin.y = -viewOriginY
        }
    }
}


