//
//  ConnexionViewController.swift
//  ConnectAsso
// F7F7F0
//  Created by Luc Coujonde on 10/05/2021.
//

import UIKit

class ConnexionViewController: UIViewController {

    var utilisateurInscritService : GestionnaireAssoService = GestionnaireAssoService()
    var associationService : AssociationService = AssociationService()
    var lineUnderMyTextField : StylesAndFormat = StylesAndFormat()
    var objGaAuth : Authentication!
    var gestionnaireAssociationCo : GestionnaireAssociation!
    var associationCo : Association!
    var authentificationGA : RequestConnexion = RequestConnexion()
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var motDePasseTextField: UITextField!
    @IBOutlet weak var messageEchecConnexion: UILabel!

    @IBOutlet var inscriptionLinkButton: UIButton!
    @IBOutlet var connexionButton: UIButton!

    var email : String?
    var motDePasse : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Connexion"

        lineUnderMyTextField.lineUnderTextField(textField: emailTextField)
        lineUnderMyTextField.lineUnderTextField(textField: motDePasseTextField)

        self.connexionButton.layer.cornerRadius = 30

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)


        // Do any additional setup after loading the view.

    }



    //hidden keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }


    @IBAction func keyboardWillShow(notification: NSNotification) {
       // if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= 150
            }
        //}
    }

    @IBAction func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y += 150
        }
    }


    @IBAction func goInscriptionView(_ sender: Any) {
        let inscriptionView = InscriptionViewController(nibName: "InscriptionViewController", bundle: nil)
        self.navigationController?.pushViewController(inscriptionView, animated: true)
    }

    @IBAction func connexionSubmit(_ sender: UIButton) {
        if !emailTextField.text!.isEmpty && !motDePasseTextField.text!.isEmpty {
            let email = emailTextField.text!
            let motDePasse = motDePasseTextField.text!

            RequestConnexion.AuthenticationService.requestConnexion(mail: email, password: motDePasse){ gestionnaireAssociationApi in
                DispatchQueue.main.async {
                    if(gestionnaireAssociationApi != nil) {
                        self.gestionnaireAssociationCo = gestionnaireAssociationApi
                        print("GA recup de lapi : \(self.gestionnaireAssociationCo.description)")
                        self.associationService.getAssofromGA(gestionnaireAssociationId: self.gestionnaireAssociationCo.id!) { associationApi in
                            DispatchQueue.main.async {
                                self.associationCo = associationApi
                                print("association du ga : \(self.associationCo.description)")
                                //                    deco
                                let controller = TousEvenementsViewController.newInstance(gestionnaireAssociation: self.gestionnaireAssociationCo, association: self.associationCo, evenementsMock: nil, gaMock: nil)
                                self.navigationController?.pushViewController(controller, animated: true)
                            }
                        }
                    } else {
                        let alert = UIAlertController(title: "Erreur", message: "mail ou mot de passe incorrect", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel))
                        self.present(alert, animated: true)
                        print("mail ou mot de passe incorrect")
                    }
                }
            }
        } else {
            let alert = UIAlertController(title: "Erreur", message: "mail et mot de passe requis", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
            self.present(alert, animated: true)
        print("mail et mot de passe requis")
        }
    }

    //func
//    func creationConnection (email : String, motDepasse : String) -> Authentication{
//        let GAConnexion = Authentication(mail: email, password: motDepasse)
//        print("GAConnexion : \(GAConnexion)")
//         return GAConnexion
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//fonction qui permet de retourner sur le prochain champs quand on clique sur le boutton return du clavier
extension ConnexionViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == self.emailTextField) {
            self.motDePasseTextField.becomeFirstResponder()// devient le champs suivant
        } else if textField == self.motDePasseTextField{
            self.connexionSubmit(self.connexionButton) //utilise la func conenxion
        }
        return true
    }
}

