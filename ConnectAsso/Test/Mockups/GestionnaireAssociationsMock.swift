//
//  GestionnaireAssociationsMock.swift
//  ConnectAsso
//
//  Created by Ludovic FAVIER on 23/01/2022.
//

import Foundation

class GestionnaireAssociationsMock{
    var dataGA: GestionnaireAssociation
    var description: String {
        return "GestionnaireAssociationMock \(dataGA)"
    }

    init(){
        self.dataGA = GestionnaireAssociation(id: 1, nom: "test-nom", prenom: "test-prénom", motDePasse: "mdp", mail: "test-mail@test.fr", descriptionPersonnelle: "test description personnelle")
    }
    func getGestionnaireAssociation() -> GestionnaireAssociation{
        return self.dataGA
    }
//    func editGestionnaireAssociation(nom: String, prenom: String, motDePasse: String, mail: String, descriptionPersonnelle: String){
//        self.dataGA = GestionnaireAssociation(id: 1, nom: nom, prenom: prenom, motDePasse: motDePasse, mail: mail, descriptionPersonnelle: descriptionPersonnelle)
//    }

    func editGestionnaireAssociation(newGA: GestionnaireAssociation){
        self.dataGA = newGA
    }

    static func getOneGestionnaireAssociation() -> GestionnaireAssociation{
            return GestionnaireAssociation(id: 1, nom: "test-nom", prenom: "test-prénom", motDePasse: "mdp", mail: "test-mail@test.fr", descriptionPersonnelle: "test description personnelle")
    }
}

