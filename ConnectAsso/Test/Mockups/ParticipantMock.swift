//
//  Participant.swift
//  ConnectAsso
//
//  Created by luc on 27/01/2022.
//
import UIKit
import Foundation

class ParticipantsMocks : UIViewController{
    var participantsEnAttenteMocks: [Benevole] = []
    var participantInscritsMocks: [Benevole] = []

    let participantEnAttente1 = Benevole(id: 1, nom: "nom-participant1", prenom: "prenom-participant1", mail: "mail-participant1", info: "description-participant1 ")
    let participantEnAttente2 = Benevole(id: 2, nom: "nom-participant2", prenom: "prenom-participant2", mail: "mail-participant2", info: "description-participant2")
    let participantEnAttente3 = Benevole(id: 3, nom: "nom-participant3", prenom: "prenom-participant3", mail: "mail-participant3", info: "description-participant3")
    let participantEnAttente4 = Benevole(id: 4, nom: "nom-participant4", prenom: "prenom-participant4", mail: "mail-participant4", info: "description-participant4")

    let participantInscrit1 = Benevole(id: 1, nom: "nom-participantInscrit1", prenom: "prenom-participantInscrit1", mail: "mail-participantInscrit1", info: "description-participantInscrit1 ")
    let participantInscrit2 = Benevole(id: 2, nom: "nom-participantInscrit2", prenom: "prenom-participantInscrit2", mail: "mail-participantInscrit2", info: "description-participantInscrit2")


    // PARTICIPANTS EN ATTENTE

    func getListParticpantsEnAttenteEvenements() -> [Benevole]{

        self.participantsEnAttenteMocks = [
            participantEnAttente1,
            participantEnAttente2,
            participantEnAttente3,
            participantEnAttente4
        ]

        return self.participantsEnAttenteMocks
    }
    func getOneParticpant(participantSelected : Benevole) {
        print(participantSelected)
        print("truc")
    }

    func moveParticipantsEnAttenteToParticipantsInscrit(participantAccepted : Benevole) {

        print("participantAccepted \(participantAccepted)")
        self.participantInscritsMocks.append(participantAccepted)
        self.participantsEnAttenteMocks = self.participantsEnAttenteMocks.filter {$0.id != participantAccepted.id}

        print("participants en attente \(self.participantsEnAttenteMocks)")
        print("participants inscrits \(self.participantInscritsMocks)")

    }

    func moveParticipantsEnAttenteToParticipantsInscrit2(participantAccepted : Benevole) {
        let participantView = ParticipantsListViewController()
        participantView.modification = true;
        print("participantAccepted \(participantAccepted)")
        self.participantInscritsMocks.append(participantAccepted)
        self.participantsEnAttenteMocks = self.participantsEnAttenteMocks.filter {$0.id != participantAccepted.id}
        self.navigationController?.pushViewController(participantView, animated: true)
        print("participants en attente \(self.participantsEnAttenteMocks)")
        print("participants inscrits \(self.participantInscritsMocks)")

    }


    // PARTICIPANTS INSCRITS
    func getListParticpantsInscritEvenements() -> [Benevole]{
        self.participantInscritsMocks = [
            participantInscrit1,
            participantInscrit2,
            participantEnAttente1
            //participantInscrit3,
            //participantInscrit4
        ]
        return self.participantInscritsMocks
    }

    func moveParticipantsInscritToParticipantsEnAttente(idParticipantAccepted : Int) {
        let participantEnAttente : Benevole
        participantEnAttente = self.participantsEnAttenteMocks[idParticipantAccepted]
        self.participantsEnAttenteMocks.append(participantEnAttente)
        self.participantInscritsMocks = self.participantInscritsMocks.filter {$0.id != idParticipantAccepted}

        print("participants en attente \(self.participantsEnAttenteMocks)")
        print("participants inscrits \(self.participantInscritsMocks)")
    }


}


