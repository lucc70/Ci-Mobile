//
//  Evenements.swift
//  ConnectAsso
//
//  Created by Ludovic FAVIER on 23/01/2022.
//

import Foundation

class EvenementsMock{
    var eventsMock: [Evenement]

    init(){
        // swiftlint:disable line_length
        let evenement1 = Evenement(id: 1, titre: "Conférence internationale 2021 de l’IHSA", dateDebut: "4/11/2021 15:30", dateFin: "4/11/2021 17:00", info: "Cette semaine à lieu la 6ème édition de la conférence internationale sur la recherche humanitaire de l’International humanitarian studies association (IHSA). La Fondation Croix-Rouge française, co-organisatrice de cet événement, propose deux panels sur la gestion des risques et catastrophes et sur les effets des épidémies sur le volontariat. C’est également durant cette conférence que la Fondation remettra ses prix de recherche annuel.", adresse: "5 Rue du Maréchal Joffre, 06400 Cannes", besoin: 42, organisateur: "Ludovic FAVIER", complet: false, nombreParticipants: 20, statut: "En Cours")

        let evenement2 = Evenement(id: 2, titre: "JOURNÉE INTERNATIONALE DE LA SOLIDARITÉ HUMAINE 2021", dateDebut: "20/12/2021 13:15", dateFin: "20/12/2021 23:00", info: "Instaurée en 2006 par la résolution A/RES/60/209 de l’Assemblée générale de l’Organisation des Nations unies (ONU), cette journée vise à promouvoir l'importance du partage et de la culture de la solidarité pour lutter contre la pauvreté.", adresse: "5 Rue du Maréchal Joffre, 06400 Cannes", besoin: 10, organisateur: "Luc COUJONDE", complet: false, nombreParticipants: 9, statut: "En Cours")

        let evenement3 = Evenement(id: 3, titre: "Open Pays d’Aix CEPAC 2021", dateDebut: "14/06/2021 10:00", dateFin: "20/06/2021 18:00", info: "l’Open Pays d’Aix CEPAC 2021 a proposé le « smartTrade Full Ace Challenge », cagnotte solidaire financée par smartTrade Technologies. Cette entreprise propose des solutions de trading électronique et possède des filiales à Londres, à Genève, à Milan, à New York, à Tokyo et à Singapour. C’est à Aix-en-Provence, ville qui accueille la compétition, que se trouve son siège social ainsi que son centre de recherche et développement.", adresse: "5 Rue du Maréchal Joffre, 06400 Cannes", besoin: 3, organisateur: "Arnaud Clément", complet: false, nombreParticipants: 3, statut: "En Cours")

        self.eventsMock = [
            evenement1,
            evenement2,
            evenement3
        ]
    }
    func getListEvenements() -> [Evenement]{
        return self.eventsMock
    }

    func deleteEvenementById(idEvenementToDelete: Int){
        self.eventsMock = self.eventsMock.filter {$0.id != idEvenementToDelete}
    }

    func createEvenement(newEvent : Evenement){
        return self.eventsMock.append(newEvent)
    }

}


