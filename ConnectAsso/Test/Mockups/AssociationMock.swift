//
//  AssociationMock.swift
//  ConnectAsso
//
//  Created by Ludovic FAVIER on 23/01/2022.
//

import Foundation

class AssociationMock{

    var dataAssociation: Association

    init(){
        self.dataAssociation = Association(id: 1, nom: "nom-Association", info: "Description de l'association ", dateCreation: "23/02/12")
    }
    func getAssociationMock() -> Association{
        print("MOCK")
        return self.dataAssociation
    }

    func printTestAssoMock(){
        print("MOCK erfeefeferref")
    }

    func editAssociationMock(newAssociation: Association){
      self.dataAssociation = newAssociation
    }

    static func getOneAssociationMock() -> Association{
           return Association(id: 1, nom: "nom-Association", info: "Description de l'association ", dateCreation: "23/02/12")
    }
}



