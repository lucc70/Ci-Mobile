//
//  TableViewCell.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 02/08/2021.
//

import UIKit

class EventsTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var adresseLabel: UILabel!
    @IBOutlet var miniDescrip: UILabel!
    @IBOutlet var besoinLabel: UILabel!
    @IBOutlet var nombreParticipant: UILabel!

    @IBOutlet var adresseConstLabel: UILabel!
    @IBOutlet var dateConstLabel: UILabel!
    @IBOutlet var participantsConstLabel: UILabel!

    override func layoutSubviews() {
        super.layoutSubviews()
    }


}

