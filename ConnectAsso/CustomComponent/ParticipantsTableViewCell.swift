//
//  ParticipantsTableViewCell.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 05/09/2021.
//

import UIKit

class ParticipantsTableViewCell: UITableViewCell {
    var  buttonPressed : (() -> ()) = {}
    var buttonRefusePressed : (() -> ()) = {}
    @IBOutlet var nomParticipantLabel: UILabel!
    @IBOutlet var prenomParticipantLabel: UILabel!
    @IBOutlet var mailParticpantLabel: UILabel!
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var refuseBtn: UIButton!

    override func layoutSubviews() {
        super.layoutSubviews()
    }
    @IBAction func acceptParticipantBtn(_ sender: Any) {
        buttonPressed()
    }
    @IBAction func refuseParticipantBtn(_ sender: Any) {
        buttonRefusePressed()
        print("refusé")
    }

    func accepterParticipant(text:String){
        print("text methode")
    }
}

