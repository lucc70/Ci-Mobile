//
//  UtilisateurInscritFactory.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 03/06/2021.
//

import Foundation

class GEstionnaireAssoFactory {

    public static func gestionnaireAssoFromDictionary(_ dict: [String: Any]) -> GestionnaireAssociation? {
        guard let nom = dict["nom"] as? String,
              let prenom = dict["prenom"] as? String,
              let motDePasse = dict["password"] as? String,
              let mail = dict["mail"] as? String,
              let descriptionPersonnelle = dict["description"] as? String
              else {
            print("else")
            return nil
        }
        let id = dict["id"] as? Int
        return GestionnaireAssociation(id: id, nom: nom, prenom: prenom, motDePasse: motDePasse, mail: mail, descriptionPersonnelle: descriptionPersonnelle)
    }

    public static func gestionnaireAssoFromDictionaryConnexion(_ dict: [String: Any]) -> GestionnaireAssociation? {
        print(dict)
        guard let gestionnaireAsso = dict["gestionnaireAsso"] as? [String: Any] else {
            return nil
        }
        guard let nom = gestionnaireAsso["nom"] as? String,
              let prenom = gestionnaireAsso["prenom"] as? String,
              let motDePasse = gestionnaireAsso["password"] as? String,
              let mail = gestionnaireAsso["mail"] as? String,
              let descriptionPersonnelle = gestionnaireAsso["description"] as? String
              else {
            print("else")
            return nil
        }
        let id = gestionnaireAsso["id"] as? Int
        return GestionnaireAssociation(id: id, nom: nom, prenom: prenom, motDePasse: motDePasse, mail: mail, descriptionPersonnelle: descriptionPersonnelle)
    }

    public static func dictionaryFromGestionnaireAsso(_ utilisateurInscrit: GestionnaireAssociation) -> [String: Any] {
        var dict : [String: Any] = [:]
        dict["nom"] = utilisateurInscrit.nom
        dict["prenom"] = utilisateurInscrit.prenom
        dict["password"] = utilisateurInscrit.motDePasse
        dict["mail"] = utilisateurInscrit.mail
        dict["description"] = utilisateurInscrit.descriptionPersonnelle


        if let id = utilisateurInscrit.id {
            dict["id"] = id
        }
        return dict
    }

//    public static func gestionnaireAssoForConnection(_ dict:[String:Any]) -> GestionnaireAssociation{
//
//        let mail = dict["mail"] as? String
//        let password = dict["password"] as? String
//
//        return GestionnaireAssociation(
//
//    }


    public func constructionUtilisateurSansService(id: Int?, nom:String, prenom:String, motDePasse: String, mail: String, descriptionPersonnelle: String) -> GestionnaireAssociation? {

        return GestionnaireAssociation(id: id,
                                       nom: nom,
                                       prenom: prenom,
                                       motDePasse: motDePasse,
                                       mail: mail,
                                       descriptionPersonnelle: descriptionPersonnelle)
    }

}

