//
//  BenevoleFactory.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 05/09/2021.
//

import Foundation

class BenevoleFactory {
    public static func benevoleFromDictionary(_ dict: [String: Any]) -> Benevole?{

        guard let nom = dict["nom"] as? String,
              let prenom = dict["prenom"] as? String,
              let mail = dict["mail"] as? String,
              let info = dict ["description"] as? String else {
            return nil
        }
        let id = dict ["id"] as? Int

        return Benevole(id: id,
                        nom: nom,
                        prenom: prenom,
                        mail: mail,
                        info: info
                        )
    }

    public static func dictionnaryFromBenevole(_ benevole: Benevole) -> [String: Any] {
        var dict: [String: Any] = [:]
        dict["nom"] = benevole.nom
        dict["prenom"] = benevole.prenom
        dict["mail"] = benevole.mail
        dict["description"] = benevole.info

        if let id = benevole.id { dict["id"] = id }

        return dict
    }
}

