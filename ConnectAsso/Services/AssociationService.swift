//
//  AssociationService.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 06/06/2021.
//

import Foundation

class AssociationService {



    public func getAssofromGA (gestionnaireAssociationId : Int, completion: @escaping (Association?) -> Void) -> Void {
        //let gestionnaireAssociationId = 35
        guard let apiAssociationURL = URL(string: "http://localhost:8010/api/v1/gestionnaireAssociation/association/gestionnaireAssociationId=\(gestionnaireAssociationId)")
        else {
            completion(nil)
            return
        }
        let task = URLSession.shared.dataTask(with: apiAssociationURL) { (data, response, err) in
            guard err == nil, let d = data else {
                completion(nil)
                return
            }

            let associationAny = try? JSONSerialization.jsonObject(with: d, options: .allowFragments)
            guard let association = associationAny as? [String: Any] else {
                completion(nil)
                return
            }
//            let res = association.compactMap(AssociationFactory.associationFromDictionary(_:))
//            completion(res)
            print("association : \(association)" )
            let res = AssociationFactory.associationFromDictionary(association)
            completion(res)
        }
        task.resume()
    }

    public func getAssociationInfos(id: Int, completion: @escaping (Association?)-> Void) -> Void {
            guard let associaitonURL = URL(string: "http://127.0.0.1:8010/api/v1/association/associationId=\(id)")  else {
                completion (nil)
                return
            }
            let task = URLSession.shared.dataTask(with: associaitonURL) { (data, response, err) in
                guard err == nil, let d = data else {
                    completion(nil)
                    return
                }

                // try? -> permet de renvoyer nil cas d'erreur de la focntion jsonObject
                let associationAny = try? JSONSerialization.jsonObject(with: d, options: .allowFragments)
                guard let association = associationAny as? [String: Any] else {
                    completion(nil)
                    return
                }
                print("association : \(association)" )
                let res = AssociationFactory.associationFromDictionary(association)
                completion(res)


            }
            task.resume() // lance le telechargement des evenements
        }


    public func edit(associationId: Int, association: Association, completion: @escaping (Association?) -> Void) -> Void {

            guard let authURL = URL(string: "http://127.0.0.1:8010/api/v1/association/associationId=\(associationId)") else {
                completion(nil)
                return
            }

            var request = URLRequest(url: authURL)
            request.httpMethod = "PATCH"

        let dict = AssociationFactory.dictionaryFromAssociation(association)
            let data = try? JSONSerialization.data(withJSONObject: dict, options: .fragmentsAllowed)
            request.httpBody = data
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")

            let task = URLSession.shared.dataTask(with: request){ (data, response, error) in
                guard error == nil, let d = data else {
                    completion(nil)
                    return
                }
                let assoData = try? JSONSerialization.jsonObject(with: d, options: .allowFragments)

                guard let association = assoData as? [String: Any] else {
                    completion(nil)
                    return
                }

                let res = AssociationFactory.associationFromDictionary(association)
                completion(res)
            }
            task.resume()

        }

//    class AuthenticationService {
//        public func auth(login : Authentication, completion: @escaping (User?)->Void)->Void {
//
//                guard let authURL = URL(string : "http://51.178.139.94:3000/auth/signin/") else {
//                    completion(nil)
//                    return
//                }
//
//                var request = URLRequest(url : authURL)
//                request.httpMethod = "POST"
//
//                let dict = AuthenticationFactory.dictionnaryFromAuthentication(login)
//                let data = try? JSONSerialization.data(withJSONObject: dict, options: .fragmentsAllowed)
//                request.httpBody = data
//                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//
//                let task = URLSession.shared.dataTask(with:request){ (data,response,error) in
//                    guard error == nil, let d = data else {
//                        completion(nil)
//                        return
//                    }
//                    let userData = try? JSONSerialization.jsonObject(with: d, options: .allowFragments)
//
//                    guard let user = userData as? [String: Any] else {
//                        completion(nil)
//                        return
//                    }
//
//                    let res = UserFactory.userFromDictonnary(user )
//                    completion(res)
//                }
//                task.resume()
//
//
//            }
//    }
//


    public func getAllAssos(completion: @escaping ([Association]) -> Void) -> Void {
        guard let assoURL = URL(string: "http://127.0.0.1:8010/api/v1/association") else {
            completion([])
            return
        }
        let task = URLSession.shared.dataTask(with: assoURL) { (data, response, err) in
            guard err == nil, let d = data else {
                completion([])
                return
            }

            // try? -> permet de renvoyer nil cas d'erreur de la fonction jsonObject
            let assosAny = try? JSONSerialization.jsonObject(with: d, options: .allowFragments)
            guard let assos = assosAny as? [ [String: Any] ] else {
                completion([])
                return
            }
            let res = assos.compactMap(AssociationFactory.associationFromDictionary(_:));
            completion(res)
        }
        task.resume() // Lance le telechargement
    }



    public func create(gestionnaireAssociationId: Int, association: Association, completion: @escaping (Bool) -> Void) -> Void {
        guard let createAssociationURL = URL(string: "http://127.0.0.1:8010/api/v1/association/createAsso/gestionnaireAssociationId=\(gestionnaireAssociationId)") else {
            completion(false)
            return
        }
        var request = URLRequest(url: createAssociationURL)
        request.httpMethod = "POST"
        let session = URLSession.shared

        let dict = AssociationFactory.dictionaryFromAssociation(association)
        let data = try? JSONSerialization.data(withJSONObject: dict, options: .fragmentsAllowed)
        request.httpBody = data
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let task = session.dataTask(with: request, completionHandler: { data, response, error in

            guard error == nil, let data = data else
                { completion(false)
                    return
                }

            do {
                guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] else {
                    completion(false)
                    return
                }
                guard let jsonId = json["id"] as? Int else {
                    completion(false)
                    return
                }
                print("json : \(json)")
                association.setId(id: jsonId)
                completion(true)

            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }

    public func delete (id: String, completion : @escaping (Bool) -> Void) -> Void {
        guard let removeAssociationURL = URL(string: "http://127.0.0.1:8010/api/v1/association/associationId=\(id)") else {
            completion(false)
            return
        }
        var request = URLRequest(url: removeAssociationURL)
        request.httpMethod = "DELETE"

        let task = URLSession.shared.dataTask(with: request) { (data, response, err) in
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(false)
                return
            }
            completion(httpResponse.statusCode == 200)
        }
        task.resume()
    }
}

