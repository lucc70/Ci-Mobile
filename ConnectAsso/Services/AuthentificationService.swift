//
//  AuthentificationService.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 01/09/2021.
//

import Foundation
import UIKit
class RequestConnexion{
    enum Result<Individual> {
        case Success(Individual)
        case Error(String, Int)
    }


class AuthenticationService {
    public func auth(login : Authentication, completion: @escaping (GestionnaireAssociation?)->Void)->Void {

            guard let authURL = URL(string : "http://127.0.0.1:8010/api/v1/authGestionnaireAsso/signin") else {
                completion(nil)
                return
            }

            var request = URLRequest(url : authURL)
            request.httpMethod = "POST"

            let dict = AuthenticationFactory.dictionnaryFromAuthentication(login)
            let data = try? JSONSerialization.data(withJSONObject: dict, options: .fragmentsAllowed)
            request.httpBody = data
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")

            let task = URLSession.shared.dataTask(with:request){ (data,response,error) in
                guard error == nil, let d = data else {
                    completion(nil)
                    return
                }
                let gestionnaireAssoData = try? JSONSerialization.jsonObject(with: d, options: .allowFragments)

                guard let gestionnaireAsso = gestionnaireAssoData as? [String: Any] else {
                    completion(nil)
                    return
                }

                let res = GEstionnaireAssoFactory.gestionnaireAssoFromDictionary(gestionnaireAsso)
                completion(res)
            }
            task.resume()

        }

    public static func requestConnexion(mail: String, password: String, completion: @escaping (GestionnaireAssociation?) -> Void){
        guard let uri = URL(string: "http://127.0.0.1:8010/api/v1/authGestionnaireAsso/signin") else {
            completion(nil)
            //completion(nil)
            return
        }
        let parameters = ["mail": mail, "password": password]

        //create the session object
        let session = URLSession.shared

        //now create the URLRequest object using the url object
        var request = URLRequest(url: uri)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
        }

        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil, let data = data else {
                print("ici 1")
                completion(nil)
                return
            }

            do {
                //create json object from data
                guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] else {
                    print("ici 2")
                    completion(nil)
                    return
                }

                guard let gestionnaireAsso = GEstionnaireAssoFactory.gestionnaireAssoFromDictionaryConnexion(json)

                else{
                    print("pas de ga")
                    completion(nil)
                    return
                }
                print("ici ga api 2 : \(gestionnaireAsso.description)")
                completion(gestionnaireAsso)
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
}
}

