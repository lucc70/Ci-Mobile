//
//  File.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 06/06/2021.
//

import Foundation

class AssociationFactory {

    public static func associationFromDictionary(_ dict: [String: Any]) -> Association?{
        guard let nom = dict["nom"] as? String,
              let info = dict["info"] as? String,
              let dateCreation = dict ["creation"]as? String
              //let gestionnaireAssociation = dict [" gestionnaire association: "] as? GestionnaireAssociation
                else {
        return nil
        }
        let id = dict["id"] as? Int
        //let evenement = dict ["evenement : "] as? [Evenement]
        return Association(id: id,
                           nom: nom,
                           info: info,
                           dateCreation: dateCreation)
//                           gestionnaireAssociation: gestionnaireAssociation,
//                           evenement: evenement)
    }

    public static func dictionaryFromAssociation(_ association: Association) -> [String: Any]{
        var dict: [String: Any] = [:]
        dict["nom"] = association.nom
        dict["info"] = association.info
        dict["creation"] = association.dateCreation

        if let id = association.id { dict["id"] = id }
        //if let evenement = association.evenement { dict["evenement : "] = evenement }
        return dict
    }


}



