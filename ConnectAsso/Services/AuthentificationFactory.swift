//
//  AuthentificationFactory.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 01/09/2021.
//

import Foundation

class AuthenticationFactory {

    public static func AuthFromDictionnary (_ dict:[String:Any]) -> Authentication{

        let mail = dict["mail"] as? String
        let password = dict["password"] as? String

        return Authentication(mail: mail, password: password)

    }

    public static func dictionnaryFromAuthentication(_ login:Authentication ) -> [String:Any]{

        var dict: [String: Any] = [:]
        dict["username"] = login.mail
        dict["password"] = login.password

        if let username = login.mail {
            dict["username"] = username
        }

        if let password = login.password {
            dict["password"] = password
        }

        return dict

    }
}

