//
//  EvenementService.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 31/05/2021.
//

import Foundation

class EvenementService {


    public func list(completion : @escaping ([Evenement]) -> Void) -> Void {
        guard let evenementURL = URL(string: "http://localhost:8010/api/v1/evenements") else {
            completion([])
            return
        }
        let tache = URLSession.shared.dataTask(with: evenementURL) { (data, response, err) in
            guard err == nil, let d = data else {
                completion([])
                return
            }

            // try? -> permet de renvoyer nil cas d'erreur de la fonction jsonObject
            let evenementsAny = try? JSONSerialization.jsonObject(with: d, options: .allowFragments)
            guard let evenements = evenementsAny as? [ [String : Any] ] else {
                completion([])
                return
            }
            let res = evenements.compactMap(EvenementFactory.eventFromDictionnary(_:))
            completion(res)
        }
        tache.resume()//lance le telechargement
    }

    public func getAllEventOfAsso(assoid: Int,completion : @escaping ([Evenement]) -> Void) -> Void {
        guard let evenementURL = URL(string: "http://127.0.0.1:8010/api/v1/association/evenement/associationId=\(assoid)") else {
            completion([])
            return
        }
        let task = URLSession.shared.dataTask(with: evenementURL) { (data, response, err) in
            guard err == nil, let d = data else {
                completion([])
                return
            }

            // try? -> permet de renvoyer nil cas d'erreur de la fonction jsonObject
            let evenementsAny = try? JSONSerialization.jsonObject(with: d, options: .allowFragments)
            guard let evenements = evenementsAny as? [ [String : Any] ] else {
                completion([])
                return
            }
            let res = evenements.compactMap(EvenementFactory.eventFromDictionnary(_:))
            completion(res)
        }
        task.resume()//lance le telechargement
    }

    public func getAvenirEventOfAsso(assoid: Int,completion : @escaping ([Evenement]) -> Void) -> Void {
        guard let evenementURL = URL(string:"http://127.0.0.1:8010/api/v1/association/evenement/a-venir/associationId=\(assoid)") else {
            completion([])
            return
        }
        let task = URLSession.shared.dataTask(with: evenementURL) { (data, response, err) in
            guard err == nil, let d = data else {
                completion([])
                return
            }

            // try? -> permet de renvoyer nil cas d'erreur de la fonction jsonObject
            let evenementsAny = try? JSONSerialization.jsonObject(with: d, options: .allowFragments)
            guard let evenements = evenementsAny as? [ [String : Any] ] else {
                completion([])
                return
            }
            let res = evenements.compactMap(EvenementFactory.eventFromDictionnary(_:))
            completion(res)
        }
        task.resume()//lance le telechargement
    }

    public func getAnnuleEventOfAsso(assoid: Int,completion : @escaping ([Evenement]) -> Void) -> Void {
        guard let evenementURL = URL(string:"http://127.0.0.1:8010/api/v1/association/evenement/annule/associationId=\(assoid)") else {
            completion([])
            return
        }
        let task = URLSession.shared.dataTask(with: evenementURL) { (data, response, err) in
            guard err == nil, let d = data else {
                completion([])
                return
            }

            // try? -> permet de renvoyer nil cas d'erreur de la fonction jsonObject
            let evenementsAny = try? JSONSerialization.jsonObject(with: d, options: .allowFragments)
            guard let evenements = evenementsAny as? [ [String : Any] ] else {
                completion([])
                return
            }
            let res = evenements.compactMap(EvenementFactory.eventFromDictionnary(_:))
            completion(res)
        }
        task.resume()//lance le telechargement
    }

    public func getTermineEventOfAsso(assoid: Int,completion : @escaping ([Evenement]) -> Void) -> Void {
        guard let evenementURL = URL(string: "http://127.0.0.1:8010/api/v1/association/evenement/termine/associationId=\(assoid)") else {
            completion([])
            return
        }
        let task = URLSession.shared.dataTask(with: evenementURL) { (data, response, err) in
            guard err == nil, let d = data else {
                completion([])
                return
            }

            // try? -> permet de renvoyer nil cas d'erreur de la fonction jsonObject
            let evenementsAny = try? JSONSerialization.jsonObject(with: d, options: .allowFragments)
            guard let evenements = evenementsAny as? [ [String : Any] ] else {
                completion([])
                return
            }
            let res = evenements.compactMap(EvenementFactory.eventFromDictionnary(_:))
            completion(res)
        }
        task.resume()//lance le telechargement
    }

    public func getEvenementInfo(id: Int, completion: @escaping (Evenement?)-> Void) -> Void {
        guard let evenementURL = URL(string: "http://localhost:8010/api/v1/evenement/\(id)")  else {
            completion (nil)
            return
        }
        let tache = URLSession.shared.dataTask(with: evenementURL) { (data, response, err) in
            guard err == nil, let d = data else {
                completion(nil)
                return
            }

            // try? -> permet de renvoyer nil cas d'erreur de la focntion jsonObject
            let evenementAny = try? JSONSerialization.jsonObject(with: d, options: .allowFragments)
            guard let evenement = evenementAny as? [String: Any] else {
                completion(nil)
                return
            }
            let res = EvenementFactory.eventFromDictionnary(evenement)
            completion(res)

        }
        tache.resume() // lance le telechargement des evenements
    }

    public func create(associationId : Int, evenement: Evenement, completion: @escaping (Evenement?) -> Void) -> Void {
        guard let createEvenementURL = URL(string: "http://127.0.0.1:8010/api/v1/evenement/createEvent/associationId=\(associationId)") else {
            completion(nil)
            return
        }
        var request = URLRequest(url: createEvenementURL)
        request.httpMethod = "POST"

        let dict = EvenementFactory.dictionnaryFroEvent(evenement)
        let data = try? JSONSerialization.data(withJSONObject: dict, options: .fragmentsAllowed)
        request.httpBody = data
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let task = URLSession.shared.dataTask(with: request) { (data, response, err) in
            guard err == nil, let d = data else {
                completion(nil)
                return
            }
            let eventData = try? JSONSerialization.jsonObject(with: d, options: .allowFragments)

            guard let evenement = eventData as? [String: Any] else {
                completion(nil)
                return
            }

            let res = EvenementFactory.eventFromDictionnary(evenement)
            completion(res)
        }
        task.resume()
    }

    public func edit(evenementId : Int, evenementToEdit : Evenement, completion: @escaping (Evenement?) -> Void) -> Void {
        guard let createEvenementURL = URL(string: "http://127.0.0.1:8010/api/v1/evenement/\(evenementId)") else {
            completion(nil)
            return
        }
        var request = URLRequest(url: createEvenementURL)
        request.httpMethod = "PATCH"

        let dict = EvenementFactory.dictionnaryFroEvent(evenementToEdit)
        let data = try? JSONSerialization.data(withJSONObject: dict, options: .fragmentsAllowed)
        request.httpBody = data
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let task = URLSession.shared.dataTask(with: request) { (data, response, err) in
            guard err == nil, let d = data else {
                completion(nil)
                return
            }
            let eventData = try? JSONSerialization.jsonObject(with: d, options: .allowFragments)

            guard let evenement = eventData as? [String: Any] else {
                completion(nil)
                return
            }

            let res = EvenementFactory.eventFromDictionnary(evenement)
            completion(res)
        }
        task.resume()
    }

    public func delete(id: Int, completion: @escaping (Bool) -> Void) -> Void {
        guard let removeEvenementURL = URL(string: "http://127.0.0.1:8010/api/v1/evenement/\(id)") else {
            completion(false)
            return
        }
        var request = URLRequest(url: removeEvenementURL)
        request.httpMethod = "DELETE"

        let tache = URLSession.shared.dataTask(with: request) { (data, response, err) in
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(false)
                return
            }
            completion(httpResponse.statusCode == 204)
        }
        tache.resume()
    }

    public func acceptAParticipants(particpantsId: Int,evenementId :Int, completion: @escaping (Bool?)-> Void) -> Void {
        guard let evenementURL = URL(string: "http://127.0.0.1:8010/api/v1/evenement/accept/evenementId=\(evenementId)/benevoleId=\(particpantsId)")  else {
            completion (false)
            print("accept 1 ")
            return
        }

        var request = URLRequest(url: evenementURL)
        request.httpMethod = "POST"

        let tache = URLSession.shared.dataTask(with: request) { (data, response, err) in
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(false)
                print("accept 2 ")
                return
            }
            completion(httpResponse.statusCode == 200)
        }
        print("accept 3")
        tache.resume()
    }


    public func resetParticipantAccepted(particpantsId: Int,evenementId :Int, completion: @escaping (Bool?)-> Void) -> Void {
        guard let evenementURL = URL(string:"http://127.0.0.1:8010/api/v1/evenement/signlist/evenementId=\(evenementId)/benevoleId=\(particpantsId)")  else {
            completion (false)
            print("reset 1 ")
            return
        }

        var request = URLRequest(url: evenementURL)
        request.httpMethod = "DELETE"

        let tache = URLSession.shared.dataTask(with: request) { (data, response, err) in
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(false)
                print("reset 2 ")
                return
            }
            completion(httpResponse.statusCode == 200)
        }
        print("reset 3")
        tache.resume()
    }

    public func refusedParticipant(particpantsId: Int,evenementId :Int, completion: @escaping (Bool?)-> Void) -> Void {
        guard let evenementURL = URL(string:"http://127.0.0.1:8010/api/v1/evenement/refused/evenementId=\(evenementId)/benevoleId=\(particpantsId)")  else {
            completion (false)
            print("refused 1 ")
            return
        }

        var request = URLRequest(url: evenementURL)
        request.httpMethod = "DELETE"

        let tache = URLSession.shared.dataTask(with: request) { (data, response, err) in
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(false)
                print("refused 2 ")
                return
            }
            completion(httpResponse.statusCode == 200)
        }
        print("refused 3")
        tache.resume()
    }

}

//http://127.0.0.1:8010/api/v1/evenement/accept/evenementId=2/benevoleId=1

