//
//  UtilisateursInscritService.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 04/06/2021.
//

import Foundation

class GestionnaireAssoService {

    //getAll
    public func GetOneGA(gestionnaireAssociationId : Int, completion: @escaping (GestionnaireAssociation?) -> Void) -> Void {
        guard let utilisateursInscritURL = URL(string: "http://127.0.0.1:8010/api/v1/gestionnaireAssociation/\(gestionnaireAssociationId)") else {
            completion(nil)
            return
        }
        let task = URLSession.shared.dataTask(with: utilisateursInscritURL) { (data, response, err) in
            guard err == nil, let d = data else {
                completion(nil)
                return
            }

            let gestionnaireAssociationAny = try? JSONSerialization.jsonObject(with: d, options: .allowFragments)
            guard let gestionnaireAssociation = gestionnaireAssociationAny as?  [String: Any]  else {
                completion(nil)
                return
            }
            let res = GEstionnaireAssoFactory.gestionnaireAssoFromDictionary(gestionnaireAssociation)
            completion(res)
        }
        task.resume()
    }


    public func create(gestionnaireAssociation: GestionnaireAssociation, completion: @escaping (Bool) -> Void) -> Void {
        guard let createGestionnaireAssociationURL = URL(string: "http://127.0.0.1:8010/api/v1/gestionnaireAssociation/") else {
            completion(false)
            return
        }
        var request = URLRequest(url: createGestionnaireAssociationURL)
        request.httpMethod = "POST"
        let session = URLSession.shared

        let dict = GEstionnaireAssoFactory.dictionaryFromGestionnaireAsso(gestionnaireAssociation)
        let data = try? JSONSerialization.data(withJSONObject: dict, options: .fragmentsAllowed)
        request.httpBody = data
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let task = session.dataTask(with: request, completionHandler: { data, response, error in

            guard error == nil, let data = data else
                { completion(false)
                    return
                }

            do {
                guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] else {
                    completion(false)
                    return
                }
                guard let jsonId = json["id"] as? Int else {
                    completion(false)
                    return
                }
                print("json : \(json)")
                gestionnaireAssociation.setId(id: jsonId)
                completion(true)

            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }


    public func edit(gestionnaireAssociationId : Int, gestionnaireAssociation : GestionnaireAssociation, completion: @escaping (GestionnaireAssociation?)->Void)->Void {

            guard let authURL = URL(string : "http://127.0.0.1:8010/api/v1/gestionnaireAssociation/\(gestionnaireAssociationId)") else {
                completion(nil)
                return
            }

            var request = URLRequest(url : authURL)
            request.httpMethod = "PATCH"

            let dict = GEstionnaireAssoFactory.dictionaryFromGestionnaireAsso(gestionnaireAssociation)
            let data = try? JSONSerialization.data(withJSONObject: dict, options: .fragmentsAllowed)
            request.httpBody = data
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")

            let task = URLSession.shared.dataTask(with:request){ (data,response,error) in
                guard error == nil, let d = data else {
                    completion(nil)
                    return
                }
                let gestionnaireAssoData = try? JSONSerialization.jsonObject(with: d, options: .allowFragments)

                guard let gestionnaireAsso = gestionnaireAssoData as? [String: Any] else {
                    completion(nil)
                    return
                }

                let res = GEstionnaireAssoFactory.gestionnaireAssoFromDictionary(gestionnaireAsso)
                completion(res)
            }
            task.resume()

        }



    public func delete(gestionnaireAssoId: Int, completion: @escaping (Bool) -> Void) -> Void {
        guard let removeGestionnaireAssoURL = URL(string: "http://127.0.0.1:8010/api/v1/gestionnaireAssociation/\(gestionnaireAssoId)") else {
            completion(false)
            return
        }
        var request = URLRequest(url: removeGestionnaireAssoURL)
        request.httpMethod = "DELETE"

        let task = URLSession.shared.dataTask(with: request) { (data, response, err) in
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(false)
                return
            }
            completion(httpResponse.statusCode == 204)
        }
        task.resume()
    }

    // mail:String, motdepasse:String
//    public func connexion(utilisateurConnexion : GestionnaireAssociation, completion:@escaping (GestionnaireAssociation?) -> Void) -> Void {
//        guard let connexionURL = URL(string: "http://127.0.0.1:8010/api/v1/authGestionnaireAsso/signin") else { //http://localhost:8010/api/v1/gestionnaire_associations
//            completion(nil)
//            return
//        }
//
//        var request = URLRequest(url: connexionURL)
//        request.httpMethod = "POST"
//
//        let dict = GEstionnaireAssoFactory.dictionaryFromGestionnaireAsso(utilisateurConnexion)
//        let data = try? JSONSerialization.data(withJSONObject: dict, options: .fragmentsAllowed)
//        request.httpBody = data
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
////        let task = URLSession.shared.dataTask(with: request) { (data, response, err) in
////            guard let httpResponse = response as? HTTPURLResponse else {
////                completion(nil)
////                return
////            }
//        let gestionnaireAssoAny = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
//        guard let gestionnaireAsso = gestionnaireAssoAny as? [String: Any] else {
//            completion(nil)
//            return
//        }
//        let res = GEstionnaireAssoFactory.gestionnaireAssoFromDictionary(gestionnaireAsso)
//        completion(res)
//    }
//    task.resume() // Lance le telechargement
//}
//
//    public func getUtilisateurInfo(id:String, completion: @escaping (GestionnaireAssociation?) -> Void) -> Void {
//        guard let utilisateurURL = URL(string: "http://localhost:8010/api/v1/gestionnaire_associations/\(id)") else {
//            completion(nil)
//            return
//        }
//        let tache = URLSession.shared.dataTask(with: utilisateurURL) {(data, response, err) in
//            guard err == nil, let d = data else {
//                completion(nil)
//                return
//            }
//            let utilisateurAny = try? JSONSerialization.jsonObject(with: d, options: .allowFragments)
//            guard let utilisateur = utilisateurAny as? [String: Any] else {
//                completion(nil)
//                return
//            }
//            let res = GEstionnaireAssoFactory.gestionnaireAssoFromDictionary(utilisateur)
//            completion(res)
//        }
//        tache.resume()
//    }
}

