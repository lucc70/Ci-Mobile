//
//  BenevoleService.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 05/09/2021.
//

import Foundation

class BenevoleService {

    public func participantsEnAttente(evenementId : Int, completion : @escaping ([Benevole]) -> Void) -> Void {
        guard let benevoleURL = URL(string: "http://127.0.0.1:8010/api/v1/evenement/participantsenattente/evenement.id=\(evenementId)") else {
            completion([])
            return
        }
        let task = URLSession.shared.dataTask(with: benevoleURL) { (data, response, err) in
            guard err == nil, let d = data else {
                completion([])
                return
            }

            // try? -> permet de renvoyer nil cas d'erreur de la fonction jsonObject
            let benevoleAny = try? JSONSerialization.jsonObject(with: d, options: .allowFragments)
            guard let benevoles = benevoleAny as? [ [String : Any] ] else {
                completion([])
                return
            }
            let res = benevoles.compactMap(BenevoleFactory.benevoleFromDictionary(_:))
            completion(res)
        }
        task.resume()//lance le telechargement
    }

    public func participantsInscrit(evenementId : Int, completion : @escaping ([Benevole]) -> Void) -> Void {
        guard let benevoleURL = URL(string: "http://127.0.0.1:8010/api/v1/evenement/participantsinscrits/evenement.id=\(evenementId)") else {
            completion([])
            return
        }
        let task = URLSession.shared.dataTask(with: benevoleURL) { (data, response, err) in
            guard err == nil, let d = data else {
                completion([])
                return
            }

            // try? -> permet de renvoyer nil cas d'erreur de la fonction jsonObject
            let benevoleAny = try? JSONSerialization.jsonObject(with: d, options: .allowFragments)
            guard let benevoles = benevoleAny as? [ [String: Any] ] else {
                completion([])
                return
            }
            let res = benevoles.compactMap(BenevoleFactory.benevoleFromDictionary(_:))
            completion(res)
        }
        task.resume()//lance le telechargement
    }
}

