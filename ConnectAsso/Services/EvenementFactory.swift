//
//  EvenementFactory.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 01/06/2021.
//

import Foundation

class EvenementFactory {

    // renvoi un evenement à partir d'un dictionnaire ou null
   public static func eventFromDictionnary(_ dict: [String: Any]) -> Evenement?{

        guard let titre = dict["titre"] as? String,
              let dateDebut = dict["debut"] as? String,
              let dateFin = dict["fin"] as? String,
              let info = dict ["description"] as? String,
              let adresse = dict ["adresse"] as? String,
              let besoin = dict ["besoin"] as? Int,
              let organisateur = dict ["organisateur"] as? String else {
            return nil
        }
        let id = dict ["id"] as? Int
        let complet = dict ["complet"] as? Bool
        let nombreParticipants = dict ["nombreParticipants"] as? Int
        let statut = dict["statut"] as? String

        return Evenement(id: id,
                         titre: titre,
                         dateDebut: dateDebut,
                         dateFin: dateFin,
                         info: info,
                         adresse: adresse,
                         besoin: besoin,
                         organisateur: organisateur,
                         complet: complet,
                         nombreParticipants: nombreParticipants,
                         statut: statut)
   }

    public static func dictionnaryFroEvent(_ evenement : Evenement) -> [String: Any] {
        var dict: [String: Any] = [:]
        dict["titre"] = evenement.titre
        dict["debut"] = evenement.dateDebut
        dict["fin"] = evenement.dateFin
        dict["description"] = evenement.info
        dict["adresse"] = evenement.adresse
        dict["besoin"] = evenement.besoin
        dict["organisateur"] = evenement.organisateur


        if let id = evenement.id { dict["id"] = id }
        if let complet = evenement.complet { dict["complet"] = complet }
        if let nombreParticipants = evenement.nombreParticipants { dict["nombreParticipants"] = nombreParticipants }
        if let statut = evenement.statut{ dict["statut"] = statut}

        return dict
    }

}

//var titre : String
//var dateDebut : Date
//var dateFin : Date
//var nomAssociation : String
//var nomOrganisateur : String
//var info : String
//var adresse : String

//var id : Int?
//var participants : Array<Any>?
//var participantsEnAttente : Array<Any>?
//var conversation : Array<Any>?

