//
//  Association.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 06/06/2021.
// rajouter utilisateur inscrit dans association faire ça partout.

import Foundation

class Association : CustomStringConvertible {

    var id : Int?
    var nom : String?
    var info : String?
    var dateCreation : String?
    var description: String {
        return "Association [id:\(self.id),nom: \(self.nom),infos: \(self.info), date de creation: \(self.dateCreation)]" //,evenement: \(self.evenement ?? []),gestionnaire association: \(self.gestionnaireAssociation)
    }

    public func setId(id: Int) -> Void{
            self.id = id
        }

    public init(id:Int?, nom : String?, info : String?, dateCreation : String?) { //, gestionnaireAssociation : GestionnaireAssociation, evenement : [Evenement]?
        self.id = id
        self.nom = nom
        self.info = info
        self.dateCreation = dateCreation
//        self.evenement = evenement
//        self.gestionnaireAssociation = gestionnaireAssociation
    }
}

