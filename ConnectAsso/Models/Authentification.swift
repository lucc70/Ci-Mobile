//
//  Authentification.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 01/09/2021.
//

import Foundation

class Authentication: CustomStringConvertible {
    var mail:String!
    var password: String!
    var description: String {
        return "Auth : [\(self.mail),\(self.password)]"
    }

    public init (mail : String?, password: String?){
        self.mail = mail;
        self.password = password;
    }
}

