//
//  Evenement.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 31/05/2021.
//  changer assoss string en obj assos

import Foundation

class Evenement : CustomStringConvertible {

    var id : Int?
    var titre : String?
    var dateDebut : String?
    var dateFin : String?
    var info : String?
    var adresse : String?
    var besoin : Int?
    var organisateur : String?
    var complet : Bool?
    var nombreParticipants : Int?
    var statut : String?
    var description: String {
        return "Evenement [\(self.id ?? 0),\(self.titre ?? ""),\(self.dateDebut ?? ""),\(self.dateFin ?? ""),\(self.info ?? ""), \(self.adresse ?? ""), \(self.besoin ?? 0), \(self.organisateur ?? ""), \(self.complet ?? false), \(self.nombreParticipants ?? 0), \(self.statut ?? "")]"
    }

    public init(id : Int?, titre : String, dateDebut : String, dateFin : String, info : String, adresse : String, besoin : Int?, organisateur : String, complet : Bool?, nombreParticipants :Int?, statut : String?) {
        self.id = id
        self.titre = titre
        self.dateDebut = dateDebut
        self.dateFin = dateFin
        self.info = info
        self.adresse = adresse
        self.besoin = besoin
        self.organisateur = organisateur
        self.complet = complet
        self.nombreParticipants = nombreParticipants
        self.statut = statut
    }
}

