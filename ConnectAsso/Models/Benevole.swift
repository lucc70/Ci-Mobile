//
//  File.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 05/09/2021.
//

import Foundation

class Benevole : CustomStringConvertible {

    var id: Int?
    var nom : String?
    var prenom : String?
    var mail :String?
    var info:String?

    var description: String {
        return "Benevole [\(self.id ?? 0),\(self.nom ?? ""),\(self.prenom ?? ""),\(self.mail ?? ""),\(self.info ?? "") ]"
    }

    public init(id : Int?, nom : String?, prenom : String?, mail : String?, info : String? ) {
        self.id = id
        self.nom = nom
        self.prenom = prenom
        self.mail = mail
        self.info = info
    }



}

