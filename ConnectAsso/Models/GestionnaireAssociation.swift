//
//  UtilisateursInscrit.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 02/06/2021.
//

import Foundation

class GestionnaireAssociation: CustomStringConvertible {

    var id : Int?
    var nom : String
    var prenom : String?
    var motDePasse : String?
    var mail : String?
    var descriptionPersonnelle : String?
//    var associationAGerer : Association? = nil
//    var avis : [avis]?
    var description: String {
        return "UtilisateurInscrit [id: \(self.id), nom: \(self.nom), prenom: \(self.prenom), mdp: \(self.motDePasse), mail: \(self.mail),description perso: \(self.descriptionPersonnelle)"
    }

    public func setId(id: Int) -> Void{
            self.id = id
        }

    public init(id : Int?, nom : String, prenom : String, motDePasse : String, mail: String, descriptionPersonnelle: String) {
        self.id = id
        self.nom = nom
        self.prenom = prenom
        self.motDePasse = motDePasse
        self.mail = mail
        self.descriptionPersonnelle = descriptionPersonnelle
    }


}

