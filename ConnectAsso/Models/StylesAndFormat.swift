//
//  Styles.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 01/08/2021.
//
import UIKit
import Foundation
class StylesAndFormat : UIViewController{

    //var excludedCharacters = ["@",".","&","'","(","§","!",")","=","+","<",">","/",":","#","*"]

//    func parseExcludedChar() -> String {
//        for exclededChar in excludedCharacters {
//            return exclededChar
//        }
//        return ""
//    }

//    func CheckFormatNames(strToCheck:String,strToCompare:String)->Bool{
//        if(strToCheck.contains(strToCompare)){
//            return true
//        }
//        return false
//    }

    func checkFormatNamesIsCorrect(strToCheck:String)->Bool {
        if strToCheck.contains("@") || strToCheck.contains(".") || strToCheck.contains("&")  || strToCheck.contains("'") || strToCheck.contains("(") || strToCheck.contains("§") || strToCheck.contains("!") || strToCheck.contains(")") || strToCheck.contains("=") || strToCheck.contains("+") || strToCheck.contains("<") || strToCheck.contains(">") || strToCheck.contains("/") || strToCheck.contains(":") || strToCheck.contains("#") || strToCheck.contains("*") || strToCheck.contains("\'")  {
            return true
        }
        return false
    }

    func checkFormatMailIsCorrect(strToCheck:String)->Bool {
        if strToCheck.contains("@") || strToCheck.contains(".") {
            return true
        }
        return false
    }

    func lineUnderTextField(textField:UITextField){
        let bottomLine = CALayer()

        bottomLine.frame = CGRect(x: 0, y: textField.frame.height - 2, width: textField.frame.width, height: 2)

        bottomLine.backgroundColor = UIColor.init(red: 58/255, green: 71/255, blue: 147/255, alpha: 0.5).cgColor

        textField.borderStyle = .none
        textField.layer.addSublayer(bottomLine)

    }

}

