//
//  Avis.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 29/08/2021.
//

import Foundation

class Avis : CustomStringConvertible {
    var description: String = ""
}
