//
//  AppDelegate.swift
//  ConnectAsso
//
//  Created by Luc Coujonde on 04/04/2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        let window = UIWindow(frame: UIScreen.main.bounds)
        var firstController: UIViewController

        let uiTestMode =  ProcessInfo.processInfo.arguments.contains("testMode")
        if uiTestMode {
            let gaMock = GestionnaireAssociationsMock()
            if ProcessInfo.processInfo.arguments.contains("deleteEvent"){
                firstController = TousEvenementsViewController.newInstance(gestionnaireAssociation: gaMock.getGestionnaireAssociation(), association: AssociationMock.getOneAssociationMock(), evenementsMock: EvenementsMock(), gaMock: gaMock)
            }
            else if ProcessInfo.processInfo.arguments.contains("editGA"){
                firstController = TousEvenementsViewController.newInstance(gestionnaireAssociation: gaMock.getGestionnaireAssociation(), association: AssociationMock.getOneAssociationMock(), evenementsMock: EvenementsMock(), gaMock: gaMock)
            }
            else if ProcessInfo.processInfo.arguments.contains("creerEvent"){
                        firstController =  TousEvenementsViewController.newInstanceProfilAssoEditTEST(gestionnaireAssociation: GestionnaireAssociationsMock.getOneGestionnaireAssociation(), association: AssociationMock.getOneAssociationMock(), evenementsMock: EvenementsMock(), associationMock: AssociationMock())
            }
            else if  ProcessInfo.processInfo.arguments.contains("ParticipantTEST"){
                    firstController =  TousEvenementsViewController.newInstanceTest(gestionnaireAssociation: GestionnaireAssociationsMock.getOneGestionnaireAssociation(), association: AssociationMock.getOneAssociationMock(), evenementsMock: EvenementsMock())
            }
            else{
                firstController = ConnexionViewController(nibName: "ConnexionViewController", bundle: nil)
            }
        }else{
            firstController = ConnexionViewController(nibName: "ConnexionViewController", bundle: nil)
        }
        window.rootViewController = UINavigationController(rootViewController: firstController)
        window.makeKeyAndVisible()
        self.window = window
        return true
    }



}


