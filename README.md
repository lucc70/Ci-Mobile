# ConnectAsso projet ios

## Description

Projet annuel de 3eme année mobilité et objets connecté : 
    L'application IOS de ConnectAsso est destiné aux membres d'associations.
    La création d'un compte et sa validation leur donne accès aux fonctionnalités suivantes : 
- Accéder à la liste des évènements 
- CRUD évènement 
- CRUD ressource (humaine, matériel, etc) 
- Consulter la liste des bénévoles qui se sont inscrits à un évènement 
- Discuter avec un utilisateur par écrit 
- Accéder à la liste des promesses de don 
- Signaler un bug 
- Valider la candidature d’un bénévole à un évènement 
- Ajouter un avis sur le profil d’un bénévole à la suite d’un évènement
- Recevoir des notifications

## Lancement

- Aller dans le dossier api-node, et suivre les instructions de son README pour lancer les serveurs. 
- A la racine, ouvrir le projet ConnectAsso.xcodeproj avec Xcode
- selectionner l'interface Ipad Prop (11-inch) 3rd generation 
- puis lancer
