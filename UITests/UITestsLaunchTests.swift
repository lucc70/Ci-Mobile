//
//  UITestsLaunchTests.swift
//  UITests
//
//  Created by Ludovic FAVIER on 20/01/2022.
//

import XCTest

class UITestsLaunchTests: XCTestCase {
    override class var runsForEachTargetApplicationUIConfiguration: Bool {
        true
    }

    override func setUpWithError() throws {
        continueAfterFailure = false
    }
}
