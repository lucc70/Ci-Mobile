//
//  UITests.swift
//  UITests
//
//  Created by Ludovic FAVIER on 20/01/2022.
//

import XCTest

class UITests: XCTestCase {

    var app: XCUIApplication!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        // UI tests must launch the application that they test.
        app = XCUIApplication()
        app.launchArguments = ["testMode"]
        //app.launch()
    }
//
//    override func tearDownWithError() throws {
//        // Put teardown code here. This method is called after the invocation of each test method in the class.
//    }

    func testEditProfileGA() throws{
        app.launchArguments.append("editGA")
        app.launch()

        let goToProfileGA = app.navigationBars.buttons["test-mail@test.fr"]
        let goToProfileGA2 = app.navigationBars.buttons["email@email.fr"]
        goToProfileGA.tap()

        let scrollViewsQuery = app.scrollViews
        let elementsQuery = scrollViewsQuery.otherElements
        let nomTextField = app.scrollViews.otherElements.textFields["Nom"]
        let prNomTextField = app.scrollViews.otherElements.textFields["Prénom"]
        let emailTextField = app.scrollViews.otherElements.textFields["Email"]
        let motDePasseSecureTextField = app.scrollViews.otherElements.secureTextFields["Mot de Passe"]
        let btnSave = elementsQuery/*@START_MENU_TOKEN@*/.staticTexts["Sauvegarder"]/*[[".buttons[\"Sauvegarder\"].staticTexts[\"Sauvegarder\"]",".staticTexts[\"Sauvegarder\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        let descriptionText = app.scrollViews.otherElements.containing(.textField, identifier:"Nom").children(matching: .textView).element
        //let nomElement = app.scrollViews.otherElements.containing(.textField, identifier:"Nom").element

        XCTAssertTrue(nomTextField.exists, "Text field doesn't exist")
        nomTextField.tap()
        nomTextField.clearText()
        nomTextField.typeText("nom")
        XCTAssertEqual(nomTextField.value as! String, "nom", "Text field value is not correct")

        XCTAssertTrue(prNomTextField.exists, "Text field doesn't exist")
        prNomTextField.tap()
        prNomTextField.clearText()
        prNomTextField.typeText("prénom")
        XCTAssertEqual(prNomTextField.value as! String, "prénom", "Text field value is not correct")

        XCTAssertTrue(emailTextField.exists, "Text field doesn't exist")
        emailTextField.tap()
        emailTextField.clearText()
        emailTextField.typeText("email@email.fr")
        XCTAssertEqual(emailTextField.value as! String, "email@email.fr", "Text field value is not correct")

        XCTAssertTrue(motDePasseSecureTextField.exists, "Text field doesn't exist")
        motDePasseSecureTextField.tap()
        motDePasseSecureTextField.clearText()
        motDePasseSecureTextField.typeText("pwd")
        XCTAssertEqual(motDePasseSecureTextField.value as! String, "•••", "Text field value is not correct")

        XCTAssertTrue(descriptionText.exists, "Text field doesn't exist")
        descriptionText.tap()
        //descriptionText.clearText()
        descriptionText.typeText("test")
        XCTAssertEqual(descriptionText.value as! String, "testtest description personnelle", "Text field value is not correct")


        //scrollViewsQuery.otherElements.containing(.textField, identifier:"Nom").children(matching: .textView).element.tap()
        app.scrollViews.otherElements.containing(.textField, identifier:"Nom").element.swipeUp()


        btnSave.forceTapElement()

        let okAlertDialog = app.alerts["Update"].scrollViews.otherElements.buttons["OK"]
        okAlertDialog.forceTapElement()


        //app.navigationBars["Profil"].buttons["Vos événements"].tap()

        goToProfileGA2.tap()

        XCTAssertTrue(nomTextField.exists, "Text field doesn't exist")
        XCTAssertEqual(nomTextField.value as! String, "nom", "Text field value is not correct")

        XCTAssertTrue(prNomTextField.exists, "Text field doesn't exist")
        XCTAssertEqual(prNomTextField.value as! String, "prénom", "Text field value is not correct")

        XCTAssertTrue(emailTextField.exists, "Text field doesn't exist")
        XCTAssertEqual(emailTextField.value as! String, "email@email.fr", "Text field value is not correct")

        XCTAssertTrue(motDePasseSecureTextField.exists, "Text field doesn't exist")
        XCTAssertEqual(motDePasseSecureTextField.value as! String, "•••", "Text field value is not correct")

        XCTAssertTrue(descriptionText.exists, "Text field doesn't exist")
        XCTAssertEqual(descriptionText.value as! String, "testtest description personnelle", "Text field value is not correct")

        sleep(2)

    }


    func testEditProfileAssociation() throws {
        app.launchArguments.append("creerEvent")
        app.launch()


        let app = XCUIApplication()
        let btnNomAssociation = app.navigationBars.buttons["nom-Association"]
        let scrollViewsQuery = app.scrollViews
        let elementsQuery = scrollViewsQuery.otherElements
        let nomTextField = elementsQuery.textFields["Nom"]


        let textView = scrollViewsQuery.otherElements.containing(.textField, identifier:"Nom").children(matching: .textView).element

        let ScrollViewOfProfilAsso = scrollViewsQuery.otherElements.containing(.textField, identifier:"Nom").element

        let sauvegarderStaticText = elementsQuery/*@START_MENU_TOKEN@*/.staticTexts["Sauvegarder"]/*[[".buttons[\"Sauvegarder\"].staticTexts[\"Sauvegarder\"]",".staticTexts[\"Sauvegarder\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        let btnOkDialogMsg = app.alerts["Infos"].scrollViews.otherElements.buttons["OK"]



        btnNomAssociation.tap()

        ScrollViewOfProfilAsso.swipeDown()
        sauvegarderStaticText.tap()
        btnOkDialogMsg.tap()
        ScrollViewOfProfilAsso.swipeUp()
        nomTextField.tap()
        nomTextField.clearText()
        nomTextField.typeText("NOUVEAU-Nom-Association")
        textView.tap()
        textView.clearText()
        textView.typeText("NOUVEL-Description-Association")
        ScrollViewOfProfilAsso.swipeDown()
        sauvegarderStaticText.tap()
        app.alerts["Update"].scrollViews.otherElements.buttons["OK"].forceTapElement()

        btnNomAssociation.tap()
        nomTextField.tap()
        XCTAssertEqual(nomTextField.value as! String, "NOUVEAU-Nom-Association", "Text field value is not correct")
        textView.tap()
        XCTAssertEqual(textView.value as! String, "NOUVEL-Description-AssociationDescription de l'association ", "Text field value is not correct")

        sleep(2)


    }


    func testAddParticipantEnAttenteToInscrits() throws {
        //ProcessInfo.processInfo.arguments.contains("ParticipantTEST")
        app.launchArguments.append("creerEvent")
        app.launch()

        let app = XCUIApplication()
        let tablesQuery = app.tables
        let evenement = tablesQuery/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"Conférence internationale 2021 de l’IHSA")/*[[".cells.containing(.staticText, identifier:\"42\")",".cells.containing(.staticText, identifier:\"20\")",".cells.containing(.staticText, identifier:\"4\/11\/2021 15:30\")",".cells.containing(.staticText, identifier:\"Cette semaine à lieu la 6ème édition de la conférence internationale sur la recherche humanitaire de l’International humanitarian studies association \")",".cells.containing(.staticText, identifier:\"Conférence internationale 2021 de l’IHSA\")"],[[[-1,4],[-1,3],[-1,2],[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.staticTexts["5 Rue du Maréchal Joffre, 06400 Cannes"]
        let btnParticipantsEnAttente = app/*@START_MENU_TOKEN@*/.staticTexts["Paricipants en attente"]/*[[".buttons[\"Paricipants en attente\"].staticTexts[\"Paricipants en attente\"]",".staticTexts[\"Paricipants en attente\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        let btnAjouterParticipant = tablesQuery/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"nom-participant1")/*[[".cells.containing(.staticText, identifier:\"prenom-participant1\")",".cells.containing(.staticText, identifier:\"mail-participant1\")",".cells.containing(.staticText, identifier:\"nom-participant1\")"],[[[-1,2],[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.buttons["selected"]
        let btnBackToEvenement = app.navigationBars["Participants En Attente"].buttons["Editer un Evenement"]
        let btnParticipantsInscrit = app/*@START_MENU_TOKEN@*/.staticTexts["Particpants Inscrits"]/*[[".buttons[\"Particpants Inscrits\"].staticTexts[\"Particpants Inscrits\"]",".staticTexts[\"Particpants Inscrits\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        let NewParticipant = tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["nom-participant1"]/*[[".cells.staticTexts[\"nom-participant1\"]",".staticTexts[\"nom-participant1\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/

        evenement.tap()
        btnParticipantsEnAttente.tap()
        btnAjouterParticipant.tap()
        btnBackToEvenement.tap()
        btnParticipantsInscrit.tap()
        NewParticipant.tap()

        XCTAssertTrue(NewParticipant.exists, "New item doesn't exist")

        sleep(2)
    }

    func testCreerEvenement() throws {

        app.launchArguments.append("creerEvent")
        app.launch()
        let btnAdd = app.navigationBars["Vos événements"].buttons["Add"]
        let textFieldNomEvenement = app.textFields["Nom de l'événement"]
        let textAdresse = app.textFields["Adresse"]
        let textViewDescription = XCUIApplication().children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .textView).element
        let debutTextField = app.textFields["debut"]
        let finTextField = app.textFields["fin"]
        let nombreParticipants = XCUIApplication().windows.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .textField).element(boundBy: 5)
        let btnConfirmer = app/*@START_MENU_TOKEN@*/.buttons["Confirmer"].staticTexts["Confirmer"]/*[[".buttons[\"Confirmer\"].staticTexts[\"Confirmer\"]",".staticTexts[\"Confirmer\"]"],[[[-1,1],[-1,0]]],[1]]@END_MENU_TOKEN@*/
        let evenement1 = app.tables.staticTexts["Conférence internationale 2021 de l’IHSA"]
        let evenement2 = app.tables.staticTexts["JOURNÉE INTERNATIONALE DE LA SOLIDARITÉ HUMAINE 2021"]
        let evenement3 = app.tables/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"Open Pays d’Aix CEPAC 2021")/*[[".cells.containing(.staticText, identifier:\"12345\")",".cells.containing(.staticText, identifier:\"14\/06\/2021 10:00\")",".cells.containing(.staticText, identifier:\"l’Open Pays d’Aix CEPAC 2021 a proposé le « smartTrade Full Ace Challenge », cagnotte solidaire financée par smartTrade Technologies. Cette entreprise\")",".cells.containing(.staticText, identifier:\"Open Pays d’Aix CEPAC 2021\")"],[[[-1,3],[-1,2],[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.staticTexts["5 Rue du Maréchal Joffre, 06400 Cannes"]
        let evenement4 = app.tables.staticTexts["test-nom-nouveau-evenement"]

        btnAdd.tap()
        textFieldNomEvenement.tap()
        textFieldNomEvenement.typeText("test-nom-nouveau-evenement")
        textAdresse.tap()
        textAdresse.typeText("test-adresse-nouveau-evenement")
        textViewDescription.forceTapElement()
        textViewDescription.typeText("test-description-nouveau-evenement")
        debutTextField.tap()
        debutTextField.typeText("20/12/2021 18:15")
        finTextField.tap()
        finTextField.typeText("21/12/2021 18:15")
        nombreParticipants.forceTapElement()
        nombreParticipants.typeText("6")
        app/*@START_MENU_TOKEN@*/.buttons["Hide keyboard"]/*[[".keyboards.buttons[\"Hide keyboard\"]",".buttons[\"Hide keyboard\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        btnConfirmer.tap()

        XCTAssertTrue(evenement1.exists, "item doesn't exist")
        XCTAssertTrue(evenement2.exists, "item doesn't exist")
        XCTAssertTrue(evenement3.exists, "item doesn't exist")
        XCTAssertTrue(evenement4.exists, "New item doesn't exist")

        sleep(2)
    }

    func testDeleteEvenement() throws{
        app.launchArguments.append("deleteEvent")
        app.launch()

        let itemToDelete = app.tables/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"JOURNÉE INTERNATIONALE DE LA SOLIDARITÉ HUMAINE 2021")/*[[".cells.containing(.staticText, identifier:\"99999\")",".cells.containing(.staticText, identifier:\"20\/12\/2021 13:15\")",".cells.containing(.staticText, identifier:\"Instaurée en 2006 par la résolution A\/RES\/60\/209 de l’Assemblée générale de l’Organisation des Nations unies (ONU), cette journée vise à promouvoir l'\")",".cells.containing(.staticText, identifier:\"JOURNÉE INTERNATIONALE DE LA SOLIDARITÉ HUMAINE 2021\")"],[[[-1,3],[-1,2],[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.staticTexts["5 Rue du Maréchal Joffre, 06400 Cannes"]

        let deleteButton2 = app.navigationBars["Editer un Evenement"].buttons["Delete"]

        XCTAssertTrue(itemToDelete.exists, "item to delete doesn't exist")

        itemToDelete.forceTapElement()

        sleep(2)

        //let deleteButton = app.navigationBars["Terminé"].buttons["Delete"]
        //deleteButton.forceTapElement()
        deleteButton2.forceTapElement()
        XCTAssertFalse(itemToDelete.exists, "item to delete still exist")

        sleep(2)


    }

    func testLancement() throws {
        app.launchArguments.append("creerEvent")
        app.launch()


        sleep(2)
    }

    func testDeleteAllEvenements() throws{
        app.launchArguments.append("deleteEvent")
        app.launch()

        let eventToDelete1 = app.tables/*@START_MENU_TOKEN@*/.staticTexts["Conférence internationale 2021 de l’IHSA"]/*[[".cells.staticTexts[\"Conférence internationale 2021 de l’IHSA\"]",".staticTexts[\"Conférence internationale 2021 de l’IHSA\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        let eventToDelete2 = app.tables.staticTexts["JOURNÉE INTERNATIONALE DE LA SOLIDARITÉ HUMAINE 2021"]
        let eventToDelete3 = app.tables/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"Open Pays d’Aix CEPAC 2021")/*[[".cells.containing(.staticText, identifier:\"12345\")",".cells.containing(.staticText, identifier:\"14\/06\/2021 10:00\")",".cells.containing(.staticText, identifier:\"l’Open Pays d’Aix CEPAC 2021 a proposé le « smartTrade Full Ace Challenge », cagnotte solidaire financée par smartTrade Technologies. Cette entreprise\")",".cells.containing(.staticText, identifier:\"Open Pays d’Aix CEPAC 2021\")"],[[[-1,3],[-1,2],[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.staticTexts["5 Rue du Maréchal Joffre, 06400 Cannes"]

        let deleteButton = app.navigationBars["Editer un Evenement"].buttons["Delete"]

        eventToDelete1.forceTapElement()
        deleteButton.forceTapElement()

        eventToDelete2.forceTapElement()
        deleteButton.forceTapElement()

        eventToDelete3.forceTapElement()
        deleteButton.forceTapElement()


        XCTAssertFalse(eventToDelete1.exists, "item to delete still exist") //vérifier que les items n'existent plus
        XCTAssertFalse(eventToDelete2.exists, "item to delete still exist")
        XCTAssertFalse(eventToDelete3.exists, "item to delete still exist")


        sleep(2)



    }

        func testInscriptionGestionnaireAssociation() throws {
        // UI tests must launch the application that they test.
            app.launch()
            // PAGE CONNEXION

            app/*@START_MENU_TOKEN@*/.staticTexts["S'inscrire"]/*[[".buttons[\"S'inscrire\"].staticTexts[\"S'inscrire\"]",".staticTexts[\"S'inscrire\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()

            // PAGE GESTIONNAIRE UTILISATEUR - INFOS

            let scrollViewsQuery = app.scrollViews
            let elementsQuery2 = scrollViewsQuery.otherElements

            let nameTexfield = elementsQuery2.textFields["Nom"]
            let firstNameTexfield = elementsQuery2.textFields["Prénom"]
            let emailTexfield = elementsQuery2.textFields["Email"]
            let confirmEmailTexfield = elementsQuery2.textFields["Confirmer Email"]
            let passwordTexfield = elementsQuery2.secureTextFields["Mot de Passe"]
            //let btnSignup = elementsQuery/*@START_MENU_TOKEN@*/.staticTexts["S'inscrire"]/*[[".buttons[\"S'inscrire\"].staticTexts[\"S'inscrire\"]",".staticTexts[\"S'inscrire\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            let btnSignup = elementsQuery2/*@START_MENU_TOKEN@*/.staticTexts["S'inscrire"]/*[[".buttons[\"S'inscrire\"].staticTexts[\"S'inscrire\"]",".staticTexts[\"S'inscrire\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/

            XCTAssertTrue(btnSignup.exists, "button doesn't exist")

            XCTAssertTrue(nameTexfield.exists, "Text field doesn't exist")
            nameTexfield.tap()
            nameTexfield.typeText("test-nom")
            XCTAssertEqual(nameTexfield.value as! String, "test-nom", "Text field value is not correct")

            XCTAssertTrue(firstNameTexfield.exists, "Text field doesn't exist")
            firstNameTexfield.tap()
            firstNameTexfield.typeText("test-prénom")
            XCTAssertEqual(firstNameTexfield.value as! String, "test-prénom", "Text field value is not correct")

            XCTAssertTrue(emailTexfield.exists, "Text field doesn't exist")
            emailTexfield.tap()
            emailTexfield.typeText("test-email@test.fr")
            XCTAssertEqual(emailTexfield.value as! String, "test-email@test.fr", "Text field value is not correct")

            XCTAssertTrue(confirmEmailTexfield.exists, "Text field doesn't exist")
            confirmEmailTexfield.tap()
            confirmEmailTexfield.typeText("test-email@test.fr")
            XCTAssertEqual(confirmEmailTexfield.value as! String, "test-email@test.fr", "Text field value is not correct")

            XCTAssertTrue(passwordTexfield.exists, "Text field doesn't exist")
            passwordTexfield.tap()
            passwordTexfield.typeText("test-mdp")
            XCTAssertEqual(passwordTexfield.value as! String, "••••••••", "Text field value is not correct")

            app/*@START_MENU_TOKEN@*/.buttons["Hide keyboard"]/*[[".keyboards.buttons[\"Hide keyboard\"]",".buttons[\"Hide keyboard\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()


            btnSignup.forceTapElement()

            // PAGE GESTIONNAIRE UTILISATEUR - DESCRIPTION

            let descriptionTextView = scrollViewsQuery.otherElements.containing(.image, identifier:"LogoEtNomS").children(matching: .textView).element

            XCTAssertTrue(descriptionTextView.exists, "Text field doesn't exist")
            descriptionTextView.tap()
            descriptionTextView.typeText("Text description personnelle")
            XCTAssertEqual(descriptionTextView.value as! String, "Text description personnelle", "Text field value is not correct")

            scrollViewsQuery.otherElements.containing(.image, identifier:"LogoEtNomS").element/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeUp()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/

            let btnContinue = elementsQuery2/*@START_MENU_TOKEN@*/.staticTexts["Continuer"]/*[[".buttons[\"Continuer\"].staticTexts[\"Continuer\"]",".staticTexts[\"Continuer\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            app/*@START_MENU_TOKEN@*/.buttons["Hide keyboard"]/*[[".keyboards.buttons[\"Hide keyboard\"]",".buttons[\"Hide keyboard\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
            btnContinue.forceTapElement() // description personnelle

            // PAGE ASSOCIATION

            let nameAssociationTextField = elementsQuery2.textFields["Nom de l'association"]
            nameAssociationTextField.tap()
            nameAssociationTextField.typeText("test nom de l'association")
            XCTAssertEqual(nameAssociationTextField.value as! String, "test nom de l'association", "Text field value is not correct")

            scrollViewsQuery.otherElements.containing(.image, identifier:"LogoEtNomS").element.swipeUp()

            XCTAssertTrue(descriptionTextView.exists, "Text field doesn't exist")
            descriptionTextView.tap()
            descriptionTextView.typeText("Text description association Text description assoc")
            XCTAssertEqual(descriptionTextView.value as! String, "Text description association Text description assoc", "Text field value is not correct")

            app/*@START_MENU_TOKEN@*/.buttons["Hide keyboard"]/*[[".keyboards.buttons[\"Hide keyboard\"]",".buttons[\"Hide keyboard\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()

            btnContinue.forceTapElement() // description association

            sleep(2)


    }
}

extension XCUIElement {
    func forceTapElement() {
        if self.isHittable {
            self.tap()
        }
        else {
            let coordinate: XCUICoordinate = self.coordinate(withNormalizedOffset: CGVector(dx:0.0, dy:0.0))
            coordinate.tap()
        }
    }

    func clearText() {
        guard let stringValue = self.value as? String else {
            return
        }
        // workaround for apple bug
        if let placeholderString = self.placeholderValue, placeholderString == stringValue {
            return
        }

        var deleteString = String()
        for _ in stringValue {
            deleteString += XCUIKeyboardKey.delete.rawValue
        }
        typeText(deleteString)
    }
}



