import bodyParser from "body-parser";
import express from "express";
import {router} from './router/router';
import { evenementRouter } from "./router/routerEvenement";
import { benevoleRouter } from "./router/routerBenevole";
import { associationRouter } from "./router/routerAssociation";
import { gestionaireAssociationRouter } from "./router/routerGestionnaireAssociation";
import { supportTechniqueRouter } from "./router/routerSupportTechnique";
import { adminRouter } from "./router/routerAdmin";
import {createConnection} from "typeorm";
import { config } from "dotenv";
import { tacheRouter } from "./router/routerTache";
import { avisRouter } from "./router/routerAvis";
import { projetRouter } from "./router/routerProjet";
import { authGestionnaireAsso_router } from "./router/routerAuthGestionnaireAsso";


config()
const app = express();
const morgan = require('morgan');

const path = require('path');
const rfs = require('rotating-file-stream');

const accessLogStream = rfs.createStream('access.log', {
    interval: '1d',
    path: path.join(__dirname, 'log')
});


const expressOasGenerator = require('express-oas-generator');
expressOasGenerator.handleResponses(app, {});

const port = process.env.PORT || 8010;


//app.engine('handlebars',expressHBs({defaultLayout: 'main'}));
//app.set('view engine', 'handlebars');



createConnection({
    type: "mysql",
    logging: true,
    host: "127.0.0.1", //192.168.1.20:85 192.168.1.11
    port: 3306,
    username: "root",
    password: "root",
    database: "connectasso",
    entities: [__dirname + "/**/model/*.ts"],
    synchronize: true
}).then(() => {
    app.use(morgan('combined', {stream: accessLogStream}));
    morgan.token('host', function(req, res){
        return req.hostname;
    });

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    
    app.use('/api/v1', router);
    app.use('/api/v1/administrateur', adminRouter);
    app.use('/api/v1/association', associationRouter);
    app.use('/api/v1/avis', avisRouter);
    app.use('/api/v1/benevole', benevoleRouter);
    app.use('/api/v1/evenement', evenementRouter);
    app.use('/api/v1/gestionnaireAssociation', gestionaireAssociationRouter);
    app.use('/api/v1/supportTechnique', supportTechniqueRouter);
    app.use('/api/v1/tache', tacheRouter);
    app.use('/api/v1/projet', projetRouter); 
    app.use('/api/v1/authGestionnaireAsso', authGestionnaireAsso_router);
    
    expressOasGenerator.handleRequests();

    app.listen(port, () => console.log('Le serveur ecoute sur le port ' + port));
});