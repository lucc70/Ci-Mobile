import express from "express";
import { addToEventsList } from "../controller/controllerAssociation";
import { deleteGestionaireAssociation, getAssociationOfGA, getGestionaireAssociation, getGestionaireAssociationById, insertGestionaireAssociation, modifyGestionaireAssociation, putAssociationToGA } from "../controller/controllerGestionnaireAssociation";
const gestionaireAssociationRouter = express.Router();

gestionaireAssociationRouter.get('/',getGestionaireAssociation);
gestionaireAssociationRouter.get('/:id', getGestionaireAssociationById);
gestionaireAssociationRouter.delete('/:id', deleteGestionaireAssociation);
gestionaireAssociationRouter.post('/', insertGestionaireAssociation);
gestionaireAssociationRouter.patch('/:id', modifyGestionaireAssociation);
//gestionaireAssociationRouter.put('/inEventList/:associationId/:evenementId', addEventsList);
// gestionaireAssociationRouter.delete('/inEventList/:associationId/:evenementId', removedEventsList);
//benevoleRouter.put('/inSignList/:benevoleId/:evenementId', addEventsToSignList); 



gestionaireAssociationRouter.get('/association/gestionnaireAssociationId=:gestionnaireAssociationId', async (req,res)=>{
    try {
        const gestionnaireAssociationId = req.params.gestionnaireAssociationId;
        const association = await getAssociationOfGA(gestionnaireAssociationId);
        res.json(association);
    } catch (err) {
        res.status(400).json(err);
    }
});


gestionaireAssociationRouter.put('/gestionnaireAssociation/gestionnaireAssociationId=:gestionnaireAssociationId/associationId=:associationId', putAssociationToGA);


export{
    gestionaireAssociationRouter
}