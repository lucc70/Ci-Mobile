import express from "express";
import { deleteProjet, getProjet, getProjetById, insertProjet, modifyProjet, removeTaskFromProject } from "../controller/controllerProjet";
const projetRouter = express.Router();


projetRouter.get('/', getProjet);
projetRouter.get('/projetId=:id', getProjetById);
projetRouter.delete('/projetId=:id', deleteProjet);
projetRouter.post('/', insertProjet);
projetRouter.patch('/projetId=:id', modifyProjet);

projetRouter.delete('/tache/projetId=:projetId/tacheId=:tacheId', removeTaskFromProject);

export{
    projetRouter
}