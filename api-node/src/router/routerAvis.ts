import express from "express";
import { getRepository } from "typeorm";
import { createAvisWithGAtoBenevole, deleteAvis, getAvis, getAvisById, insertAvis, modifyAvis, putBenevoleToAvis, putGAandBenevoleToAvis, putGAToAvis } from "../controller/controllerAvis";
import { Avis } from "../model/modelAvis";
const avisRouter = express.Router();


avisRouter.get('/', getAvis);
avisRouter.get('/:id', getAvisById);
avisRouter.delete('/:id', deleteAvis);
avisRouter.post('/:benevoleId/:gestionnaireAssociationId', insertAvis);
avisRouter.patch('/:id', modifyAvis);

avisRouter.put('/avisId=:avisId/benevoleId=:benevoleId/gestionnaireAssociationId=:gestionnaireAssociationId', putGAandBenevoleToAvis);

avisRouter.put('/benevole/avisId=:avisId/benevoleId=:benevoleId',putBenevoleToAvis);

avisRouter.put('/GA/avisId=:avisId/gestionnaireAssociationId=:gestionnaireAssociationId',putGAToAvis);

// avisRouter.put('/benevoleId=:benevoleId/gestionnaireAssociationId=:gestionnaireAssociationId', insertAvis, async (req, res) => {
//     try {
//         const gestionnaireAssociationId = req.params.gestionnaireAssociationId;
//         const benevoleId = req.params.benevoleId;
//         const avisController = await getRepository(Avis);
//         const avis = await avisController.getId;
//         await avis.createAvisWithGAtoBenevole(gestionnaireAssociationId, benevoleId);
//         logger.info(`User ${(req.user as User).username} has accepted an invitation to an organisation with id ${organisationId}`);

//         res.status(204).end();
//     } catch (error) {
//         logger.error(`${req.route.path} \n ${error}`);
//         res.status(400).json(error);
//     }
// });

export{
    avisRouter
}