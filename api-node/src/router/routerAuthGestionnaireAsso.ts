import {Router} from "express";
import { ControllerAuthGestionnaireAsso } from "../controller/controllerAuthGestionnaireAsso";

const authGestionnaireAsso_router = Router();

authGestionnaireAsso_router.post("/signin",async (req,res)=> {

    const controllerAuthGestionnaireAsso : ControllerAuthGestionnaireAsso = ControllerAuthGestionnaireAsso.getInstance()

    const loginInfo = await controllerAuthGestionnaireAsso.signIn({...req.body});
   
    res.status(loginInfo.status).json(loginInfo);
    
});

export {
    authGestionnaireAsso_router
};