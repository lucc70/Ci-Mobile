import express from "express";
import { addTaskToProject, deleteTache, getTache, getTacheById, insertTache, modifyTache} from "../controller/controllerTache";
const tacheRouter = express.Router();


tacheRouter.get('/', getTache);
tacheRouter.get('/tacheId=:id', getTacheById);
tacheRouter.delete('/tacheId=:id', deleteTache);
tacheRouter.post('/', insertTache);
tacheRouter.patch('/tacheId=:id', modifyTache);

tacheRouter.put('/projet/tacheId=:tacheId/projetId=:projetId', addTaskToProject);

export{
    tacheRouter
}