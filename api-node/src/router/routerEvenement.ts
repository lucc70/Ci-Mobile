import express from "express";
import {getEvenements,getEvenementById,insertEvenement,modifyEvenement,deleteEvenement, getParticipantsInscrit, getParticipantsEnAttente, acceptBenevoleForAnEvent, getEvenementAvenir, getEvenementAvenirByAssociationId, createEventAndPutAsso, giveAssoToEvent, removeBenevoleFromSignInList, removedBenevoleFromWaitingList, getEvenementTermnineByAssociationId, getEvenementAnnuleByAssociationId} from "../controller/controllerEvenement";
const evenementRouter = express.Router();

evenementRouter.get('/', getEvenements);
evenementRouter.get('/:id', getEvenementById);
evenementRouter.post('/', insertEvenement);
evenementRouter.patch('/:id', modifyEvenement);
evenementRouter.delete('/:id', deleteEvenement);
evenementRouter.post('/accept/evenementId=:evenementId/benevoleId=:benevoleId', acceptBenevoleForAnEvent);
evenementRouter.get('/statut/avenir', getEvenementAvenir);


evenementRouter.post('/createEvent/associationId=:associationId', createEventAndPutAsso);
evenementRouter.post('/giveAssoToEvent/associationId=:associationId/evenementId=:evenementId', giveAssoToEvent);
evenementRouter.delete('/signlist/evenementId=:evenementId/benevoleId=:benevoleId', removeBenevoleFromSignInList); 
evenementRouter.delete('/refused/evenementId=:evenementId/benevoleId=:benevoleId', removedBenevoleFromWaitingList);


evenementRouter.get('/statut/avenir/AssociationId=:id', getEvenementAvenirByAssociationId);
evenementRouter.get('/statut/annule/AssociationId=:id', getEvenementAnnuleByAssociationId);
evenementRouter.get('/statut/termine/AssociationId=:id', getEvenementTermnineByAssociationId);



// getParticipantsInscrit getEvenementAvenirByAssociationId
evenementRouter.get('/participantsinscrits/evenement.id=:evenementId', async (req,res)=>{
    try {
        const evenementId = req.params.evenementId;
        const evenement = await getParticipantsInscrit(evenementId);
        res.json(evenement);
    } catch (err) {
        res.status(400).json(err);
    }
});


evenementRouter.get('/participantsenattente/evenement.id=:evenementId', async (req,res)=>{
    try {
        const evenementId = req.params.evenementId;
        const evenement = await getParticipantsEnAttente(evenementId);
        res.json(evenement);
    } catch (err) {
        res.status(400).json(err);
    }
});



export{
    evenementRouter
}