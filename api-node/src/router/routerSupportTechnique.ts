import express from "express";
import { assignTaskToSupport, deleteSupportTechnique, getSupportTechnique, getSupportTechniqueById, insertSupportTechnique, modifySupportTechnique, pullTaskToSupport } from "../controller/controllerSupportTechnique";
const supportTechniqueRouter = express.Router();
//http://127.0.0.1:8010/api/v1/supportTechnique/
///api/v1/supportTechnique
supportTechniqueRouter.get('/', getSupportTechnique);
supportTechniqueRouter.get('/supportId=:supportId', getSupportTechniqueById);
supportTechniqueRouter.delete('/supportId=:id', deleteSupportTechnique);
supportTechniqueRouter.post('/', insertSupportTechnique);
supportTechniqueRouter.patch('/supportId=:id', modifySupportTechnique);

supportTechniqueRouter.put('/task/supportId=:supportId/tacheId=:tacheId', assignTaskToSupport);
supportTechniqueRouter.delete('/task/supportId=:supportId/tacheId=:tacheId', pullTaskToSupport);


export{
    supportTechniqueRouter
}