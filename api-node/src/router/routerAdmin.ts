import express from "express";
import { getAdmins, getAdminById, modifyAdmin, deleteAdmin, insertAdmin } from "../controller/controllerAdministrateur";
const adminRouter = express.Router();

adminRouter.get('/',getAdmins);
adminRouter.get('/admin=:id', getAdminById);
adminRouter.delete('/admin=:id', deleteAdmin);
adminRouter.post('/', insertAdmin);
adminRouter.patch('/admin=:id', modifyAdmin);

export {
    adminRouter
}
