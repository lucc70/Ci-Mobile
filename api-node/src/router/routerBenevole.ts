import express from "express";
import { addEventsToSignList, inscriptionEvenement, deleteBenevole, getBenevole, getBenevoleById, insertBenevole, modifyBenevole, removedEventsToSignList, removedEventsToWaitingList, getEvenementEnAttenteForOneBenevole, getEvenementInscritForOneBenevole, desinscriptionEvent } from "../controller/controllerBenevole";
const benevoleRouter = express.Router();


benevoleRouter.get('/', getBenevole);
benevoleRouter.get('/benevoleId=:id', getBenevoleById);
benevoleRouter.delete('/:id', deleteBenevole);
benevoleRouter.post('/', insertBenevole);
benevoleRouter.patch('/:id', modifyBenevole);
benevoleRouter.put('/inWaitingList/benevoleId=:benevoleId/evenementId=:evenementId', inscriptionEvenement);
benevoleRouter.delete('/inWaitingList/:benevoleId/:evenementId', removedEventsToWaitingList);
benevoleRouter.put('/inSignList/benevoleId=:benevoleId/evenementId=:evenementId', addEventsToSignList);
benevoleRouter.delete('/inSignList/:benevoleId/:evenementId', desinscriptionEvent);


//  /evenementEnAttente/benevoleId=

benevoleRouter.get('/evenementEnAttente/benevoleId=:benevoleId', async (req,res)=>{
    try {
        const benevoleId = req.params.benevoleId;
        const benevole = await getEvenementEnAttenteForOneBenevole(benevoleId);
        res.json(benevole);
    } catch (err) {
        res.status(400).json(err);
    }
});

benevoleRouter.get('/evenementInscrit/benevoleId=:benevoleId', async (req,res)=>{
    try {
        const benevoleId = req.params.benevoleId;
        const benevole = await getEvenementInscritForOneBenevole(benevoleId);
        res.json(benevole);
    } catch (err) {
        res.status(400).json(err);
    }
});


// getEvenenementEnAttente
// benevoleRouter.get('/evenementattente/benevoleId=:benevoleId', async (req,res)=>{
//     try {
//         const benevoleId = req.params.benevoleId;
//         const evenement = await getParticipantsInscrit(evenementId);
//         res.json(evenement);
//     } catch (err) {
//         res.status(400).json(err);
//     }
// });

// benevoleRouter.patch('/moveList/:benevoleId/:evenementId',moveEventsToWaitingListToSignList);



export{
    benevoleRouter
}