import express from "express";
import {getAssociation, getAssociationById, deleteAssociation, insertAssociation, modifyAssociation, addToEventsList, removedToEventsList, getAllEvenementOfAssociation, getEvenementOfAssociationAvenir, createAssociationAndPutGa, giveGAtoAssociation } from "../controller/controllerAssociation";
const associationRouter = express.Router();


associationRouter.get('/', getAssociation);
associationRouter.get('/associationId=:id', getAssociationById);
associationRouter.delete('/associationId=:id', deleteAssociation);
associationRouter.post('/', insertAssociation);
associationRouter.patch('/associationId=:id', modifyAssociation);
associationRouter.put('/inEventList/associationId=:associationId/evenementId=:evenementId', addToEventsList);

associationRouter.delete('/inEventList/associationId=:associationId/evenementId=:evenementId', removedToEventsList);


associationRouter.get('/evenement/associationId=:id', async (req,res)=>{
    try {
        const associationId = req.params.id;
        const evenements = await getAllEvenementOfAssociation(associationId);
        res.json(evenements);
    } catch (err) {
        res.status(400).json(err);
    }
});

associationRouter.get('/evenement/a-venir/associationId=:associationId', async (req,res)=>{
    try {
        const associationId = req.params.associationId;
        const statut = "a-venir";
        const evenements = await getEvenementOfAssociationAvenir(associationId,statut);
        res.json(evenements);
    } catch (err) {
        res.status(400).json(err);
    }
});

associationRouter.get('/evenement/annule/associationId=:associationId', async (req,res)=>{
    try {
        const associationId = req.params.associationId;
        const statut = "annule";
        const evenements = await getEvenementOfAssociationAvenir(associationId,statut);
        res.json(evenements);
    } catch (err) {
        res.status(400).json(err);
    }
});

associationRouter.get('/evenement/termine/associationId=:associationId', async (req,res)=>{
    try {
        const associationId = req.params.associationId;
        const statut = "termine";
        const evenements = await getEvenementOfAssociationAvenir(associationId,statut);
        res.json(evenements);
    } catch (err) {
        res.status(400).json(err);
    }
});

associationRouter.post('/createAsso/gestionnaireAssociationId=:gestionnaireAssociationId', createAssociationAndPutGa);

associationRouter.put('/gestionnaireAssociation/associationId=:associationId/gestionnaireAssociationId=:gestionnaireAssociationId', giveGAtoAssociation);
//benevoleRouter.put('/inSignList/:benevoleId/:evenementId', addEventsToSignList); getEvenementOfAssociation

export{
    associationRouter
}