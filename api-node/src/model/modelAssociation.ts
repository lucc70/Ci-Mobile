import { Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Evenement } from "./modelEvenement";
import { GestionaireAssociation } from "./modelGestionnaireAssociation";

@Entity()
export class Association{
    @PrimaryGeneratedColumn()
    id: string;
    @Column()
    nom: string;
    @Column({length: 2000})
    info: string;
    @Column()
    creation: string;
    @OneToMany(()=>Evenement,evenement=>evenement.association, {onDelete: "CASCADE"})//,{eager:true}
    evenement: Evenement[];
    @OneToOne(()=>GestionaireAssociation, gestionnaireAssociation=>gestionnaireAssociation.association, {eager: true, cascade: true ,onDelete: "CASCADE"})//,{eager:true,cascade:true}
    @JoinColumn()
    gestionnaireAssociation:GestionaireAssociation;
}
