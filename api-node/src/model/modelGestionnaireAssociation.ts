import { join } from "path";
import { Column, Entity, JoinColumn, ManyToMany, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Association } from "./modelAssociation";
import { Avis } from "./modelAvis";
import { Evenement } from "./modelEvenement";

export interface gestionnaireAssociationProps {
    nom : string;
    prenom : string;
    password : string;
    mail : string;
    description : string;
    
}

@Entity()
export class GestionaireAssociation implements gestionnaireAssociationProps{
    @PrimaryGeneratedColumn()
    id:String;
    @Column()
    nom:string;
    @Column()
    prenom:string;
    @Column()
    password:string;
    @Column()
    mail:string;
    @Column({length: 2000})
    description:string;
    @OneToOne(()=>Association, association=>association.gestionnaireAssociation, {onDelete: "CASCADE"})//,{)//,{eager:true,cascade:true}
    @JoinColumn()
    association:Association;
    @OneToMany(()=>Avis, avis=>avis.gestionnaireAssociation, {onDelete: "CASCADE"})//,{)//,{eager:true,cascade:true,onDelete: "CASCADE"}
    avis:Avis[]
}

// many to many = join table ; one to many = join colomn

//,{eager:true}