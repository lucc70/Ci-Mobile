import { join } from "path";
import { Column, Entity, JoinColumn, JoinTable, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { Tache } from "./modelTache";

@Entity()
export class SupportTechnique{
    @PrimaryGeneratedColumn()
    id:number;
    @Column()
    nom:string;
    @Column()
    prenom:string;
    @Column()
    mail:string;
    @Column()
    password:string;
    @ManyToMany(()=>Tache,tache=>tache.supportTechnique,{eager:true,cascade:true})
    @JoinTable()
    tache:Tache[];
}