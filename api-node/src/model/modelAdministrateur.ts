import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Admin{
    @PrimaryGeneratedColumn()
    id:string;
    @Column()
    nom:string;
    @Column()
    prenom:string;
    @Column()
    mail:string;
    @Column()
    password:string;
}