import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { tacheRouter } from "../router/routerTache";
import { Tache } from "./modelTache";

@Entity()
export class Projet{
    @PrimaryGeneratedColumn()
    id:number;
    @Column()
    nom:string;
    @OneToMany(()=>Tache, taches=>taches.projet)
    taches:Tache[];
}