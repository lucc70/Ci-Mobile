import { Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Projet } from "./modelProjet";
import { SupportTechnique } from "./modelSupportTechnique";

@Entity()
export class Tache {
    @PrimaryGeneratedColumn()
    id:number;
    @Column()
    titre:string;
    @Column({length: 500})
    descriptionTache:string;
    @Column({nullable:true})
    dateDeDebut: string;
    @Column({nullable:true})
    dateDeFin: string;
    @ManyToMany(()=>SupportTechnique,supportTechnique=>supportTechnique.tache)
    @JoinTable()
    supportTechnique:SupportTechnique[];
    @ManyToOne(()=>Projet,projet=>projet.taches,{eager:true,cascade:true})
    projet:Projet;
}


// export async function acceptBenevoleForAnEvent(req,res) {
//     let evenementAverifier = await getRepository(Evenement).findOne(req.params.evenementId)

//     if (evenementAverifier["complet"] == true) {
//         res.json("Erreur, evenement complet ajout impossible");
//     } else {
//         await getRepository(Evenement).createQueryBuilder()
//             .relation("participantsEnAttente")
//             .of(req.params.benevoleId)
//             .remove(req.params.evenementId);

//             await getRepository(Evenement).createQueryBuilder()
//             .relation("participantsInscrits")
//             .of(req.params.benevoleId)
//             .add(req.params.evenementId);

//             const post = await getRepository(Evenement).findOne(req.params.evenementId);
//             await getRepository(Evenement).update(req.params.evenementId, { nombreParticipants: post.nombreParticipants + 1})
//             if (post.nombreParticipants + 1 == evenementAverifier["besoin"]) {
//                 const post = await getRepository(Evenement).findOne(req.params.evenementId);
//                 await getRepository(Evenement).update(req.params.evenementId, { complet: true });
//             }

//             res.json(await getRepository(Evenement).findOne(req.params.evenementId));
//             const benevole = await getRepository(Benevole).findOne(req.params.benevoleId);
//             const evenement = await getRepository(Evenement).findOne(req.params.evenementId);
//              sendMail({
//                  to: benevole.mail, 
//                  from: process.env.MAILER_USER,// `"ConnectAsso" <${process.env.MAILER_USER}>`
//                  subject: "Participation accepté",
//                  text: `Bonjour ${benevole.prenom}, Votre participation à l'événement : ${evenement.titre} qui aura lieu le ${evenement.debut} au ${evenement.adresse}, A été accepté, félicitation !`
//              }).catch();
//     //supprime le benevole de la liste des participants en attentes de l'évenement
    
//     }
// }