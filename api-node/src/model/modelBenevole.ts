import { Column, Entity, JoinColumn, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Avis } from "./modelAvis";
import { Evenement } from "./modelEvenement";

@Entity()
export class Benevole {
    @PrimaryGeneratedColumn()
    id:string;
    @Column()
    nom:string;
    @Column()
    prenom:string;
    @Column()
    mail:string;
    @Column()
    password:string;
    @Column({length: 2000})
    description:string;
    @OneToMany(()=>Avis, avis=>avis.benevole)
    avis:Avis[]; // tableau d'avis
    @ManyToMany(()=>Evenement, evenement=>evenement.participantsInscrits, {onDelete: "CASCADE"})//,{) //,cascade:true
    @JoinTable()
    evenementsInscrit: Evenement[];
    @ManyToMany(()=>Evenement, evenement=>evenement.participantsEnAttente,{cascade:true, onDelete: "CASCADE"})
    @JoinTable() // many to many = join table ; one to many = join colomn
    evenementsEnAttente: Evenement[];
}
//     @ManyToMany(()=>Evenement, evenement=>evenement.participantsInscrits,{eager:true}) //,{eager:true}
//     @JoinTable()
//     evenementsInscrit: Evenement[];
//     @ManyToMany(()=>Evenement, evenement=>evenement.participantsEnAttente,
//)
//     @JoinTable() // many to many = join table ; one to many = join colomn
//     evenementsEnAttente: Evenement[];
// }