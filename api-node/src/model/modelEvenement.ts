import { Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { associationRouter } from "../router/routerAssociation";
import { Association } from "./modelAssociation";
import { Benevole } from "./modelBenevole";

@Entity()
export class Evenement {
    @PrimaryGeneratedColumn()
    id:string;
    @Column()
    titre:string;
    @Column()
    debut:string;
    @Column()
    fin:string;
    @ManyToOne(()=>Association,association=>association.evenement,{ eager:true,cascade:true})
    association:Association;
    @Column() // a changer en id 
    organisateur:string;
    @Column({length: 2000})
    description:string;
    @Column()
    adresse:string;
    @Column({ default: "a-venir" })
    statut:string; //A venir, fini, annulé
    @Column({ default: false })
    complet:boolean;
    @Column({ default: 1 })
    besoin:number;
    @Column({ default: 0 })
    nombreParticipants:number;
    @ManyToMany(()=>Benevole, benevole => benevole.evenementsEnAttente,{eager:true,  onDelete: "CASCADE"})
    @JoinTable()
    participantsEnAttente:Benevole[];
    @ManyToMany(()=>Benevole, benevole => benevole.evenementsInscrit,{eager:true,cascade:true, onDelete: "CASCADE"})// , {eager:true}
    @JoinTable()
    participantsInscrits:Benevole[];
}
//     @ManyToMany(()=>Benevole, benevole => benevole.evenementsEnAttente,{onDelete: "CASCADE",cascade:true})
//     @JoinTable()
//     participantsEnAttente:Benevole[];
//     @ManyToMany(()=>Benevole, benevole => benevole.evenementsInscrit,{onDelete: "CASCADE",cascade:true}) // ,cascade:true
//     @JoinTable()
//     participantsInscrits:Benevole[];
// }