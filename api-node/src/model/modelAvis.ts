import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Benevole } from "./modelBenevole";
import { GestionaireAssociation } from "./modelGestionnaireAssociation";

@Entity()
export class Avis {
    @PrimaryGeneratedColumn()
    id:string;
    @Column()
    note:number;
    @Column({length: 400})
    commentaire:string;
    @ManyToOne(()=>Benevole,benevole=>benevole.avis,{eager:true,cascade:true})
    benevole:Benevole;        
    @ManyToOne(()=>GestionaireAssociation,gestionnaireAssociation=>gestionnaireAssociation.avis,{eager:true,cascade:true})
    gestionnaireAssociation: GestionaireAssociation;
}