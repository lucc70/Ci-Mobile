import { getRepository } from "typeorm";
import { sendMail } from "../config/mailer.config";
import { Association } from "../model/modelAssociation";
import { Evenement } from "../model/modelEvenement";
import { GestionaireAssociation } from "../model/modelGestionnaireAssociation";


    export async function getAssociation(req,res){
        res.json(await getRepository(Association).find());
    }

    export async function getAssociationById(req,res){
        res.json(await getRepository(Association).findOne(req.params.id));
    }

    export async function modifyAssociation(req,res){
        const association = getRepository(Association).update(req.params.id,req.body);
        res.json(await getRepository(Association).findOne(req.params.id));
    }

    export async function insertAssociation(req,res){
        const association = getRepository(Association).create(req.body);
        res.json(await getRepository(Association).save(association));
        return association["id"];
    }


    export async function deleteAssociation(req,res){
        const association = getRepository(Association).delete(req.params.id);
        res.status(204).send(); 
    }

    export async function addToEventsList(req,res) {
        await getRepository(Association).createQueryBuilder()
            .relation("evenement")
            .of(req.params.associationId)
            .add(req.params.evenementId);
            //res.status(204).send();
            res.json(await getRepository(Association).findOne(req.params.associationId));
            
           const association = await getRepository(Association).findOne(req.params.associationId);
           const evenement = await getRepository(Evenement).findOne(req.params.evenementId);
            sendMail({
                to: association.gestionnaireAssociation.mail, // association.gestionnaireAssociation.mail
                from: 'lcoujonde@myges.fr',// `"ConnectAsso" <${process.env.MAILER_USER}>`
                subject: "Confirmation de création d'un événement",
                text: `Connect Asso vous confirme que votre événement : ${evenement.titre} qui aura lieu le ${evenement.debut} au ${evenement.adresse}, A bien été créé.`
            }).catch();
    }


    export async function removedToEventsList(req,res) {
        await getRepository(Association).createQueryBuilder()
            .relation("evenement")
            .of(req.params.associationId)
            .remove(req.params.evenementId);
            //res.status(204).send();
            res.json(await getRepository(Association).findOne(req.params.associationId));
    }

    

    export async function getAllEvenementOfAssociation(associationId : any): Promise<Evenement[]> {
        return await getRepository(Evenement).createQueryBuilder()
        .leftJoin("Evenement.association", "Association")
        .where("Association.id=:associationId",{associationId})
        // .leftJoin("Evenement.statut", "statut")
        // .andWhere("statut==a-venir")
        
        .getMany();
    }

    export async function getEvenementOfAssociationAvenir(associationId : any, statut : any): Promise<Evenement[]> {
        return await getRepository(Evenement).createQueryBuilder()
        .leftJoin("Evenement.association", "Association")
        .where("Association.id=:associationId",{associationId},)
        .andWhere("Evenement.statut=:statut",{statut})
        .getMany();
    }

    export async function giveGAtoAssociation(req,res){
        await getRepository(Association).createQueryBuilder()
            .relation("gestionnaireAssociation")
            .of(req.params.associationId)
            .set(req.params.gestionnaireAssociationId);
        res.json(await getRepository(Association).findOne(req.params.associationId));
    }

    export async function createAssociationAndPutGa(req,res){

        let association = getRepository(Association).create(req.body); //{...req.body}
        association = await getRepository(Association).save(association)

        await getRepository(Association).createQueryBuilder()
            .relation("gestionnaireAssociation")
            .of(association["id"]) 
            .set(req.params.gestionnaireAssociationId);
        res.json(await getRepository(Association).findOne(association["id"]));
    }

    

 