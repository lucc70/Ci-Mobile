import { any } from "joi";
import { getConnection, getRepository, QueryResult } from "typeorm";
import { Avis } from "../model/modelAvis";
import { Benevole } from "../model/modelBenevole";
import { GestionaireAssociation } from "../model/modelGestionnaireAssociation";


    export async function getAvis(req,res){
        res.json(await getRepository(Avis).find());
    }
    export async function getAvisById(req,res){
        res.json(await getRepository(Avis).findOne(req.params.id));
    }

    export async function modifyAvis(req,res){
        const avis = getRepository(Avis).update(req.params.id, req.body);
        res.json(await getRepository(Avis).findOne(req.params.id));
    }

    export async function insertAvis(req,res){
        let avis= getRepository(Avis).create({...req.body}); //, benevole : req.params.benevoleId, gestionnaireAssociation : req.params.gestionnaireAssociationId
        avis = await getRepository(Avis).save(avis)

        await getRepository(Avis).createQueryBuilder()
            .relation("benevole")
            .of(avis["id"]) 
            .set(req.params.benevoleId);

        await getRepository(Avis).createQueryBuilder()
            .relation("gestionnaireAssociation")
            .of(avis["id"])
            .set(req.params.gestionnaireAssociationId);
        res.json(await getRepository(Avis).findOne(avis["id"]));
    }

    export async function deleteAvis(req,res){
        const avis = getRepository(Avis).delete(req.params.id);
        res.status(204).send();
    }

    export async function putAvisToGA(req,res) {
        await getRepository(Avis).createQueryBuilder()
        .relation("gestionnaireAssociation")
        .of(req.params.gestionnaireAssociationId)
        .add(req.params.avisId)
        res.json(await getRepository(Avis).findOne(req.params.avisId));
    }

    

    // export async function putGAandBenevoleToAvis(req,res) {
    //     await getRepository(Avis).createQueryBuilder()
    //     .relation("benevole")
    //     .of(req.params.avisId)
    //     .set(req.params.benevoleId)
    //     await getRepository(Avis).createQueryBuilder()
    //     .relation("gestionnaireAssociation")
    //     .of(req.params.avisId)
    //     .set(req.params.gestionnaireAssociationId)
    //     res.json(await getRepository(Avis).findOne(req.params.avisId));
    // }

    export async function putGAandBenevoleToAvis(req,res) {
        //await getRepository(Avis).createQueryBuilder()
        //console.log("fonction put benevole")
        await getRepository(Avis).createQueryBuilder()
            .relation("benevole")
            .of(req.params.avisId) //on inverse les entités quand ça marche pas 
            .set(req.params.benevoleId);
 //       console.log("fonction put GA")
        // await getRepository(Avis).createQueryBuilder()
        //     .relation("gestionnaireAssociation")
        //     .of(req.params.avisId) //on inverse les entités quand ça marche pas 
        //     .set(req.params.gestionnaireAssociationId);
        // res.json(await getRepository(Avis).findOne(req.params.tacheId));
    }

    export async function putBenevoleToAvis(req,res) {
        console.log("fonction put benevole")
        await getRepository(Avis).createQueryBuilder()
            .relation("benevole")
            .of(req.params.avisId) //on inverse les entités quand ça marche pas 
            .set(req.params.benevoleId);
            //res.status(204).send();
            res.json(await getRepository(Avis).findOne(req.params.avisId));
    }

    export async function putGAToAvis(req,res) {
        console.log("fonction put GA")
        await getRepository(Avis).createQueryBuilder()
            .relation("gestionnaireAssociation")
            .of(req.params.avisId) //on inverse les entités quand ça marche pas 
            .set(req.params.gestionnaireAssociationId);
            //res.status(204).send();
            res.json(await getRepository(Avis).findOne(req.params.avisId));
    }

    // export async function giveAvisFromeGAToBenevole(req,res) {
    //     insertAvis(any,any)
    //     putAvisToGA(any,any)
    //     putAvisToBenevole(any,any)
    // }

    // export async function create(req, res, benevole: Benevole, gestionnaireAssociation: GestionaireAssociation){
    //     let avis = getRepository(Avis).create({benevole:benevole, gestionnaireAssociation: gestionnaireAssociation});
    //     insertAvis(any,any);
    //     res.json(await getRepository(Avis).save(avis));
    // }

    export async function createAvisWithGAtoBenevole(req,res,benevole: Benevole, gestionnaireAssociation: GestionaireAssociation){
        const avis = getRepository(Avis).create({
            benevole,
            gestionnaireAssociation,
        });
        await getRepository(Avis).save(avis);
        res.json(await getRepository(Avis).save(avis));
    }


        // export async function addToEventsList(req,res) {
    //     await getRepository(Association).createQueryBuilder()
    //         .relation("evenement")
    //         .of(req.params.associationId)
    //         .add(req.params.evenementId);
    //         //res.status(204).send();
    //         res.json(await getRepository(Association).findOne(req.params.associationId));
            
    //        const association = await getRepository(Association).findOne(req.params.associationId);
    //        const evenement = await getRepository(Evenement).findOne(req.params.evenementId);
    //         sendMail({
    //             to: association.gestionnaireAssociation.mail, // association.gestionnaireAssociation.mail
    //             from: 'lcoujonde@myges.fr',// `"ConnectAsso" <${process.env.MAILER_USER}>`
    //             subject: "Confirmation de création d'un événement",
    //             text: `Connect Asso vous confirme que votre événement : ${evenement.titre} qui aura lieu le ${evenement.debut} au ${evenement.adresse}, A bien été créé.`
    //         }).catch();
    // }