import { getRepository } from "typeorm";
import {Projet } from "../model/modelProjet";
import { Tache } from "../model/modelTache";



    export async function getProjet(req,res){
        res.json(await getRepository(Projet).find());
    }
    export async function getProjetById(req,res){
        res.json(await getRepository(Projet).findOne(req.params.id));
    }

    export async function modifyProjet(req,res){
        const projet = getRepository(Projet).update(req.params.id, req.body);
        res.json(await getRepository(Projet).findOne(req.params.id));
    }

    export async function insertProjet(req,res){
        const projet = getRepository(Projet).create(req.body);
        res.json(await getRepository(Projet).save(projet));
    }

    export async function deleteProjet(req,res){
        const projet = getRepository(Projet).delete(req.params.id);
        res.status(204).send();
    }

    export async function removeTaskFromProject(req,res) {
        await getRepository(Projet).createQueryBuilder()
            .relation("taches")
            .of(req.params.tacheId)
            .remove(req.params.projetId);
            res.json(await getRepository(Projet).findOne(req.params.tacheId));
    }