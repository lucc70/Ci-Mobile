import { getRepository } from "typeorm";
import { sendMail } from "../config/mailer.config";
import { Association } from "../model/modelAssociation";
import { Benevole } from "../model/modelBenevole";
import { Evenement } from "../model/modelEvenement";



    export async function getEvenements(req,res){
        res.json(await getRepository(Evenement).find());
    }
    export async function getEvenementById(req,res){
        let evenement = await getRepository(Evenement).findOne(req.params.id)
        res.json(evenement);
        console.log("mail du gestionnaire : " + evenement["association"]["gestionnaireAssociation"]["mail"]); //.gestionnaireAssociation.mail
    }

    export async function  modifyEvenement(req,res){
        const evenement = getRepository(Evenement).update(req.params.id, req.body);
        res.json(await getRepository(Evenement).findOne(req.params.id));
    }

    export async function insertEvenement(req,res){
        const evenement = getRepository(Evenement).create(req.body);
        res.json(await getRepository(Evenement).save(evenement));
    }

  export async function deleteEvenement(req,res){
      const eventId = req.params.id;
      const benevoles = await getRepository(Benevole).createQueryBuilder()
      .leftJoin("Benevole.evenementsInscrit","EvenementA")
      .leftJoin("Benevole.evenementsEnAttente","EvenementB")
      .distinct()
      .where("EvenementA.id=:eventId",{eventId})
      .orWhere("EvenementB.id=:eventId",{eventId})
      .getMany();
      const evenement = await getRepository(Evenement).findOne(req.params.id);
      sendBenevolesCancelMails(benevoles,evenement);
        getRepository(Evenement).delete(req.params.id);
        res.status(204).send();
    }

    async function sendBenevolesCancelMails(benevoles: Benevole[],evenement:Evenement): Promise<void> {
        for(let benevole of benevoles){
            await sendMail({
                to: benevole.mail, 
                from: process.env.MAILER_USER,
                subject: "Participation annulé 😢",
                text: `Bonjour ${benevole.prenom}, Votre participation à l'événement : ${evenement.titre}, a été annulé 😢.`
            });
        }
    }


    export async function getParticipantsInscrit(evenementId: any): Promise<Benevole[]> {
        return await getRepository(Benevole).createQueryBuilder()
            .leftJoin("Benevole.evenementsInscrit", "Evenement")
            .where("Evenement.id=:evenementId", {evenementId})
            .getMany();
    }

    export async function getParticipantsEnAttente(evenementId: any): Promise<Benevole[]> {
        return await getRepository(Benevole).createQueryBuilder()
            .leftJoin("Benevole.evenementsEnAttente", "Evenement")
            .where("Evenement.id=:evenementId", {evenementId})
            .getMany();
    }

    export async function getEvenementAvenir(req,res){
        res.json(await getRepository(Evenement).find({ where: {statut: "a-venir"} }));
    }

    export async function getEvenementAvenirByAssociationId(req,res){
        let evenement = await getRepository(Evenement).find( { where: {statut: "a-venir", id : req.params.id}});
        res.json(evenement);
    }

    export async function getEvenementTermnineByAssociationId(req,res){
        res.json(await getRepository(Evenement).findOne(req.params.id,{ where: {statut: "termine", id : req.params.id} }));
    }
    

    export async function getEvenementAnnuleByAssociationId(req,res){
        let evenement = await getRepository(Evenement).findOne(req.params.id, { where: {statut: "annule", id : req.params.id} });
        res.json(evenement);
    }



    

    export async function acceptBenevoleForAnEvent(req,res) {
        let evenementAverifier = await getRepository(Evenement).findOne(req.params.evenementId)

        if (evenementAverifier["complet"] == true) {
            res.json("Erreur, evenement complet ajout impossible");
        } else {
            await getRepository(Evenement).createQueryBuilder()
                .relation("participantsEnAttente")
                .of(req.params.benevoleId)
                .remove(req.params.evenementId);
    
                await getRepository(Evenement).createQueryBuilder()
                .relation("participantsInscrits")
                .of(req.params.benevoleId)
                .add(req.params.evenementId);
    
                const post = await getRepository(Evenement).findOne(req.params.evenementId);
                await getRepository(Evenement).update(req.params.evenementId, { nombreParticipants: post.nombreParticipants + 1})
                if (post.nombreParticipants + 1 == evenementAverifier["besoin"]) {
                    const post = await getRepository(Evenement).findOne(req.params.evenementId);
                    await getRepository(Evenement).update(req.params.evenementId, { complet: true });
                }
    
                res.json(await getRepository(Evenement).findOne(req.params.evenementId));
                const benevole = await getRepository(Benevole).findOne(req.params.benevoleId);
                const evenement = await getRepository(Evenement).findOne(req.params.evenementId);
                 sendMail({
                     to: benevole.mail, 
                     from: process.env.MAILER_USER,// `"ConnectAsso" <${process.env.MAILER_USER}>`
                     subject: "Participation accepté 😌",
                     text: `Bonjour ${benevole.prenom}, Votre participation à l'événement : ${evenement.titre} qui aura lieu le ${evenement.debut} au ${evenement.adresse}, A été accepté, félicitation 😌 !`
                 }).catch();
        //supprime le benevole de la liste des participants en attentes de l'évenement
        
        }
    }


    export async function removeBenevoleFromSignInList(req,res) {
        await getRepository(Evenement).createQueryBuilder()
            .relation("participantsInscrits")
            .of(req.params.benevoleId)
            .remove(req.params.evenementId);
        
        await getRepository(Evenement).createQueryBuilder()
            .relation("participantsEnAttente")
            .of(req.params.benevoleId)
            .add(req.params.evenementId);
            
        const post = await getRepository(Evenement).findOne(req.params.evenementId);
        await getRepository(Evenement).update(req.params.evenementId, { nombreParticipants: post.nombreParticipants - 1 });
        await getRepository(Evenement).update(req.params.evenementId, { complet: false });
        
        res.json(await getRepository(Evenement).findOne(req.params.evenementId));
        const benevole = await getRepository(Benevole).findOne(req.params.benevoleId);
        const evenement = await getRepository(Evenement).findOne(req.params.evenementId);
        sendMail({
            to: benevole.mail, 
            from: process.env.MAILER_USER,// `"ConnectAsso" <${process.env.MAILER_USER}>`
            subject: "Participation en attente",
            text: `Bonjour ${benevole.prenom}, Votre participation à l'événement : ${evenement.titre} qui aura lieu le ${evenement.debut} au ${evenement.adresse}, A été remise en attente.`
        }).catch();

    }


    export async function removedBenevoleFromWaitingList(req,res) {
        await getRepository(Benevole).createQueryBuilder()
            .relation("evenementsEnAttente")
            .of(req.params.benevoleId) 
            .remove(req.params.evenementId);
            res.json(await getRepository(Evenement).findOne(req.params.evenementId));

            const benevole = await getRepository(Benevole).findOne(req.params.benevoleId);
           const evenement = await getRepository(Evenement).findOne(req.params.evenementId);

           sendMail({
            to: benevole.mail, 
            from: 'lcoujonde@myges.fr',// `"ConnectAsso" <${process.env.MAILER_USER}>`
            subject: "Participation refusé",
            text: `Votre participation à l'événement : ${evenement.titre} qui aura lieu le ${evenement.debut} au ${evenement.adresse}, n'a pas été retenue.`
        }).catch();
    }

    export async function finalRemoveBenevoleFromSignInList(req,res) {
        await getRepository(Evenement).createQueryBuilder()
            .relation("participantsInscrits")
            .of(req.params.benevoleId)
            .remove(req.params.evenementId);
        res.json(await getRepository(Evenement).findOne(req.params.evenementId));
        const benevole = await getRepository(Benevole).findOne(req.params.benevoleId);
        const evenement = await getRepository(Evenement).findOne(req.params.evenementId);
        sendMail({
            to: benevole.mail, 
            from: process.env.MAILER_USER,// `"ConnectAsso" <${process.env.MAILER_USER}>`
            subject: "Evenement Annulé",
            text: `Bonjour ${benevole.prenom}, l'événement : ${evenement.titre} qui devait avoir lieu le ${evenement.debut} au ${evenement.adresse}, A été annulé.`
        }).catch();
    }


    export async function finalRemoveBenevoleFromWaitingList(req,res) {
        await getRepository(Evenement).createQueryBuilder()
            .relation("participantsEnAttente")
            .of(req.params.benevoleId)
            .remove(req.params.evenementId);
        res.json(await getRepository(Evenement).findOne(req.params.evenementId));
        const benevole = await getRepository(Benevole).findOne(req.params.benevoleId);
        const evenement = await getRepository(Evenement).findOne(req.params.evenementId);
        sendMail({
            to: benevole.mail, 
            from: process.env.MAILER_USER,// `"ConnectAsso" <${process.env.MAILER_USER}>`
            subject: "Evenement Annulé",
            text: `Bonjour ${benevole.prenom}, l'événement : ${evenement.titre} qui devait avoir lieu le ${evenement.debut} au ${evenement.adresse}, A été annulé.`
        }).catch();
    }

    export async function moveEventsToWaitingListToSignList(req,res) {
        await getRepository(Benevole).createQueryBuilder()
            .relation("evenementsEnAttente")
            .of(req.params.benevoleId)
            .remove(req.params.evenementId);
        await getRepository(Benevole).createQueryBuilder()
            .relation("evenementsInscrit")
            .of(req.params.benevoleId)
            .add(req.params.evenementId);
            res.json(await getRepository(Benevole).findOne(req.params.benevoleId));
            const benevole = await getRepository(Benevole).findOne(req.params.benevoleId);
            const evenement = await getRepository(Evenement).findOne(req.params.evenementId);
             sendMail({
                 to: benevole.mail, 
                 from: process.env.MAILER_USER,// `"ConnectAsso" <${process.env.MAILER_USER}>`
                 subject: "Participation accepté",
                 text: `Bonjour ${benevole.prenom}, Votre participation à l'événement : ${evenement.titre} qui aura lieu le ${evenement.debut} au ${evenement.adresse}, A été accepté, félicitation !`
             }).catch();
    }

    export async function createEventAndPutAsso(req,res){

        let event = getRepository(Evenement).create(req.body); //{...req.body}
        event = await getRepository(Evenement).save(event)

        await getRepository(Evenement).createQueryBuilder()
            .relation("association")
            .of(event["id"]) 
            .set(req.params.associationId);
        res.json(await getRepository(Evenement).findOne(event["id"]));
    }

    export async function giveAssoToEvent(req,res){
        await getRepository(Evenement).createQueryBuilder()
            .relation("association")
            .of(req.params.evenementId) 
            .set(req.params.associationId);
        res.json(await getRepository(Evenement).findOne(req.params.evenementId));
    }
