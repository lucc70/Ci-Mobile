import {getRepository, Repository} from "typeorm";
import { GestionaireAssociation, gestionnaireAssociationProps } from "../model/modelGestionnaireAssociation";
import { sign } from "jsonwebtoken";
import { compare } from "bcrypt";
import {validate} from "class-validator";

export class ControllerAuthGestionnaireAsso {

    private static instance: ControllerAuthGestionnaireAsso;

    public static getInstance(){
        if(ControllerAuthGestionnaireAsso.instance === undefined)
        {
            ControllerAuthGestionnaireAsso.instance = new ControllerAuthGestionnaireAsso();
        }

        return ControllerAuthGestionnaireAsso.instance;
    }
    

    public async signIn(props:gestionnaireAssociationProps) : Promise<any>{

        const gestionnaireAssoRepository = getRepository(GestionaireAssociation);
        const mail = props.mail ? props.mail : null;
        const password = props.password ? props.password : null;

        let id : String;
        let token : String;
        let gestionnaireAsso : GestionaireAssociation;
        let errorMessage : String;
        let status = 200;
        
        //Recherche de l'existence du user
        try{
            gestionnaireAsso = await gestionnaireAssoRepository.findOneOrFail({
                mail
            });
        

            let checkPassword : Boolean;
            //Compare passwords 
            if(gestionnaireAsso.password === password) {
                checkPassword = true
            }
             //= await compare(password,gestionnaireAsso.password);
            console.log("check :" + checkPassword);
            
            if(checkPassword == true){   
                id = gestionnaireAsso.id;
                console.log("id :" + id);
            }else{
                errorMessage = "Mot de passe incorrect";
                status = 400;
                id = "";
                token = "";
                gestionnaireAsso = null
            }
            console.log("gestionnaireAsso :" + gestionnaireAsso);
            return {
                status : status,
                // id : id,
                // mail : mail,
                
                gestionnaireAsso : gestionnaireAsso,
                errorMessage : errorMessage
            };
        }
        catch(e)
        {
            errorMessage = "Identifiant ou mot de passe introuvable"
            status = 400;
            
            return {
                status : status,
                errorMessage : errorMessage,
            };
        }
    }
    
}