import { getRepository } from "typeorm";
import { Tache } from "../model/modelTache";



    export async function getTache(req,res){
        res.json(await getRepository(Tache).find());
    }
    export async function getTacheById(req,res){
        res.json(await getRepository(Tache).findOne(req.params.id));
    }

    export async function modifyTache(req,res){
        const tache = getRepository(Tache).update(req.params.id, req.body);
        res.json(await getRepository(Tache).findOne(req.params.id));
    }

    export async function insertTache(req,res){
        const tache = getRepository(Tache).create(req.body);
        res.json(await getRepository(Tache).save(tache));
    }

    export async function deleteTache(req,res){
        const tache = getRepository(Tache).delete(req.params.id);
        res.status(204).send();
    }

    export async function addTaskToProject(req,res) {
        await getRepository(Tache).createQueryBuilder()
            .relation("projet")
            .of(req.params.tacheId) //on inverse les entités quand ça marche pas 
            .set(req.params.projetId);
            //res.status(204).send();
            res.json(await getRepository(Tache).findOne(req.params.tacheId));
    }

    