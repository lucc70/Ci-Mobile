import { getRepository } from "typeorm";
import { Association } from "../model/modelAssociation";
import { GestionaireAssociation } from "../model/modelGestionnaireAssociation";


    export async function getGestionaireAssociation(req,res){
        res.json(await getRepository(GestionaireAssociation).find());
    }
    export async function getGestionaireAssociationById(req,res){
        res.json(await getRepository(GestionaireAssociation).findOne(req.params.id));
    }

    export async function modifyGestionaireAssociation(req,res){
        const gestionnaireAssociation = getRepository(GestionaireAssociation).update(req.params.id, req.body);
        res.json(await getRepository(GestionaireAssociation).findOne(req.params.id));
    }

    export async function insertGestionaireAssociation(req,res) : Promise<Number>{
        const gestionnaireAssociation = getRepository(GestionaireAssociation).create(req.body);
        res.json(await getRepository(GestionaireAssociation).save(gestionnaireAssociation));
        console.log(gestionnaireAssociation["id"])
        return gestionnaireAssociation["id"]
    }

    export async function deleteGestionaireAssociation(req,res){
        const gestionnaireAssociation = getRepository(GestionaireAssociation).delete(req.params.id);
        res.status(204).send();
    }


    export async function putAssociationToGA(req,res){
        await getRepository(GestionaireAssociation).createQueryBuilder()
        .relation("association")
        .of(req.params.gestionnaireAssociationId)
        .set(req.params.associationId);
        res.json(await getRepository(GestionaireAssociation).findOne(req.params.associationId));
    }


    export async function createGAwithAssociation(req,res){
        let gestionnaireAssociation = getRepository(GestionaireAssociation).create({...req.body}); //, benevole : req.params.benevoleId, gestionnaireAssociation : req.params.gestionnaireAssociationId
        gestionnaireAssociation = await getRepository(GestionaireAssociation).save(gestionnaireAssociation)

        let association = getRepository(Association).create({...req.body});
        association = await getRepository(Association).save(association)

        await getRepository(GestionaireAssociation).createQueryBuilder()
            .relation("association")
            .of(gestionnaireAssociation["id"]) 
            .set(association["id"]);
        res.json(await getRepository(GestionaireAssociation).findOne(association["id"]));
    }

    export async function getAssociationOfGA(gestionnaireAssociationId : any): Promise<Association> {
        return await getRepository(Association).createQueryBuilder()
        .leftJoin("Association.gestionnaireAssociation", "GestionaireAssociation")
        .where("GestionaireAssociation.id=:gestionnaireAssociationId",{gestionnaireAssociationId})
        .getOne();
    }

    // export async function addGestionnaireAssociaition(req,res) {
    //     await getRepository(Association).createQueryBuilder()
    //         .relation("gestionnaireAssociation")
    //         .of(req.params.associationId)
    //         .set(req.params.gestionnaireAssociationId);
    //         //res.status(204).send();
    //         res.json(await getRepository(Association).findOne(req.params.associationId));
    // }

    // export async function moveEventsToWaitingListToSignList(req,res) {
    //     await getRepository(Benevole).createQueryBuilder()
    //         .relation("evenementsEnAttente")
    //         .of(req.params.benevoleId)
    //         .remove(req.params.evenementId);
    //     await getRepository(Benevole).createQueryBuilder()
    //         .relation("evenementsInscrit")
    //         .of(req.params.benevoleId)
    //         .add(req.params.evenementId);
    //         res.json(await getRepository(Benevole).findOne(req.params.benevoleId));
    //         const benevole = await getRepository(Benevole).findOne(req.params.benevoleId);
    //         const evenement = await getRepository(Evenement).findOne(req.params.evenementId);
    //          sendMail({
    //              to: benevole.mail, 
    //              from: process.env.MAILER_USER,// `"ConnectAsso" <${process.env.MAILER_USER}>`
    //              subject: "Participation accepté",
    //              text: `Votre participation à l'événement : ${evenement.titre} qui aura lieu le ${evenement.debut} au ${evenement.adresse}, A été accepté, félicitation !`
    //          }).catch();
    // }

/* module.exports = {
    getGestionnaireAssociations: function (req,res){
        gestionnaire_association.findAll().then(data =>{
            res.json(data);
        }).catch(function (e){
            console.log(e);
        });
    },
    getGestionnaireAssociation: function (req,res){
      gestionnaire_association.findByPk(req.params.id).then(data => {
        res.json(data);
      }).catch(function (e){
        console.log(e);
    });
    },


    modifyGestionnaireAssociation: function (req,res){

      gestionnaire_association.update({ 'nom' :  req.body.nom,
      'prenom' : req.body.prenom,
      'pass' : req.body.pass,
      'promesse_don' : req.body.promesse_don,
      'mail' : req.body.mail,
      'conversation' : req.body.conversation,
      'notification' : req.body.notification,
      'biographie' : req.body.biographie,
      'association' : req.body.association,
      }, {where:{id:req.params.id}}).then(data=>{
        res.json("ok");
    }).catch((err) => {
        if(err){
            console.log(err);
        }
    });
    },

    insertGestionnaireAssociation: function (req,res){
      var a = { 'nom' :  req.body.nom,
      'prenom' : req.body.prenom,
      'pass' : req.body.pass,
      'promesse_don' : req.body.promesse_don,
      'mail' : req.body.mail,
      'conversation' : req.body.conversation,
      'notification' : req.body.notification,
      'biographie' : req.body.biographie,
      'association' : req.body.association,
      }

      gestionnaire_association.create(a).then(data=>{
        res.json("ok");
    }).catch((err) => {
        if(err){
            console.log(err);
        }
    });
  },


    deleteGestionnaireAssociation: function (req,res){
        gestionnaire_association.destroy({where:{id:req.params.id}}).then(data=>{
          res.json("ok");
      }).catch((err) => {
          if(err){
              console.log(err);
          }
      });
    }
} */