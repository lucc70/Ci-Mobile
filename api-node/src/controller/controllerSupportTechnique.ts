import { getRepository } from "typeorm";
import { SupportTechnique } from "../model/modelSupportTechnique";


    export async function getSupportTechnique(req,res){
        res.json(await getRepository(SupportTechnique).find());
    }
    export async function getSupportTechniqueById(req,res){
        res.json(await getRepository(SupportTechnique).findOne(req.params.supportId));
    }

    export async function modifySupportTechnique(req,res){
        const supportTechnique = getRepository(SupportTechnique).update(req.params.supportId, req.body);
        res.json(await getRepository(SupportTechnique).findOne(req.params.supportId));
    }

    export async function insertSupportTechnique(req,res){
        const supportTechnique = getRepository(SupportTechnique).create(req.body);
        res.json(await getRepository(SupportTechnique).save(supportTechnique));
  }

  export async function deleteSupportTechnique(req,res){
        const supportTechnique = getRepository(SupportTechnique).delete(req.params.supportId);
        res.status(204).send();
    }

    export async function assignTaskToSupport(req,res) {
        await getRepository(SupportTechnique).createQueryBuilder()
            .relation("tache")
            .of(req.params.supportId) //on inverse les entités quand ça marche pas 
            .add(req.params.tacheId);
            //res.status(204).send();
            res.json(await getRepository(SupportTechnique).findOne(req.params.supportId));
    }

    export async function pullTaskToSupport(req,res) {
        await getRepository(SupportTechnique).createQueryBuilder()
            .relation("tache")
            .of(req.params.supportId)
            .remove(req.params.tacheId);
            res.json(await getRepository(SupportTechnique).findOne(req.params.supportId));
    }


/*
module.exports = {
    getSupportTechniques: function (req,res){
        support_technique.findAll().then(data =>{
            res.json(data);
        }).catch(function (e){
            console.log(e);
        });
    },
    getSupportTechnique: function (req,res){
      support_technique.findByPk(req.params.id).then(data => {
        res.json(data);
      }).catch(function (e){
        console.log(e);
    });
    },


    modifySupportTechnique: function (req,res){

      support_technique.update({ 'nom' :  req.body.nom,
      'prenom' : req.body.prenom,
      'mail' : req.body.mail,
      'pass' : req.body.pass,
      'tache' : req.body.tache,
      }, {where:{id:req.params.id}}).then(data=>{
        res.json("ok");
    }).catch((err) => {
        if(err){
            console.log(err);
        }
    });
    },

    insertSupportTechnique: function (req,res){
      var a = { 'nom' :  req.body.nom,
      'prenom' : req.body.prenom,
      'mail' : req.body.mail,
      'pass' : req.body.pass,
      'tache' : req.body.tache,
      }

      support_technique.create(a).then(data=>{
        res.json("ok");
    }).catch((err) => {
        if(err){
            console.log(err);
        }
    });
  },


    deleteSupportTechnique: function (req,res){
        support_technique.destroy({where:{id:req.params.id}}).then(data=>{
          res.json("ok");
      }).catch((err) => {
          if(err){
              console.log(err);
          }
      });
    }
} */