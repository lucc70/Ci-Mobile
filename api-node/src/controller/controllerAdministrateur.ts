import { getRepository } from "typeorm";
import { Admin } from "../model/modelAdministrateur";



    export async function getAdmins (req,res){
        res.json(await getRepository(Admin).find());
    }
    export async function getAdminById(req,res){
        res.json(await getRepository(Admin).findOne(req.params.id));
    }

    export async function insertAdmin (req,res){
        const administrateur = getRepository(Admin).create(req.body);
        res.json(await getRepository(Admin).save(administrateur));
    }

    export async function modifyAdmin (req,res){
        const administrateur = getRepository(Admin).update(req.params.id, req.body);
        res.json(await getRepository(Admin).findOne(req.params.id));
    }

    
    export async function deleteAdmin (req,res){
        const administrateur = getRepository(Admin).delete(req.params.id);
        res.status(204).send();
    }
