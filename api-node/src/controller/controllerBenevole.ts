import { debug } from "console";
import { getRepository } from "typeorm";
import { sendMail } from "../config/mailer.config";
import { Avis } from "../model/modelAvis";
import { Benevole } from "../model/modelBenevole";
import { Evenement } from "../model/modelEvenement";



    export async function getBenevole(req,res){
        res.json(await getRepository(Benevole).find());
    }
    export async function getBenevoleById(req,res){
        res.json(await getRepository(Benevole).findOne(req.params.id,{relations:["evenementsInscrit", "evenementsEnAttente"]}));
    }

    export async function modifyBenevole(req,res){
        const benevole = getRepository(Benevole).update(req.params.id, req.body);
        res.json(await getRepository(Benevole).findOne(req.params.id));
    }

    export async function insertBenevole(req,res){
        const benevole = getRepository(Benevole).create(req.body);
        res.json(await getRepository(Benevole).save(benevole));
  }

  export async function deleteBenevole(req,res){
    const benevole = getRepository(Benevole).delete(req.params.id);
        res.status(204).send();
    }

    export async function inscriptionEvenement(req,res) {

        // ajoute l'événement a la liste des evenements en attente du benevole
        let evenementAverifier = await getRepository(Evenement).findOne(req.params.evenementId)
        
        if (evenementAverifier["complet"] == true) {
            res.json("Erreur, evenement complet ajout impossible");
        } else {
            await getRepository(Benevole).createQueryBuilder()
            .relation("evenementsEnAttente")
            .of(req.params.benevoleId) //on inverse les entités quand ça marche pas evenementId evenementId
            .add(req.params.evenementId); 
            
            res.json(await getRepository(Evenement).findOne(req.params.evenementId));
            
           const benevole = await getRepository(Benevole).findOne(req.params.benevoleId);
           const evenement = await getRepository(Evenement).findOne(req.params.evenementId);
           console.log("mail du gestionnaire d'association " + evenement["association"]["gestionnaireAssociation"]["mail"]); //
           //console.log("mail du gestionnaire d'association " + evenement["association"]["gestionnaireAssociation"]["mail"]);
            sendMail({
                to: benevole.mail, 
                from: 'lcoujonde@myges.fr',// `"ConnectAsso" <${process.env.MAILER_USER}>`
                subject: "Confirmation demande de participation à un événement",
                text: `Connect Asso vous confirme que votre demande de participation à l'événement : ${evenement.titre} qui aura lieu le ${evenement.debut} au ${evenement.adresse}, A bien été prise en compte. L'association vous donnera une réponse très bientôt !`
            }).catch();
            sendMail({
                to: evenement.organisateur,
                from: 'lcoujonde@myges.fr',// `"ConnectAsso" <${process.env.MAILER_USER}>`
                subject: "Demande de participation à un événement",
                text: `Le bénévole ${benevole.prenom} ${benevole.nom} souhaite participer à l'événement : ${evenement.titre} qui aura lieu le ${evenement.debut} au ${evenement.adresse}, Veuillez s'il vous plait, vous rendre sur l'application Connect Asso pour traiter cette demande.`
            }).catch();
        }
    }

    export async function desinscriptionEvent(req,res) {
        let evenementAverifier = await getRepository(Evenement).findOne(req.params.evenementId)
        await getRepository(Evenement).createQueryBuilder()
            .relation("participantsInscrits")
            .of(req.params.benevoleId)
            .remove(req.params.evenementId);
        res.json(await getRepository(Evenement).findOne(req.params.evenementId));
        const benevole = await getRepository(Benevole).findOne(req.params.benevoleId);
        const evenement = await getRepository(Evenement).findOne(req.params.evenementId);

        const post = await getRepository(Evenement).findOne(req.params.evenementId);
        await getRepository(Evenement).update(req.params.evenementId, { nombreParticipants: post.nombreParticipants - 1})
                if (post.nombreParticipants + 1 == evenementAverifier["besoin"]) {
                    const post = await getRepository(Evenement).findOne(req.params.evenementId);
                    await getRepository(Evenement).update(req.params.evenementId, { complet: true });
                }
        sendMail({
            to: evenement.organisateur, 
            from: process.env.MAILER_USER,// `"ConnectAsso" <${process.env.MAILER_USER}>`
            subject: "Un participants s'est désisté",
            text: `Bonjour, un participant qui été inscrit à l'événement : ${evenement.titre} qui doit avoir lieu le ${evenement.debut} au ${evenement.adresse}, s'est désisté.`
        }).catch();
    }


    export async function removedEventsToWaitingList(req,res) {
        await getRepository(Benevole).createQueryBuilder()
            .relation("evenementsEnAttente")
            .of(req.params.benevoleId) 
            .remove(req.params.evenementId);
            res.json(await getRepository(Evenement).findOne(req.params.evenementId));
    }


    // export async function moveEventsToWaitingListToSignList(req,res) {
    //     await getRepository(Benevole).createQueryBuilder()
    //         .relation("evenementsEnAttente")
    //         .of(req.params.benevoleId)
    //         .remove(req.params.evenementId);
    //     await getRepository(Benevole).createQueryBuilder()
    //         .relation("evenementsInscrit")
    //         .of(req.params.benevoleId)
    //         .add(req.params.evenementId);
    //         res.json(await getRepository(Benevole).findOne(req.params.benevoleId));
    //         const benevole = await getRepository(Benevole).findOne(req.params.benevoleId);
    //         const evenement = await getRepository(Evenement).findOne(req.params.evenementId);
    //          sendMail({
    //              to: benevole.mail, 
    //              from: process.env.MAILER_USER,// `"ConnectAsso" <${process.env.MAILER_USER}>`
    //              subject: "Participation accepté",
    //              text: `Votre participation à l'événement : ${evenement.titre} qui aura lieu le ${evenement.debut} au ${evenement.adresse}, A été accepté, félicitation !`
    //          }).catch();
    // }

    
    //moveEventsToWaitingListToSignList

    

    export async function addEventsToSignList(req,res) {
        await getRepository(Benevole).createQueryBuilder()
            .relation("evenementsInscrit")
            .of(req.params.benevoleId)
            .add(req.params.evenementId);
            res.json(await getRepository(Benevole).findOne(req.params.benevoleId));

            const benevole = await getRepository(Benevole).findOne(req.params.benevoleId);
            const evenement = await getRepository(Evenement).findOne(req.params.evenementId);
             sendMail({
                 to: benevole.mail, 
                 from: process.env.MAILER_USER,// `"ConnectAsso" <${process.env.MAILER_USER}>`
                 subject: "Participation accepté",
                 text: `Votre participation à l'événement : ${evenement.titre} qui aura lieu le ${evenement.debut} au ${evenement.adresse}, A été accepté, félicitation !`
             }).catch();
    }

    export async function removedEventsToSignList(req,res) {
        await getRepository(Benevole).createQueryBuilder()
            .relation("evenementsInscrit")
            .of(req.params.benevoleId)
            .remove(req.params.evenementId);
            res.json(await getRepository(Benevole).findOne(req.params.benevoleId));

            const benevole = await getRepository(Benevole).findOne(req.params.benevoleId);
           const evenement = await getRepository(Evenement).findOne(req.params.evenementId);
    }


    export async function getAvisForOneBenevole(benevoleId : any): Promise<Avis[]> {
        return await getRepository(Avis).createQueryBuilder()
        .leftJoin("Avis.benevole", "Benevole")
        .where("Benevole.id=:benevoleId",{benevoleId})
        .getMany();
    }

    export async function getEvenementEnAttenteForOneBenevole(benevoleId : any): Promise<Evenement[]> {
        return await getRepository(Evenement).createQueryBuilder()
        .leftJoin("Evenement.participantsEnAttente", "Benevole")
        .where("Benevole.id=:benevoleId",{benevoleId})
        .getMany();
        
    }

    export async function getEvenementInscritForOneBenevole(benevoleId : any): Promise<Evenement[]> {
        return await getRepository(Evenement).createQueryBuilder()
        .leftJoin("Evenement.participantsInscrits", "Benevole")
        .where("Benevole.id=:benevoleId",{benevoleId})
        .getMany();
    }


    // export async function getParticipantsInscrit(evenementId: any): Promise<Benevole[]> {
    //     return await getRepository(Benevole).createQueryBuilder()
    //         .leftJoin("Benevole.evenementsInscrit", "Evenement")
    //         .where("Evenement.id=:evenementId", {evenementId})
    //         .getMany();
    // }


    
    



/* module.exports = {
    getBenevoles: function (req,res){
        benevole.findAll().then(data =>{
            res.json(data);
        }).catch(function (e){
            console.log(e);
        });
    },
    getBenevole: function (req,res){
        benevole.findByPk(req.params.id).then(data => {
          res.json(data);
        }).catch(function (e){
          console.log(e);
      });
    },
    deleteBenevole: function (req,res){
        benevole.destroy({where:{id:req.params.id}}).then(data=>{
          res.json("ok");
      }).catch((err) => {
          if(err){
              console.log(err);
          }
      });
    },

    insertBenevole: function (req,res){
      var b = { 'nom' :  req.body.nom,
      'prenom' : req.body.prenom,
      'mail' : req.body.mail,
      'pass' : req.body.pass,
      'description' : req.body.description
      }

      benevole.create(b).then(data=>{
          res.json("ok");
      }).catch((err) => {
          if(err){
              console.log(err);
          }
      });
    },

    modifyBenevole: function (req,res){

      benevole.update({ 'nom' :  req.body.nom,
      'prenom' : req.body.prenom,
      'mail' : req.body.mail,
      'pass' : req.body.pass,
      'description' : req.body.description
      }, {where:{id:req.params.id}}).then(data=>{
        res.json("ok");
    }).catch((err) => {
        if(err){
            console.log(err);
        }
    });
    }

} */