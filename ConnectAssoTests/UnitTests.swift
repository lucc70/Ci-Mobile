//
//  UnitTests.swift
//  ConnectAssoTests
//
//  Created by luc on 16/01/2022.
//

import XCTest
@testable import ConnectAsso

class UnitTests: XCTestCase {

    func testInitAssociation() throws {
        let association = Association(id: 1, nom: "SPA", info: "Association de protection animal", dateCreation: "20/05/2002")

        XCTAssertEqual(1, association.id)
        XCTAssertEqual("SPA", association.nom)
        XCTAssertEqual("Association de protection animal", association.info)
        XCTAssertEqual("20/05/2002", association.dateCreation)
    }

    func testInitBenevole() throws {
        let benevole = Benevole(id: 1, nom: "favier", prenom: "ludovic", mail: "l.favier@gmail.com", info: "")

        XCTAssertEqual(1, benevole.id)
        XCTAssertEqual("favier", benevole.nom)
        XCTAssertEqual("ludovic", benevole.prenom)
        XCTAssertEqual("l.favier@gmail.com", benevole.mail)
        XCTAssertEqual("", benevole.info)
    }

    func testInitGestionnaireAssociation() throws {
        let gestionnaireAssociation = GestionnaireAssociation(id: 1, nom: "Dupont", prenom: "Benoit", motDePasse: "azerty", mail: "benoit.dupont@gmail.com", descriptionPersonnelle: "je suis gestionnaire dévénement de l'association SPA Genevilliers depuis 2008")

        XCTAssertEqual(1, gestionnaireAssociation.id)
        XCTAssertEqual("Dupont", gestionnaireAssociation.nom)
        XCTAssertEqual("Benoit", gestionnaireAssociation.prenom)
        XCTAssertEqual("azerty", gestionnaireAssociation.motDePasse)
        XCTAssertEqual("benoit.dupont@gmail.com", gestionnaireAssociation.mail)
        XCTAssertEqual("je suis gestionnaire dévénement de l'association SPA Genevilliers depuis 2008", gestionnaireAssociation.descriptionPersonnelle)
    }


    func testInitEvenement() throws {
        let gestionnaireAssociation = GestionnaireAssociation(id: 1, nom: "Dupont", prenom: "Benoit", motDePasse: "azerty", mail: "benoit.dupont@gmail.com", descriptionPersonnelle: "je suis gestionnaire dévénement de l'association SPA Genevilliers depuis 2008")

        // swiftlint:disable:next line_length
        let infoEvenement = "Cette semaine à lieu la 6ème édition de la conférence internationale sur la recherche humanitaire de l’International humanitarian studies association (IHSA). La Fondation Croix-Rouge française, co-organisatrice de cet événement, propose deux panels sur la gestion des risques et catastrophes et sur les effets des épidémies sur le volontariat. C’est également durant cette conférence que la Fondation remettra ses prix de recherche annuel."

        let evenement = Evenement(id: 1, titre: "Conférence internationale 2021 de l’IHSA", dateDebut: "4/11/2021 15:30", dateFin: "4/11/2021 17:00", info: infoEvenement, adresse: "5 Rue du Maréchal Joffre, 06400 Cannes", besoin: 1, organisateur: gestionnaireAssociation.nom, complet: false, nombreParticipants: 10000, statut: "termine")

        XCTAssertEqual(1, evenement.id)
        XCTAssertEqual("Conférence internationale 2021 de l’IHSA", evenement.titre)
        XCTAssertEqual("4/11/2021 15:30", evenement.dateDebut)
        XCTAssertEqual("4/11/2021 17:00", evenement.dateFin)
        XCTAssertEqual(infoEvenement, evenement.info)
        XCTAssertEqual("5 Rue du Maréchal Joffre, 06400 Cannes", evenement.adresse)
        XCTAssertEqual(1, evenement.besoin)
        XCTAssertEqual("Dupont", evenement.organisateur)
        XCTAssertEqual(false, evenement.complet)
        XCTAssertEqual(10000, evenement.nombreParticipants)
        XCTAssertEqual("termine", evenement.statut)
    }


    func testCheckDateEvent(){
        let creerEvenementViewController = CreerEvenementViewController()
        let newDateEvent1 = Date().addingTimeInterval(15000)
        let newDateEvent2 = Date().addingTimeInterval(456600000000)
        let dateEventExisting1 = Date().addingTimeInterval(456600000000)
        let dateEventExisting2 = Date().addingTimeInterval(15001)
        let dateEventExisting3 = Date().addingTimeInterval(14056456456)
        let dateEventExisting4 = Date().addingTimeInterval(456600000000000000000)
        let dateEventExisting5 = Date().addingTimeInterval(34078678600)
        let dateEventExisting6 = Date().addingTimeInterval(37867864000)
        let dateEventExisting7 = Date().addingTimeInterval(785534000)
        let dateEventExisting8 = Date().addingTimeInterval(456600000000000000000)
        let dateEventExisting9 = Date().addingTimeInterval(387545255634000)
        let dateEventExisting10 = Date().addingTimeInterval(45686876734000)
        let dateEventExisting11 = Date().addingTimeInterval(135864564564000)
        let dateEventExisting12 = Date().addingTimeInterval(45660000000000000000)
        let dateEventExisting13 = Date().addingTimeInterval(36137455434000)
        let dateEventExisting14 = Date().addingTimeInterval(93874000)
        let dateEventExisting15 = Date().addingTimeInterval(7215334000)
        let dateEventExisting16 = Date().addingTimeInterval(456600000000)

        let allEventsExisting = [dateEventExisting1, dateEventExisting2, dateEventExisting3, dateEventExisting4, dateEventExisting5, dateEventExisting6, dateEventExisting7, dateEventExisting8, dateEventExisting9, dateEventExisting10, dateEventExisting11, dateEventExisting12, dateEventExisting13, dateEventExisting14, dateEventExisting15, dateEventExisting16]
        //print("ici \(newDateEvent1 == dateEventExisting3)")

        let res = creerEvenementViewController.compareNewEventDateWithAllEventDateExisting(newEventDate: newDateEvent1, allEventDateExisting: allEventsExisting)
        let res2 = creerEvenementViewController.compareNewEventDateWithAllEventDateExisting(newEventDate: newDateEvent2, allEventDateExisting: allEventsExisting)

        XCTAssertEqual(false, res)
        XCTAssertEqual(true, res2)
    }

    func testGetConvertedStringDateInData() throws{
        let creerEvenementViewController = CreerEvenementViewController()

        let dateDebutString: String = "23/09/2021 14:00"
        let dateFinString: String = "23/09/2021 18:00"

        let dates: [String: Date] = creerEvenementViewController.getConvertedStringDateInData(dateDebut: dateDebutString, dateFin: dateFinString)

        var dateDebutType = ""
        var dateFinType = ""
        print("\(type(of: dates["dateDebut"]!))", to: &dateDebutType)
        print("\(type(of: dates["dateFin"]!))", to: &dateFinType)

        let dateDebutTypeTrim = dateDebutType.trimmingCharacters(in: .whitespacesAndNewlines)
        let dateFinTypeTrim = dateFinType.trimmingCharacters(in: .whitespacesAndNewlines)

        XCTAssertEqual(dateDebutTypeTrim, "Date")
        XCTAssertEqual(dateFinTypeTrim, "Date")

    }


    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }



}

